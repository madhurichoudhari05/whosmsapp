package com.example.admin.a100dark.interfaces;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Admin on 3/19/2018.
 */

public interface SendContext {

    public Activity getActivity();
}
