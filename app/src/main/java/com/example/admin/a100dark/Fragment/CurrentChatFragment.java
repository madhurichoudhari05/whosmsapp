package com.example.admin.a100dark.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.SeachhActivity;
import com.example.admin.a100dark.Adapter.CurrentchatAdapter;
import com.example.admin.a100dark.Model.LatestMessage;
import com.example.admin.a100dark.Model.RecentchatModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class CurrentChatFragment extends Fragment {
    RecyclerView recy_chat_list;
    Context context;
    ArrayList<RecentchatModel> arrayList;
    RelativeLayout rl_root;
    RecentchatModel model;
    CurrentchatAdapter recentchatAdapter;
    EditText et_search;
    TextView tv_toolbar_title, tvGroup;
    private String token = "";
    private ImageView iv_profile_pic, ivProgress;
    List<LatestMessage> latestMessageList = new ArrayList<>();
    List<LatestMessage> updatemsgMessageList;
    private String profile_pic = "";
    private String senderid = "";
    private String firebasetoken = "";
    FloatingActionButton fab,fabParent;

    int count=0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        context = getActivity();

        //   latestMessageList.clear();

        rl_root = view.findViewById(R.id.rl_root);


        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fabParent = (FloatingActionButton) getActivity().findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, SeachhActivity.class));
                /*FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.frame, new Sel());
                fragmentTransaction.commit();*/
            }
        });

        // checkKeyBoardUp();

        if (CommonUtils.getPreferences(context, wehyphens.com.satyaconnect.utils.AppConstants.FIREBASE_KEY) != null) {

            firebasetoken = CommonUtils.getPreferences(context, wehyphens.com.satyaconnect.utils.AppConstants.FIREBASE_KEY);
        } else {
            firebasetoken = FirebaseInstanceId.getInstance().getToken();
        }

        senderid = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        updatemsgMessageList = new ArrayList<>();
        recentchatAdapter = new CurrentchatAdapter(latestMessageList, context);
        recy_chat_list = view.findViewById(R.id.recy_chat_list);
        ivProgress = view.findViewById(R.id.ivProgress);
        tv_toolbar_title = view.findViewById(R.id.tv_toolbar_title);
        tvGroup = view.findViewById(R.id.tvGroup);
        iv_profile_pic = view.findViewById(R.id.iv_profile_pic);
        et_search = view.findViewById(R.id.et_search);
        tv_toolbar_title.setText("Recent chats");
        tvGroup.setVisibility(View.VISIBLE);
        tvGroup.setText("Recent Chat will come soon here!");
        ivProgress.setVisibility(View.GONE);
        recy_chat_list.setVisibility(View.GONE);
        Animation animation1 =
                AnimationUtils.loadAnimation(context, R.anim.move);
        ivProgress.startAnimation(animation1);

        profile_pic = CommonUtils.getPreferences(context, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);

        if (profile_pic != null && !TextUtils.isEmpty(profile_pic)) {

            Picasso.with(context).load(profile_pic).noFade().error(R.drawable.placeholder_user).fit().centerCrop()
                    .into(iv_profile_pic);
        } else {
            iv_profile_pic.setImageResource(R.drawable.placeholder_user);

        }

        FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(senderid).orderByChild("timestamp").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //   Toast.makeText(context, "onChildAdded", Toast.LENGTH_SHORT).show();


                LatestMessage latestMessage = null;


                int firebaseObjectCount = 0;
                tvGroup.setVisibility(View.GONE);
                ivProgress.setVisibility(View.GONE);
                recy_chat_list.setVisibility(View.VISIBLE);
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        latestMessage = postSnapshot.getValue(LatestMessage.class);
                        firebaseObjectCount = (int) postSnapshot.getChildrenCount();

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            latestMessageList.add(latestMessage);
                        }

                    }

                   // tvMsgCount.setText(count+"");

                    if (latestMessageList != null && latestMessageList.size() > 0) {
                        Collections.sort(latestMessageList, Collections.reverseOrder());
                        recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                        recy_chat_list.setAdapter(recentchatAdapter);

                    } else {
                        tvGroup.setVisibility(View.VISIBLE);
                    }
                }

                else {
                    tvGroup.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                String oponent_id = "";
                String Serveroponent_id = "";
                String message = "";
                LatestMessage post = null;

                LatestMessage message1 = new LatestMessage();
                int firebaseObjectCount = (int) dataSnapshot.getChildrenCount();
                if (dataSnapshot.getValue() != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        post = postSnapshot.getValue(LatestMessage.class);
                        firebaseObjectCount = (int) postSnapshot.getChildrenCount();

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            Serveroponent_id = post.getOponent_id();
                        }
                        if (latestMessageList.size() == 0) {
                            latestMessageList.add(post);
                            Collections.sort(latestMessageList, Collections.reverseOrder());
                            recy_chat_list.setLayoutManager(new GridLayoutManager(context, 1));
                            recy_chat_list.setAdapter(recentchatAdapter);
                        }
                    }
                    Log.e("Currentpost", "Currentpost::" + post);
                    LatestMessage changedMsg = null;
                    int possition = 0;
                    if (latestMessageList != null && latestMessageList.size() > 0) {
                        try {
                            for (int i = 0; i < latestMessageList.size(); i++) {
                                if (latestMessageList.get(i).getOponent_id() != null || Serveroponent_id != null) {
                                    if (latestMessageList.get(i).oponent_id.equalsIgnoreCase(post.getOponent_id())) {
                                        Log.e("latestMessageList", "latestMessageList::" + possition);
                                        Log.e("latestMessageList", "latestMessageList::" + possition);
                                        Log.e("latestMessageList", "latestMessageList::" + message);
                                        Log.e("latestMessageList", "latestMessageList::" + oponent_id);
                                        changedMsg = post;
                                        Log.e("latestMessageList", "latestMessageList::" + changedMsg);
                                        possition = i;
                                        break;
                                    } else {
                                        //  Toast.makeText(context, "not::"+latestMessageList.get(i).getOponent_id(), Toast.LENGTH_SHORT).show();

                                    } } }


                            for (int i = 0; i < latestMessageList.size(); i++) {
                                    count= latestMessageList.size()-1;




                            }

                            CurrentchatAdapter.userCounter=0;

                          //   tvMsgCount.setText(count+"");

                          //  Toast.makeText(context, "onChildChanged"+count+1, Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (firebaseObjectCount == 1) {
                            //     Toast.makeText(context, "onChildAdded:::" + firebaseObjectCount, Toast.LENGTH_SHORT).show();

                        } else {
                            try {
                                if (message1 != null) {
                                    latestMessageList.set(possition, changedMsg);
                                    Collections.sort(latestMessageList, Collections.reverseOrder());
                                    recentchatAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            } } } }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString());
            }
        });
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        CurrentchatAdapter.userCounter=0;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }


    private void filter(String text) {
        //new array list that will hold the filtered data
        List<LatestMessage> filterList = new ArrayList();

        //looping through existing elements
        for (LatestMessage s : latestMessageList) {
            //if the existing elements contains the search input
            if (s.getOponent_name().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterList.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list

        if (filterList != null && filterList.size() > 0) {
            recentchatAdapter.filterList(filterList);
        }
    }


}



