package com.example.admin.a100dark.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.admin.a100dark.chat.interfaces.lemda.FunNoParam;


/**
 * Created by abul on 30/1/18.
 */

public class GpsLocationReceiver extends BroadcastReceiver {

    private FunNoParam mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
            if(mListener!=null){
                mListener.done();
            }
    }

    public void setmListener(FunNoParam listener){
        mListener=listener;
    }
}