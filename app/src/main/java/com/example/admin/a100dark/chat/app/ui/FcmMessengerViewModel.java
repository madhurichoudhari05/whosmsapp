package com.example.admin.a100dark.chat.app.ui;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.retofit.model.TokenResponse;
import com.example.admin.a100dark.chat.app.retofit.model.fcmRes.FcmResponseDto;
import com.example.admin.a100dark.chat.app.retofit.model.notif.Data;
import com.example.admin.a100dark.chat.app.retofit.model.notif.DataNotifDto;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.google.gson.Gson;


import java.util.HashMap;
import java.util.List;

import co.intentservice.chatui.models.ChatMessage;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abul on 28/12/17.
 */


public class FcmMessengerViewModel extends SuperAndroidViewModel implements IConstants.IFcm {

    private LiveData<List<ChatMessageDto>> chatDBOns;
    private LiveData<List<ChatMessageDto>> allChatObs;
    private LiveData<UserStatusDto> userStatusObs;
    private LiveData<List<UserStatusDto>> userListObs;
    private MutableLiveData<TokenResponse> fcmKeyObs;
    private LiveData<ChatMessageDto> latestMsgObs;
    private LiveData<Long> countObs;

    private MutableLiveData<Boolean> correctTokenObs;
    private MutableLiveData<String> myTokenUpdateObs;
    private MutableLiveData<Integer> requestStatus;
    boolean isValidToken = false;

    private LiveData<List<UserStatusDto>> userAppList;


    public FcmMessengerViewModel(@NonNull Application application) {
        super(application);
//        chatDBOns = mAppDb.chatDao().loadMessageByPost("","");
        allChatObs=mAppDb.chatDao().getAll();
        userListObs = mAppDb.userDao().getAll();
        fcmKeyObs = new MutableLiveData<>();
        correctTokenObs = new MutableLiveData<>();
        myTokenUpdateObs = new MutableLiveData<>();
        requestStatus = new MutableLiveData<>();
    }



    /*************************
     * Getter
     * ****************/

    public LiveData<List<ChatMessageDto>> getAllChatObs() {
        return allChatObs;
    }

    public MutableLiveData<Boolean> getCorrectTokenObs() {
        return correctTokenObs;
    }

    public LiveData<UserStatusDto> getUserStatusObs(String userId) {
        userStatusObs = mAppDb.userDao().getById(userId);
        return userStatusObs;
    }

    public LiveData<List<UserStatusDto>> getUserAppList(boolean approveStatus, boolean requestStatus) {
        userAppList = mAppDb.userDao().getAllByAppStatus(approveStatus, requestStatus);
        return userAppList;
    }

    public LiveData<List<UserStatusDto>> getUserAppList(boolean approveStatus) {
        userAppList = mAppDb.userDao().getAllByAppStatus(approveStatus);
        return userAppList;
    }

    public LiveData<List<ChatMessageDto>> getChatDBOns(boolean status) {
        chatDBOns = mAppDb.chatDao().loadMsgByReadStatus(status);
        return chatDBOns;
    }



    public LiveData<List<ChatMessageDto>> getChatDBOns(String toId) {
        chatDBOns = mAppDb.chatDao().loadMessageByUser(toId);
        return chatDBOns;
    }


    public LiveData<ChatMessageDto> getLatestMsgObs(String toId) {
        return latestMsgObs=mAppDb.chatDao().getLatestMsg(toId);
    }

    public LiveData<Long> getCountObs(String id,boolean status) {
        return countObs=mAppDb.chatDao().getUnreadMsgCount(id,status);
    }

    public MutableLiveData<TokenResponse> getFcmKeyObs() {
        return fcmKeyObs;
    }

    public MutableLiveData<String> getMyTokenUpdateObs() {
        return myTokenUpdateObs;
    }

    public LiveData<List<UserStatusDto>> getUserListObs() {
        return userListObs;
    }

    public MutableLiveData<Integer> getRequestStatus() {
        return requestStatus;
    }


    /*=========================================================================*/
    /******************
     * Send Msg
     * ****************/
    public void sendFcmMessage(ChatMessageDto chat, String fcmToken) {
        /*Header*/
        HashMap<String, String> header = new HashMap<>();
        header.put("Authorization", "key=" + IConstants.IFcm.FCM_LEGECY_KEY);

        /*data*/
        ChatMessageDto sendTypeMsg=chat.clone();
        sendTypeMsg.switchToSendType(chat);
        Data data = new Data("msg", new Gson().toJson(sendTypeMsg), 2);
        DataNotifDto dto = new DataNotifDto(fcmToken, data, false, "high", true);

        Observable<FcmResponseDto> call = mApi.sendMessage(header, dto);
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally(() -> status.setValue(IConstants.INetwork.RET_COMPLETE))
                .subscribe(responseBody -> {
                            if (responseBody.getSuccess() == 1) {
                                mExecutor.execute(() -> mAppDb.chatDao().insert(chat));
                                requestStatus.setValue(1);
                            } else {
                                requestStatus.setValue(0);
                            }
                        },
                        e -> requestStatus.setValue(0));
    }

/*=========================================================================================*/

    /****************
     * Update fcm token
     * ***************/
    public void updateFcmToken(String userId, String fcmToken) {

        call = mApiInter.updateFcmKey(userId, fcmToken);
        getCall().subscribe(responseBody -> {
                    myTokenUpdateObs.setValue(System.currentTimeMillis() + "");
                    status.setValue(IConstants.INetwork.RET_NEXT);
                    msgToast.setValue("Updated");
                },
                e -> {
                    status.setValue(IConstants.INetwork.RET_ERROR);
                    error.setValue(e);
                });
    }

    /*********************
     * Get fcm Token
     * ********************/
    public void getFcmToken(UserStatusDto user) {

        call = mApiInter.getFcmToken(user.getToId());
        getCall(TokenResponse.class).subscribe(responseBody -> {
                    status.setValue(IConstants.INetwork.RET_NEXT);
                    fcmKeyObs.setValue(responseBody);
                    user.setFcmKey(responseBody.getDeviceToken());
                    mExecutor.execute(() -> mAppDb.userDao().insert(user));
                },
                e -> {
                    status.setValue(IConstants.INetwork.RET_ERROR);
                    error.setValue(e);
                });
    }

}
