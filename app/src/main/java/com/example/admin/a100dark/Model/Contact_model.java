package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/8/2017.
 */

public class Contact_model {

    public Contact_model(String contact_name, String contact_status, String contact_time, int contact_image) {
        this.contact_name = contact_name;
        this.contact_status = contact_status;
        this.contact_time = contact_time;
        this.contact_image = contact_image;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_status() {
        return contact_status;
    }

    public void setContact_status(String contact_status) {
        this.contact_status = contact_status;
    }

    public String getContact_time() {
        return contact_time;
    }

    public void setContact_time(String contact_time) {
        this.contact_time = contact_time;
    }

    public int getContact_image() {
        return contact_image;
    }

    public void setContact_image(int contact_image) {
        this.contact_image = contact_image;
    }

    private String contact_name;
    private String contact_status;
    private String contact_time;
    private int contact_image;


}
