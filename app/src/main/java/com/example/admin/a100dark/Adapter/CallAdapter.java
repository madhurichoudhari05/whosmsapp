package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Model.CallModel;
import com.example.admin.a100dark.Model.Contact_model;
import com.example.admin.a100dark.Model.StatusModel;
import com.example.admin.a100dark.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 11/1/2017.
 */

public class CallAdapter extends  RecyclerView.Adapter<CallAdapter.ViewHolder> {
    ArrayList<CallModel> arrayList = new ArrayList<>();
    Context context;

    public CallAdapter(ArrayList<CallModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public CallAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_item, parent, false);
        CallAdapter.ViewHolder recyclerViewHolder = new CallAdapter.ViewHolder(view);

        return recyclerViewHolder;
    }
    @Override
    public void onBindViewHolder(CallAdapter.ViewHolder holder, int position) {
        CallModel callModel = arrayList.get(position);
        holder.userName.setText(callModel.getUserName());
        holder.timing.setText(callModel.getTiming());
        Picasso.with(context).load(callModel.getProfile_pic()).into(holder.profileImage);
        if (position == 0) {
          //  holder.linearLayout.setBackgroundColor(Color.parseColor("#00BFFF"));
            holder.closeImage.setImageResource(R.drawable.crosso);
            holder.videoCamera.setImageResource(R.drawable.video);
        } else if (position == 1) {
          //  holder.linearLayout.setBackgroundColor(Color.parseColor("#E85E7F"));
            holder.closeImage.setImageResource(R.drawable.crosso);
            holder.videoCamera.setImageResource(R.drawable.video);
        } else if (position == 2) {
            holder.closeImage.setImageResource(R.drawable.crosso);
            holder.videoCamera.setImageResource(R.drawable.video);
         //   holder.linearLayout.setBackgroundColor(Color.parseColor("#0abc70"));
        } else if (position % 3 == 0) {
           // holder.linearLayout.setBackgroundColor(Color.parseColor("#521155"));
            holder.closeImage.setImageResource(R.drawable.tick);
            holder.videoCamera.setImageResource(R.drawable.phone);

        } else if (position % 4 == 0) {
            holder.closeImage.setImageResource(R.drawable.crosso);
            holder.videoCamera.setImageResource(R.drawable.video);
           // holder.linearLayout.setBackgroundColor(Color.parseColor("#f5b022"));
        } else if (position % 5 == 0) {
         //   holder.linearLayout.setBackgroundColor(Color.parseColor("#267dc5"));
            holder.closeImage.setImageResource(R.drawable.tick);
            holder.videoCamera.setImageResource(R.drawable.phone);

        }

    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, timing;
        LinearLayout linearLayout;
        ImageView closeImage,videoCamera;
        CircleImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.call_linear_layout);
            userName = (TextView) itemView.findViewById(R.id.call_user_name);
            timing = (TextView) itemView.findViewById(R.id.call_timing);
            profileImage = (CircleImageView) itemView.findViewById(R.id.call_profile_image);
            closeImage=(ImageView)itemView.findViewById(R.id.close_image);
            videoCamera=(ImageView)itemView.findViewById(R.id.video_camera);


        }
    }
}