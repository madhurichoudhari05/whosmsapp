package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ChangeStatusAdapter;
import com.example.admin.a100dark.Model.StatusData;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.MadhuFileUploader;
import com.example.admin.a100dark.utils.RecyclerTouchListener;

import org.apache.http.HttpException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ChangeStatusActivity extends AppCompatActivity {


    RecyclerView statusRecycler;
    ImageView editStatus;
    Context mContext;
    List<StatusData> statusDataList;
    StatusData data;
    EditText tv_status;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;

    String [] all_status={"Available","Away","Busy","At Work","Home","At the movies","In a meating","Sleeping",
            "Urgent Calls only","Gym","Office","Dinner"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_status);
        mContext=ChangeStatusActivity.this;
        statusRecycler = findViewById(R.id.statusRecycler);
        editStatus = findViewById(R.id.editStatus);
        tv_status = findViewById(R.id.tv_status);
        tv_status.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_PROFILE_STATUS));


        statusDataList=new ArrayList<>();
        data = new StatusData();
        data.setStatus("Available");
        statusDataList.add(data);

        data = new StatusData();
        data.setStatus("Away");
        statusDataList.add(data);
        data = new StatusData();
        data.setStatus("Busy");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("At Work");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("Home");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("At the movies");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("In a meating");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("Sleeping");
        statusDataList.add(data);

        data = new StatusData();
        data.setStatus("Urgent Calls only");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("Gym");
        statusDataList.add(data);


        data = new StatusData();
        data.setStatus("office");
        statusDataList.add(data);

        data = new StatusData();
        data.setStatus("dinner");
        statusDataList.add(data);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ChangeStatusActivity.this);
        statusRecycler.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(statusRecycler.getContext(),
                linearLayoutManager.getOrientation());
        statusRecycler.addItemDecoration(dividerItemDecoration);

        ChangeStatusAdapter changeStatusAdapter = new ChangeStatusAdapter(ChangeStatusActivity.this,statusDataList);
        statusRecycler.setAdapter(changeStatusAdapter);

        statusRecycler.addOnItemTouchListener(new RecyclerTouchListener(mContext, statusRecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                StatusData data = statusDataList.get(position);
                String status=data.getStatus();
                status = data.getStatus();
              //  updateStatus(status);
                tv_status.setText(status);

                String  user_id=CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
                String  name=CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
                callProfileData(name,status,user_id);
               // et_fname.setText(status);

                Toast.makeText(mContext, data.getStatus() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        editStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // tv_status.setEnabled(true);

                startActivity(new Intent(ChangeStatusActivity.this, EditStatusActivity.class));
            }
        });

        findViewById(R.id.statusBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }


    private void callProfileData(String userName,String userStatus,String userID) {
        CommonUtils.showProgress(mContext);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call=null;
        String  user_id=CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
        RequestBody user_id1 = RequestBody.create(MediaType.parse("text/plain"),userID);
        RequestBody emoji_staus = RequestBody.create(MediaType.parse("text/plain"),userStatus);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"),userName );
        call = service.uploadFileWithPartMap(user_id1, emoji_staus,name);
        call.enqueue(new Callback<ResponseBody>() {
            public boolean status;
            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();
                String str = "";
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        boolean profilepic = jsonObject.getBoolean("status");
                        String messageimg = jsonObject.getString("message");
                        CommonUtils.savePreferencesString(mContext,AppConstants.USER_PROFILE_STATUS,userStatus);
                        // CommonUtils.savePreferencesString(mContext,AppConstants.USER_NAME,etName.getText().toString());
                        // CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
                        if (profilepic) {
                            //  CommonUtils.snackBar(messageimg,cameraopen);
                            if (messageimg != null && !messageimg.isEmpty()) {
                                //  callcongratulation();
                            }
                            else {
                                Log.e("userpic", "null");}
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.dismissProgress();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }
}
