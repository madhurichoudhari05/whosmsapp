package com.example.admin.a100dark.Activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.icu.text.IDNA;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Adapter.SearchAdapter;
import com.example.admin.a100dark.Adapter.StateAdapterDialog;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.Model.chat_model;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.RunTimePermissionWrapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit2.Callback;

/**
 * Created by Admin on 11/3/2017.
 */

public  class SeachhActivity extends AppCompatActivity {


    RecyclerView chatrecylerview, otherrecylerview, messagerecylerview;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    SearchAdapter searchAdapter, othersearchAdapter;
    List<AllUserListDetailsModel> userList = new ArrayList<>();
    ArrayList<AllUserListDetailsModel> contactlist = new ArrayList<>();
    private EditText textEdittext;
    ImageView cross, leftarrow;
    private Context mcontext;
    private List<CountryCodeDetails> mStringFilterList;
    //  private String user_id = "";
    private int user_id = 0;
    String[] WALK_THROUGH = new String[]{Manifest.permission.READ_CONTACTS};
    private String[] PERMISSIONS = {Manifest.permission.READ_CONTACTS};
    private AppDatabase mAppDb;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    ArrayList<String> phoneContactList;
    public ProgressDialog pDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchactivity);
        mcontext = SeachhActivity.this;
        mAppDb = MyApplication.getDb();
        CommonUtils.savePreferencesBoolean(mcontext, AppConstants.FIRST_TIME_LOGIN, true);
        phoneContactList = new ArrayList<>();
        contactlist = new ArrayList<>();
        Thread t = new Thread(new Runnable() {
            public void run() {
                /*
                 * Do something
                 */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                } else {
                    // Android version is lesser than 6.0 or the permission is already granted.
                    getContactList();
                    Log.e("TAG", phoneContactList.size() + "");
                }

            }
        });

        Thread t2 = new Thread(new Runnable() {
            public void run() {
                /*
                 * Do something
                 */
               // callUserListApi();

            }
        });


        t.start();

       // Log.d("TAG",userList.size()+"");
        Log.d("TAG",phoneContactList.size()+"");
         new GetContacts().execute();
        if (CommonUtils.getPreferencesString(mcontext, AppConstants.USER_ID) != null && (!CommonUtils.getPreferencesString(mcontext, AppConstants.USER_ID).equalsIgnoreCase("")))
            try {
                user_id = Integer.valueOf(CommonUtils.getPreferencesString(mcontext, AppConstants.USER_ID));
                Log.d("TAG", user_id + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        chatrecylerview = (RecyclerView) findViewById(R.id.chatrecyler);
        otherrecylerview = (RecyclerView) findViewById(R.id.otherrecyler);
        cross = (ImageView) findViewById(R.id.cross);


        // callUserListApi();
        /*if (!hasPermissions(mcontext, PERMISSIONS)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }

        } else if (hasPermissions(mcontext, PERMISSIONS)) {
            initContactFragment();
        }*/
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                textEdittext.setVisibility(View.INVISIBLE);
            }
        });
        leftarrow = (ImageView) findViewById(R.id.leftarrow);
        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // startActivity(new Intent(SeachhActivity.this, MainActivity.class));
                onBackPressed();

            }
        });
        textEdittext = (EditText) findViewById(R.id.textEdittext);
        searchAdapter = new SearchAdapter(userList, mcontext, userList);
        chatrecylerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        chatrecylerview.setAdapter(searchAdapter);

        textEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                searchAdapter.getFilter().filter(arg0);
                searchAdapter.getItemViewType(arg1);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }

    private void callUserListApi() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
        call.enqueue(new Callback<List<AllUserListModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                List<AllUserListModel> userListModelList = response.body();
                userList.clear();
                try {
                    if (userListModelList.get(0).getCurrentUser().getUserName() != null) {
                        CommonUtils.savePreferencesString(mcontext, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());
                        CommonUtils.savePreferencesString(mcontext, AppConstants.SENDER_PROFILE_PIC_server, userListModelList.get(0).getCurrentUser().getUserPic());

                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (userListModelList != null && userListModelList.size() > 0) {
                    for (int i = 0; i < userListModelList.size(); i++) {
                        for (int j = 0; j < userListModelList.get(i).getAllUser().size(); j++) {
                            for (int k = 0; k < phoneContactList.size(); k++) {
                                String contactno = phoneContactList.get(k);
                                // contactno=contactno.replace("[-+.^:,]","");  // remove special character from string
                                // contactno=contactno.replaceAll("[()\\-\\s]+","").trim();
                                // contactno=contactno.replace("+","");
                                // contactno=contactno.replace("-","");
                                contactno = contactno.replaceAll("\\D+", "");
                                if (contactno.contains(userListModelList.get(i).getAllUser().get(j).getUserMobile())) {
                                    // userList.addAll((Collection<? extends AllUserListDetailsModel>) userListModelList.get(i).getAllUser().get(j));
                                    userList.add(userListModelList.get(i).getAllUser().get(j));
                                }
                            }
                        }
                    }


                    searchAdapter.notifyDataSetChanged();

                } else {
                    CommonUtils.snackBar("Not Fetched", leftarrow);
                }

                // userList=removeDuplicates(contactlist);

                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
            }
        });


    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (RunTimePermissionWrapper.isAllPermissionGranted(this, PERMISSIONS)) {
            getContactList();
            Log.e("TAG", phoneContactList.size() + "");
        } else {
            showSnack(ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS[0]));
        }
    }

    private void initContactFragment() {
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            AllUserListDetailsModel contactModel = new AllUserListDetailsModel();
            contactModel.setUserName(name);
            contactModel.setUserMobile(phoneNumber);
            //contactlist.add(contactModel);
            // bothlist.add(contactModel);
        }
    }


    // Function to remove duplicates from an ArrayList
    public static <String> ArrayList<String> removeDuplicates(ArrayList<String> list) {

        // Create a new ArrayList
        ArrayList<String> newList = new ArrayList<String>();

        // Traverse through the first list
        for (String element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }


    private void getContactList() {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        String phoneNo = "0";
        ArrayList<String> contactList = new ArrayList<>();


        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String number = null;
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        Log.i("TAG", "Name: " + name);
                        Log.i("TAG", "Phone Number: " + phoneNo);
                        phoneNo = phoneNo.replace(" ", "");
                        contactList.add(phoneNo);
                    }
                    pCur.close();
                }
            }
        }


        phoneContactList = removeDuplicates(contactList);
        if (cur != null) {
            cur.close();
        }
    }

    private void showSnack(final boolean isRationale) {
        final Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "Please provide contact permission", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(isRationale ? "VIEW" : "Settings", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();

                if (isRationale)
                    RunTimePermissionWrapper.handleRunTimePermission(SeachhActivity.this, RunTimePermissionWrapper.REQUEST_CODE.MULTIPLE_WALKTHROUGH, WALK_THROUGH);
                else
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1001);
            }
        });

        snackbar.show();
    }




 class GetContacts extends AsyncTask<Void, Void, Void> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       //  Showing progress dialog
        pDialog = new ProgressDialog(SeachhActivity.this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

    }

    @Override
    protected Void doInBackground(Void... arg0) {
        // Making a request to url and getting response

        Thread t = new Thread() {
            public void run() {
                ContentResolver cr = getContentResolver();
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                String phoneNo = "0";
                ArrayList<String> contactList = new ArrayList<>();

                if ((cur != null ? cur.getCount() : 0) > 0) {
                    while (cur != null && cur.moveToNext()) {
                        String number = null;
                        String id = cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        if (cur.getInt(cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                            while (pCur.moveToNext()) {
                                phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                Log.i("TAG", "Name: " + name);
                                Log.i("TAG", "Phone Number: " + phoneNo);
                                phoneNo = phoneNo.replace(" ", "");
                                contactList.add(phoneNo);
                            }
                            pCur.close();
                        }
                    }
                }


                phoneContactList = removeDuplicates(contactList);
                if (cur != null) {
                    cur.close();
                }
            }
        };

        Thread t2 = new Thread() {
            public void run() {
                FileUploadInterface service = RetrofitHandler.getInstance().getApi();
                //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
                retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
                call.enqueue(new Callback<List<AllUserListModel>>() {
                    @Override
                    public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                        List<AllUserListModel> userListModelList = response.body();
                        userList.clear();
                        try {
                            if (userListModelList.get(0).getCurrentUser().getUserName() != null) {
                                CommonUtils.savePreferencesString(mcontext, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());
                                CommonUtils.savePreferencesString(mcontext,AppConstants.SENDER_PROFILE_PIC_server,userListModelList.get(0).getCurrentUser().getUserPic());
                            }
                        }catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        if (userListModelList != null && userListModelList.size() > 0) {
                            for (int i = 0; i < userListModelList.size(); i++) {
                                for (int j = 0; j < userListModelList.get(i).getAllUser().size(); j++) {
                                    for (int k=0;k<phoneContactList.size();k++){
                                        String contactno=phoneContactList.get(k);
                                        // contactno=contactno.replace("[-+.^:,]","");  // remove special character from string
                                        // contactno=contactno.replaceAll("[()\\-\\s]+","").trim();
                                        // contactno=contactno.replace("+","");
                                        // contactno=contactno.replace("-","");
                                        contactno=contactno.replaceAll("\\D+","");
                                        if(contactno.contains(userListModelList.get(i).getAllUser().get(j).getUserMobile())){
                                            // userList.addAll((Collection<? extends AllUserListDetailsModel>) userListModelList.get(i).getAllUser().get(j));
                                            userList.add( userListModelList.get(i).getAllUser().get(j));
                                        }
                                    }
                                }
                            }
                            searchAdapter.notifyDataSetChanged();

                        } else {
                            CommonUtils.snackBar("Not Fetched", leftarrow);
                        }
                        // userList=removeDuplicates(contactlist);
                        //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
                    }
                });
            }
        };

        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t2.start();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        // Dismiss the progress dialog
        if (pDialog.isShowing())
           pDialog.dismiss();
        searchAdapter = new SearchAdapter(userList, mcontext,userList);
        chatrecylerview.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        chatrecylerview.setAdapter(searchAdapter);


    }

}
}


