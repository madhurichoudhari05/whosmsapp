package com.example.admin.a100dark.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.interfaces.StateInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/16/2017.
 */

public class StateAdapterDialog extends BaseAdapter  implements Filterable {
    Context context;
    private List<CountryCodeDetails> result;
    private StateInterface stateInterface;
    private ValueFilter valueFilter;
    private static LayoutInflater inflater = null;
    private int type;
    List<CountryCodeDetails> currentSelected=new ArrayList<>();
    private List<CountryCodeDetails> mStringFilterList;

    public StateAdapterDialog(Activity activity, List<CountryCodeDetails> name, StateInterface stateInterface, int type) {
        this.result = name;
        this.context = activity;
        mStringFilterList=name;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.type=type;
       this. stateInterface=stateInterface;
    }
    @Override
    public int getCount() {

        return result.size();
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }
    public class Holder {
        TextView name;
        ImageView img;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // TODO Auto-generated method stub

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View rowView;
        rowView = mInflater.inflate(R.layout.country_item, null);
        final LinearLayout marridlayoutvisible = (LinearLayout)rowView.findViewById(R.id.marridlayoutvisible);
        final TextView  name = (TextView) rowView.findViewById(R.id.tvCountryName);
        final TextView  code = (TextView) rowView.findViewById(R.id.tvCountryCode);
       final ImageView img = (ImageView) rowView.findViewById(R.id.iamgegreen);
        CountryCodeDetails stateModel=result.get(position);

         name.setText(stateModel.getName());
         code.setText("+ "+stateModel.getPhonecode());

        if (stateModel.isSelected()) {
            img.setVisibility(View.VISIBLE);
            img.setImageResource(R.drawable.ic_check_black_24dp);

            marridlayoutvisible.setBackgroundColor(Color.parseColor("#FFFFFF"));

            name.setTextColor(Color.parseColor("#64676c"));
            code.setTextColor(Color.parseColor("#64676c"));

        } else {
            img.setVisibility(View.INVISIBLE);

            img.setImageResource(R.drawable.ic_check_white_24dp);

            marridlayoutvisible.setBackgroundColor(Color.parseColor("#FFEFDFD9"));

            name.setTextColor(Color.parseColor("#e86131"));
            code.setTextColor(Color.parseColor("#e86131"));





        }


        rowView.setOnClickListener( v-> {
            CountryCodeDetails tempResult=null;
            for(int i=0;i<result.size();i++){
                CountryCodeDetails m=result.get(i);
                if(position==i){
//                    m.isSeleted() ? m.setSeleted(false):m.setSeleted(true)
                    if (m.isSelected()) {
                        currentSelected.remove(m);
                        m.setSelected(false);
                     //   stateInterface.setValue("","",false);
                       // notifyDataSetChanged();
                    } else {
                        tempResult=m;
                        currentSelected.add(m);
                        m.setSelected(true);
                    //    stateInterface.setValue(tempResult.getName(), tempResult.getPhonecode(),true);
                       // notifyDataSetChanged();
                    }
                }else {
                    m.setSelected(false);
                }

            }


            if (tempResult!= null) {
                stateInterface.setValue(tempResult.getName(), tempResult.getPhonecode(), true);
            }else {
                stateInterface.setValue("","", false);
            }
            stateInterface.setValue(currentSelected,type);
            notifyDataSetChanged();
        });



        return rowView;






    }

    public void setSelection(String id){
        for(int i=0;i<result.size();i++){
            CountryCodeDetails m=result.get(i);
            if(id.equals(m.getPhonecode())){
                m.setSelected(true);
                stateInterface.setValue(m.getName(), m.getPhonecode(),false);
                break;
            }else {
                m.setSelected(false);
            }

        }
        notifyDataSetChanged();
    }

    public void setSelection(List<String> list){
        if(list.size()==0){
            return;
        }

        for(int i=0;i<result.size();i++){
            CountryCodeDetails m=result.get(i);
            for (String id:list){
                if(id.equals(m.getPhonecode())){
                    currentSelected.add(m);
                    m.setSelected(true);
                }else {
                    m.setSelected(false);
                }
            }
        }
        stateInterface.setValue(currentSelected, type);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<CountryCodeDetails> filterList=new ArrayList<CountryCodeDetails>();
                for(int i=0;i<mStringFilterList.size();i++){
                    if((mStringFilterList.get(i).getName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        CountryCodeDetails contacts = new CountryCodeDetails();
                        contacts.setName(mStringFilterList.get(i).getName());
                        contacts.setPhonecode(mStringFilterList.get(i).getPhonecode());
                        filterList.add(contacts);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=mStringFilterList.size();
                results.values=mStringFilterList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            result=(ArrayList<CountryCodeDetails>) results.values;
            notifyDataSetChanged();
        }
    }
}