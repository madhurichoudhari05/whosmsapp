package com.example.admin.a100dark.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.a100dark.Adapter.CallAdapter;
import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Model.CallModel;
import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.R;

import java.util.ArrayList;

/**
 * Created by Admin on 11/1/2017.
 */

public class CallFragment extends Fragment {


    ArrayList<CallModel> arrayList = new ArrayList<>();

    RecyclerView recyclerView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.call_screen, null);
        recyclerView=(RecyclerView)vv.findViewById(R.id.call_recyclerview);

        init();

        CallAdapter callAdapter = new CallAdapter(arrayList,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(callAdapter);

        return vv;
    }
    public void init()
    {
        for (int i = 0 ; i<10;i++) {
          CallModel callModel = new CallModel("Arjun","Today 9:00 a.m.",R.drawable.images);
            arrayList.add(callModel);

        }
    }
}
