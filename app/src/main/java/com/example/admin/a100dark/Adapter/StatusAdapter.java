package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.Model.StatusModel;
import com.example.admin.a100dark.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11/1/2017.
 */

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.ViewHolder> {
    ArrayList<StatusModel> arrayList = new ArrayList<>();
    Context context;

    public StatusAdapter(ArrayList<StatusModel> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public StatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_item, parent, false);
        StatusAdapter.ViewHolder recyclerViewHolder = new StatusAdapter.ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(StatusAdapter.ViewHolder holder, int position) {
        StatusModel statusModel = arrayList.get(position);
        holder.userName.setText(statusModel.getUserName());
        holder.timing.setText(statusModel.getStatusTiming());
        Picasso.with(context).load(statusModel.getProfileImage()).into(holder.profileImage);

        if (position == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#00BFFF"));
        } else if (position == 1) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#E85E7F"));
        } else if (position == 2) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#0abc70"));
        } else if (position % 3 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#521155"));
        } else if (position % 4 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#f5b022"));
        } else if (position % 5 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#267dc5"));
        }

    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, timing;
        LinearLayout linearLayout;
        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.status_linear_layout);
            userName = (TextView) itemView.findViewById(R.id.status_user_name);
            timing = (TextView) itemView.findViewById(R.id.status_time);
            profileImage = (ImageView) itemView.findViewById(R.id.status_profile_image);


        }
    }
}

