package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.example.admin.a100dark.Fragment.CallFragment;
import com.example.admin.a100dark.Fragment.ChatFragment;
import com.example.admin.a100dark.Fragment.ContainerChatFragment;
import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.Fragment.cam_frgament;

/**
 * Created by Admin on 10/31/2017.
 */

public class ChatViewPagerAdapter extends FragmentPagerAdapter {
    Drawable myDrawable;
    private Context mcontext;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();


    public ChatViewPagerAdapter(FragmentManager fm , Context  mcontext) {
        super(fm);
       this. mcontext=mcontext;

    }
    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new NewUserFragment();
        else
            return new ChatFragment();

    }

    @Override
    public int getCount() {
        return 2;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "";
            case 1:

                return "";
            default:return "";
        }
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    /**
     * Remove the saved reference from our Map on the Fragment destroy
     *
     * @param container
     * @param position
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }


    /**
     * Get the Fragment by position
     *
     * @param position tab position of the fragment
     * @return
     */
    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

}
