package com.example.admin.a100dark.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ViewPagerAdapter;
import com.example.admin.a100dark.Fragment.CurrentChatFragment;
import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

public class HomeActivity extends AppCompatActivity {



    TabLayout tabLayout;
    Toolbar toolBar;
    TextView setting, newgroup, newbroadcast, whatsappweb, starredmessage;
    ImageView search;
    ImageView more;
    FloatingActionButton fab, fabedit;
    private Context context;
    FrameLayout flLayout;
    private boolean doubleBackToExitPressedOnce=false;
     public  static TextView tvMsgCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = HomeActivity.this;
        setContentView(R.layout.activity_home);
        flLayout = (FrameLayout) findViewById(R.id.flLayout);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        search = (ImageView) findViewById(R.id.search);
        more = (ImageView) findViewById(R.id.more);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabedit = (FloatingActionButton) findViewById(R.id.fabedit);
        tvMsgCount =findViewById(R.id.tvMsgCountHome);

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, Setting.class));
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SeachhActivity.class));

            }
        });


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
           // Toast.makeText(context, "MainActivitytrue", Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            CurrentChatFragment frag=new CurrentChatFragment();
            transaction.replace(R.id.flLayout, frag);
            transaction.commit();
            }
        else {
          //  Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();

            FragmentManager fragmentManager=getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            NewUserFragment newUserFragment=new NewUserFragment();
            transaction.replace(R.id.flLayout, newUserFragment);
            transaction.commit();

            }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}




