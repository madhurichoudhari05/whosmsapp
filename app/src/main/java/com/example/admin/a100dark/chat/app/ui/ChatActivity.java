package com.example.admin.a100dark.chat.app.ui;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.MutableLiveData;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.MsgForwordActivity;
import com.example.admin.a100dark.Activity.NewBRoasdCast;
import com.example.admin.a100dark.Activity.NewGroup;
import com.example.admin.a100dark.Activity.Setting;
import com.example.admin.a100dark.Activity.Starredmessage;
import com.example.admin.a100dark.Activity.WhatsAppWebActivity;
import com.example.admin.a100dark.Model.Message;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.StaticConfig;
import com.example.admin.a100dark.chat.app.retofit.FcmApi;
import com.example.admin.a100dark.chat.dependency.handler.FCMRetrofitHandler;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.CropActivity;
import com.example.admin.a100dark.utils.GlobalAccess;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;

import org.apache.http.util.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.activity.UIChatProfileActivity;
import co.intentservice.chatui.models.ChatMessage;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Callback;


import static com.vincent.filepicker.activity.BaseActivity.IS_NEED_FOLDER_LIST;
import static com.vincent.filepicker.activity.VideoPickActivity.IS_NEED_CAMERA;


public class ChatActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    private ImageView leftarrow;
    public static HashMap<String, Bitmap> bitmapAvataFriend;
    private ArrayList<CharSequence> idFriend;
    private String roomId = "";
    private String sendermsg = "";
    private TextView tvName;
    private boolean sendStatus;
    private String chatMsg = "";
    private String chatType = "";
    private Dialog mDialog;
    private Context mContext;
    private ChatView chatView;
    private CardView llcontainer;
    private String userName = "", fileName = "";
    private File file, thumbImagefile1;
    public static final int RequestPermissionCode = 1;
    Dialog dialog;
    private FirebaseUser firebaseUser;
    private DatabaseReference databaseReference;
    CoordinatorLayout coordinatorLayout;
    private ImageView iv_profile_pic, ivProgress;
    private FirebaseAuth firebaseAuth;
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String mCurrentPhotoPath = "";
    private Uri mCurrentPhotoUri;
    // private LinearLayout rl_header;
    private CircleImageView rl_header;
    private String userOnline_id = "";
    private ArrayList<String> photoPaths = new ArrayList<>();
    ArrayList<String> docPaths = new ArrayList<>();
    private boolean isFileImg;
    private LinearLayout llBorder;
    private String filename = "";
    private ChatMessage chatMessage;
    private MutableLiveData<Integer> requestStatus;
    String receiverid = "";
    String notificationReceiverid = "";
    String senderid = "";
    String chatId = "", group_id = "", nodeId = "";
    int RTPYE = 0;
    int STPYE = 0;
    int msgLocalCount = 0;
    String image = "";
    String user_name = "";
    String userstatus = "";
    private ImageView ivuserBackground;
    private String fireBaseKey = "";
    private ImageView iv_delete, iv_copy, tv_forword;
    private ArrayList<Message> firebaseTimestamp;
    private int deletePosition = 0;
    long globalTimeStamp = 0;


    /* String RTPYE="";
     String STPYE="";*/
    protected FcmApi mApi;
    protected MutableLiveData<Integer> status;
    private StorageReference storageReference;
    private String receiverPic = "";
    private String reciver_name = "";
    private String groupName = "", group_user_name = "";
    private String firebaseUsergroupName = "";
    private String userScreenStatus = "";
    private int serverCount = 0;
    private List<Integer> group_arrayCountList;
    private String user_id = "";
    private ImageView more;
    TextView setting, newgroup, newbroadcast, whatsappweb, starredmessage;


    private static final String TAG = "ChatActivity";
    private String sender_img = "";
    private String sender_img_server = "";
    AlertDialog.Builder builder;
    private ClipboardManager myClipboard;
    private ClipData myClip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caht);
        mContext = ChatActivity.this;
        builder = new AlertDialog.Builder(mContext);
        getToken();
        group_arrayCountList = new ArrayList<>();
        firebaseTimestamp = new ArrayList<>();

        //TODO  getting all the deatils about UserProfile
        user_id = CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
        try {
            STPYE = Integer.valueOf(CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (CommonUtils.getPreferencesString(mContext, AppConstants.USER_STATUS) != null) {
            userstatus = CommonUtils.getPreferencesString(mContext, AppConstants.USER_STATUS);
        }
        if (CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME) != null) {
            user_name = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
        }


        if (CommonUtils.getPreferencesString(mContext, AppConstants.PROFILE_PIC) != null) {
            sender_img = CommonUtils.getPreferences(mContext, AppConstants.PROFILE_PIC);
        }

        if (CommonUtils.getPreferencesString(mContext, AppConstants.SENDER_PROFILE_PIC_server) != null) {
            sender_img_server = CommonUtils.getPreferences(mContext, AppConstants.SENDER_PROFILE_PIC_server);
        }

        Log.e("sender_img_server", sender_img_server);


        //TODO getting IDs of views
        tvName = (TextView) findViewById(R.id.tvName);
        rl_header = (CircleImageView) findViewById(R.id.ivuserBackground);
        ivProgress = findViewById(R.id.ivProgress);
        leftarrow = findViewById(R.id.leftarrow);
        chatView = (ChatView) findViewById(R.id.chat_view);
        ivuserBackground = findViewById(R.id.ivuserBackground);
        llBorder = findViewById(R.id.root_view);
        coordinatorLayout = findViewById(R.id.coordinator);


        //TODO intailize firebase lebraries

        status = new MutableLiveData<>();
        requestStatus = new MutableLiveData<>();
        mApi = FCMRetrofitHandler.getInstance().getApi();
        storageReference = FirebaseStorage.getInstance().getReference();


        //TODO setting details of views


        more = (ImageView) findViewById(R.id.more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                eventDialog();
            }
        });

        chatView.setTypingListener(new ChatView.TypingListener() {
            @Override
            public void userStartedTyping() {
            }

            @Override
            public void userStoppedTyping() {

            }

            @Override
            public void userClick() {
                Intent intent = new Intent(mContext, ActivityAudioRecording.class);
                startActivityForResult(intent, AppConstants.AUDIO_CALL_STATUS);
            }

            @Override
            public void userDeleteClick(int position, String imageStatus1, View view) {
                deletePosition = position;
                view.setBackgroundColor(Color.parseColor("#2bb3de"));

                if (firebaseTimestamp.get(deletePosition).type.equalsIgnoreCase("0")) {
                    // for delete messaage intance
                    // do nothing
                } else {
                    if (firebaseTimestamp.get(position).sender_id.equalsIgnoreCase(String.valueOf(STPYE))) {
                        deleteReplyDailog(mContext, true, imageStatus1, view);
                    } else {
                        deleteReplyDailog(mContext, false, imageStatus1, view);
                    }
                }
            }

        });


        //TODO getting all the intent Data


        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(wehyphens.com.satyaconnect.utils.AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
            //   Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();
            if (getIntent().getStringExtra(AppConstants.GROUP_NODE) != null) {
                nodeId = getIntent().getStringExtra(AppConstants.GROUP_NODE);
                Log.e("GROUP_NODE", "GROUP_NODE" + chatId);
            }

            if (getIntent().getStringExtra(AppConstants.GROUP_NAME) != null) {

                groupName = getIntent().getStringExtra(AppConstants.GROUP_NAME);
                firebaseUsergroupName = getIntent().getStringExtra(AppConstants.GROUP_NAME);
                //  tvName.setText(CommonUtils.NameCaps(groupName));
                tvName.setText(groupName);
                Log.e("GROUP_NODE", "GROUP_NODE" + chatId);
            }
            if (getIntent().getStringExtra(AppConstants.GROUP_ID) != null) {

                group_id = getIntent().getStringExtra(AppConstants.GROUP_ID);
                notificationReceiverid = getIntent().getStringExtra(AppConstants.GROUP_ID);

                chatId = "Group" + "_" + group_id;

                Log.e("group_id", "group_id" + group_id);
                Log.e("notificationReceiverid", "notificationReceiverid" + notificationReceiverid);
            }
            if (getIntent().getStringExtra(AppConstants.GROUP_PIC) != null) {

                receiverPic = getIntent().getStringExtra(AppConstants.GROUP_PIC);

                Picasso.with(mContext).load(getIntent().getStringExtra(AppConstants.GROUP_PIC)).error(R.drawable.placeholder_user).into(ivuserBackground);


            }
            if (getIntent().getStringExtra(AppConstants.GROUP__USER_NAME) != null) {
                group_user_name = getIntent().getStringExtra(AppConstants.GROUP__USER_NAME);

                //  tvStatusTime.setText(group_user_name);
                Log.e("group_user_name", "group_user_name" + group_user_name);
            }
            //   Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();


        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {


            //   Toast.makeText(mContext, "recent_group_chat", Toast.LENGTH_SHORT).show();


            if (getIntent().getStringExtra(AppConstants.GROUP_NODE) != null) {

                // chatId = getIntent().getStringExtra(AppConstants.GROUP_NODE);
                nodeId = getIntent().getStringExtra(AppConstants.GROUP_NODE);
                Log.e("GROUP_NODE", "GROUP_NODE" + chatId);
            }

            if (getIntent().getStringExtra(AppConstants.GROUP_NAME) != null) {

                groupName = getIntent().getStringExtra(AppConstants.GROUP_NAME);
                firebaseUsergroupName = getIntent().getStringExtra(AppConstants.GROUP_NAME);
                //    tvName.setText(CommonUtils.NameCaps(groupName));
                tvName.setText(groupName);
            }
            if (getIntent().getStringExtra(AppConstants.GROUP_ID) != null) {

                //  group_id = getIntent().getStringExtra(AppConstants.FIREBASE_GROUP_ID);

                chatId = getIntent().getStringExtra(AppConstants.GROUP_ID);
                notificationReceiverid = getIntent().getStringExtra(AppConstants.FIREBASE_GROUP_ID);


                // chatId="Group"+"_"+ group_id;
                // chatId= group_id;
                Log.e("ActivitychatId", "ActivitychatId::" + chatId);
                Log.e("notificationReceiverid", "notificationReceiverid::" + notificationReceiverid);
                Log.e("group_id", "group_id" + group_id);
            }
            if (getIntent().getStringExtra(AppConstants.GROUP_PIC) != null) {

                receiverPic = getIntent().getStringExtra(AppConstants.GROUP_PIC);

                Picasso.with(mContext).load(getIntent().getStringExtra(AppConstants.GROUP_PIC)).error(R.drawable.placeholder_user).into(ivuserBackground);

            }


            if (getIntent().getStringExtra(AppConstants.GROUP__USER_NAME) != null) {

                group_user_name = getIntent().getStringExtra(AppConstants.GROUP__USER_NAME);
            }


        } else {
            //  Toast.makeText(mContext, "both", Toast.LENGTH_SHORT).show();


            if (getIntent().getStringExtra(AppConstants.RECEIVER_ID) != null) {
                Log.e("RTPYE", "RTPYE" + RTPYE);
                RTPYE = Integer.parseInt(getIntent().getStringExtra(AppConstants.RECEIVER_ID));
                notificationReceiverid = getIntent().getStringExtra(AppConstants.RECEIVER_ID);
                Log.e("notificationReceiverid", "notificationReceiverid" + notificationReceiverid);

            }
            if (getIntent().getStringExtra(AppConstants.RECEIVER__USER_NAME2) != null) {
                reciver_name = getIntent().getStringExtra(AppConstants.RECEIVER__USER_NAME2);
                firebaseUsergroupName = getIntent().getStringExtra(AppConstants.RECEIVER__USER_NAME2);

            }

            Log.e("RTPYE", "RTPYE" + RTPYE);
            Log.e("STPYE", "STPYE" + STPYE);
            //  Log.e("RTPYE","RTPYE"+STPYE);

            if (RTPYE > STPYE) {

                chatId = STPYE + "_" + RTPYE;
            } else {

                chatId = RTPYE + "_" + STPYE;
            }

            CommonUtils.savePreferencesString(mContext, AppConstants.SENDER_RECEIVER_ID, chatId);

            tvName.setText(CommonUtils.NameCaps(reciver_name));


            if (!TextUtils.isEmpty(getIntent().getStringExtra(AppConstants.RECEIVER__PIC)) && getIntent().getStringExtra(AppConstants.RECEIVER__PIC) != null) {

                // Toast.makeText(mContext, "bothRECEIVER__PIC", Toast.LENGTH_SHORT).show();
                receiverPic = getIntent().getStringExtra(AppConstants.RECEIVER__PIC);

                Picasso.with(mContext).load(getIntent().getStringExtra(AppConstants.RECEIVER__PIC)).error(R.drawable.placeholder_user).into(ivuserBackground);


                Log.e("profilePicReciever", receiverPic);
                Log.e("profileSender", sender_img);
                Log.e("sender_img_server", sender_img_server);
            }

        }

        //TODO  getting Data from intent
        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {

            rl_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {
            rl_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            //   Toast.makeText(mContext, "recent_group_chat", Toast.LENGTH_SHORT).show();
        } else {
            rl_header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(mContext, "CHAT_USER", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(mContext, UIChatProfileActivity.class);
                    i.putExtra("CHAT_USER", notificationReceiverid);
                    i.putExtra("FROM_CHAT_USER", "FROM_CHAT_USER");
                    startActivity(i);
                    finish();
                }
            });


        }

        callOnOffStatus("1");
        callGetOnOffStatus();
        callResetMsgCount();
        callCountMsg();

        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //    Toast.makeText(mContext, "back", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        setListners();

        chatView.setAttachmentListener(isCam -> {
            captureImage(isCam);
        });
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        File localFile = null;


        storageReference = FirebaseStorage.getInstance().getReference();
        try {
            localFile = File.createTempFile("SatyaImage", "jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        storageReference.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                        Toast.makeText(mContext, "satyaImage", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
            }
        });
        Log.e("chatId", "chatId::" + chatId);
        FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getValue() != null) {

                    //   CommonUtils.dismissProgress();

                    //  Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();

                    setAllMsgList(dataSnapshot);
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //  Toast.makeText(mContext, "child"+dataSnapshot, Toast.LENGTH_SHORT).show();
                if (dataSnapshot.getValue() != null) {
                    //  Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();
                    HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                    String type = (String) mapMessage.get("type");
                    String senderId = (String) mapMessage.get("sender_id");
                    String timestamp = firebaseTimestamp.get(deletePosition).timestamp;

                    if (senderId.equalsIgnoreCase(String.valueOf(STPYE))) {
                        if (type != null && type.equalsIgnoreCase("0")) {

                            if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                                ChatMessage chatMessage = new ChatMessage((String) mapMessage.get("sender_name"), "You deleted this message", Long.valueOf(timestamp), (String) mapMessage.get("type") + "", 1, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1, "0", (String) mapMessage.get("isForwardedMsg"));
                                chatView.setAdapter(deletePosition, chatMessage);

                            } else {
                                ChatMessage chatMessage = new ChatMessage((String) mapMessage.get("sender_name"), "You deleted this message", Long.valueOf(timestamp), (String) mapMessage.get("type") + "", 0, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1, "0", (String) mapMessage.get("isForwardedMsg"));
                                chatView.setAdapter(deletePosition, chatMessage);
                            }
                        }
                    } else {

                        if ((type).equalsIgnoreCase("Group")) {

                            //  Toast.makeText(mContext, "child", Toast.LENGTH_SHORT).show();
                            chatId = (String) mapMessage.get("chatId");
                        } else {
                        }


                        int changedPostion = 0;
                        for (int i = 0; i < firebaseTimestamp.size(); i++) {
                            if (firebaseTimestamp.get(i).timestamp.equalsIgnoreCase((String) mapMessage.get("timestamp"))) {
                                changedPostion = i;
                                //   Toast.makeText(mContext, "position::"+changedPostion, Toast.LENGTH_SHORT).show();
                            }
                        }
                        if (type != null && type.equalsIgnoreCase("0")) {
                            ChatMessage chatMessage = new ChatMessage((String) mapMessage.get("sender_name"),
                                    "This message was deleted",
                                    Long.valueOf(timestamp),
                                    (String) mapMessage.get("message"),
                                    0, ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0,
                                    "0", (String) mapMessage.get("isForwardedMsg")
                            );
                            chatView.setAdapter(changedPostion, chatMessage);
                        }

                    }

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setAllMsgList(DataSnapshot dataSnapshot) {

        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
        String type = (String) mapMessage.get("type");
        Message newMessage = new Message();
        newMessage.type = (String) mapMessage.get("type");
        newMessage.sender_id = (String) mapMessage.get("sender_id");
        newMessage.reciever_id = (String) mapMessage.get("reciever_id");
        if (newMessage.type.equalsIgnoreCase("3")) {
            newMessage.filename = (String) mapMessage.get("filename");
            newMessage.fileSize = (String) mapMessage.get("fileSize");
        }
        receiverid = (String) mapMessage.get("reciever_id");
        senderid = (String) mapMessage.get("sender_id");
        newMessage.message = (String) mapMessage.get("message");
        newMessage.timestamp = (String) mapMessage.get("timestamp");
        newMessage.time = (String) mapMessage.get("time");

        if (newMessage.timestamp.trim().length() <= 11) {
            newMessage.timestamp += "000";
        }

        firebaseTimestamp.add(newMessage);

        //TODO for set data for  SENDER

        if (senderid.equalsIgnoreCase(String.valueOf(STPYE))) {

            if (type != null && type.equalsIgnoreCase("0")) {
                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {
                } else {

                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {

                        //  Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();

                        /* (String) mapMessage.get("message"*/
                        chatView.addMessage(new ChatMessage(
                                (String) mapMessage.get("sender_name"), "You deleted this message"
                                ,
                                Long.valueOf((String) mapMessage.get("timestamp")),
                                (String) mapMessage.get("type") + "", 1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1,
                                "0", (String) mapMessage.get("isForwardedMsg")));

                    } else {

                        chatView.addMessage(new ChatMessage(
                                (String) mapMessage.get("message"),
                                Long.valueOf((String) mapMessage.get("timestamp")),
                                1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1,
                                "1",
                                (String) mapMessage.get("isForwardedMsg")
                        ));


                    }

                }


            } else if (type != null && type.equalsIgnoreCase("1")) {
                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {

                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        //  Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //  Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                        chatView.addMessage(new ChatMessage(
                                (String) mapMessage.get("sender_name"),
                                (String) mapMessage.get("message"),
                                Long.valueOf((String) mapMessage.get("timestamp")),
                                "",
                                1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1));

                    } else {
                        chatView.addMessage(new ChatMessage(
                                (String) mapMessage.get("message"),
                                Long.valueOf((String) mapMessage.get("timestamp")),
                                1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1,
                                "1",
                                (String) mapMessage.get("isForwardedMsg"))
                        );
                    }

                }

            } else if (type != null && type.equalsIgnoreCase("2")) {

                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {

                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        // Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();

                        String timestamp = (String) mapMessage.get("timestamp");

                        chatMessage = new ChatMessage((String) mapMessage.get("sender_name"), "", Long.valueOf(timestamp), (String) mapMessage.get("message"), 1, ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);
                        chatView.addMessage(chatMessage);
                        chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                    } else {

                        Log.e("file", "fileFirebase" + (String) mapMessage.get("message"));
                        String downloadimage = (String) mapMessage.get("message");
                        String downloadFile = downloadimage.substring(downloadimage.lastIndexOf("/") + 1);
                        Log.e("senderChar::", "senderChar::" + mapMessage.get("sender_name"));
                        String timestamp = (String) mapMessage.get("timestamp");

                        chatMessage = new ChatMessage(
                             (String) mapMessage.get("sender_name"),
                                "",
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"),
                                0,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3,
                             "2",
                             "",
                             "2",
                             (String)mapMessage.get("isForwardedMsg")
                        );
                        chatView.addMessage(chatMessage);
                        chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                    }

                }

            } else if (type != null && type.equalsIgnoreCase("3")) {


                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {
                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        // Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        // Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("sender_name"),
                                (String) mapMessage.get("filename"),
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"), 1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9, (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));

                    } else {
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage(
                                (String) mapMessage.get("filename"),
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"),
                                0,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9,
                                "3",
                                (String) mapMessage.get("fileSize"),
                               "3",
                                (String)mapMessage.get("isForwardedMsg")

                        ));

                    }
                }
            } else if (type != null && type.equalsIgnoreCase("4")) {
                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {
                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        // Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        // Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("sender_name"),
                                (String) mapMessage.get("filename"),
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"), 1,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7, (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));

                    } else {
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("message"),
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"),
                                0,
                                ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7,
                                (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));

                    }
                }
            }

        }

        //TODO  set data for  Admin Add someOne

        else if (senderid.equalsIgnoreCase("1")) {

            //  if (type != null && type.equalsIgnoreCase("5")) {

            // Toast.makeText(mContext, "555", Toast.LENGTH_SHORT).show();
            if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

            } else {

                String timestamp = (String) mapMessage.get("timestamp");
                if (newMessage.timestamp.trim().length() <= 11) {
                    newMessage.timestamp += "000";
                }
                //  Toast.makeText(mContext, "555:::"+(String) mapMessage.get("message"), Toast.LENGTH_SHORT).show();
                chatView.addMessage(new ChatMessage(
                        (String) mapMessage.get("message"),
                        Long.valueOf(timestamp),
                         1,
                        ChatMessage.Type.TYPE_VIEW_ADMIN_ADD_DELETE,
                        "1",
                        (String) mapMessage.get("isForwardedMsg")));


            }


            // }
        }

        //TODO for set data for  RECEIVER

        else {
            if (type != null && type.equalsIgnoreCase("1")) {
                if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                    // Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                    //    Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                    String timestamp = (String) mapMessage.get("timestamp");
                    if (newMessage.timestamp.trim().length() <= 11) {
                        newMessage.timestamp += "000";
                    }
                    chatView.addMessage(new ChatMessage(
                            (String) mapMessage.get("sender_name"),
                            (String) mapMessage.get("message"),
                            Long.valueOf(timestamp),
                            receiverPic + "",
                            1,
                            ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0)
                    );
                } else {
                    // Toast.makeText(mContext, "typeUser::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                    //  Toast.makeText(mContext, "senderUser::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                    String timestamp = (String) mapMessage.get("timestamp");
                    if (newMessage.timestamp.trim().length() <= 11) {
                        newMessage.timestamp += "000";
                    }
                    chatView.addMessage(new ChatMessage((String) mapMessage.get("message"),
                            Long.valueOf((String) mapMessage.get("timestamp")),
                                    1,
                                    ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0,
                                    (String) mapMessage.get("type"),
                                    (String) mapMessage.get("isForwardedMsg")
                            )
                    );

                }

            } else if (type != null && type.equalsIgnoreCase("2")) {

                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {
                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        //  Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //    Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();

                        String timestamp = (String) mapMessage.get("timestamp");

                        chatMessage = new ChatMessage((String) mapMessage.get("reciever_name"), "", Long.valueOf(timestamp), (String) mapMessage.get("message"), 1, ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2);
                        chatView.addMessage(chatMessage);
                        chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                    } else {
                        Log.e("file", "fileFirebase" + (String) mapMessage.get("message"));
                        String downloadimage = (String) mapMessage.get("message");
                        String downloadFile = downloadimage.substring(downloadimage.lastIndexOf("/") + 1);
                        String timestamp = (String) mapMessage.get("timestamp");
                        // chatMessage = new ChatMessage("", System.currentTimeMillis(), (String) mapMessage.get("message"), 0, ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2);
                      /*  chatMessage = new ChatMessage(
                                (String) mapMessage.get("sender_name"),
                                receiverPic + "",
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message") + "",
                                0,
                                ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2


                        );*/

                        chatMessage = new ChatMessage(
                                (String) mapMessage.get("sender_name"),
                                receiverPic + "",
                                Long.valueOf(timestamp),
                                (String) mapMessage.get("message"),
                                0,
                                ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2,
                                "2",
                                "",
                                "2",
                                (String)mapMessage.get("isForwardedMsg")
                        );


                        chatView.addMessage(chatMessage);
                        chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                    }

                }

            } else if (type != null && type.equalsIgnoreCase("3")) {

                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {

                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        //  Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("sender_name"),
                                (String) mapMessage.get("filename"),
                                Long.valueOf(timestamp), (String) mapMessage.get("message"), 1, ChatMessage.Type.TYPE_VIEW_REC_MSG_DOC_8, (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));

                    } else {

                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }

                        //   Toast.makeText(mContext, "filename::"+(String) mapMessage.get("filename"), Toast.LENGTH_SHORT).show();
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("filename") + "",
                                Long.valueOf(timestamp), (String) mapMessage.get("message"), 0, ChatMessage.Type.TYPE_VIEW_REC_MSG_DOC_8, receiverPic + "", (String) mapMessage.get("fileSize")));

                    }
                }
            } else if (type != null && type.equalsIgnoreCase("4")) {

                if (chatMsg.equalsIgnoreCase((String) mapMessage.get("message"))) {

                } else {
                    if (((String) mapMessage.get("chatType")).equalsIgnoreCase("Group")) {
                        //  Toast.makeText(mContext, "typeGroup::"+(String) mapMessage.get("chatType"), Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(mContext, "senderGroup::"+(String) mapMessage.get("sender_name"), Toast.LENGTH_SHORT).show();
                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        chatView.addMessage(new ChatMessage((String) mapMessage.get("sender_name"),
                                (String) mapMessage.get("message"),
                                Long.valueOf(timestamp), (String) mapMessage.get("message"), 1, ChatMessage.Type.TYPE_VIEW_REC_MSG_AUDIO_6, (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));

                    } else {

                        String timestamp = (String) mapMessage.get("timestamp");
                        if (newMessage.timestamp.trim().length() <= 11) {
                            newMessage.timestamp += "000";
                        }
                        //   Toast.makeText(mContext, "filename::"+(String) mapMessage.get("filename"), Toast.LENGTH_SHORT).show();
                        chatView.addMessage(new ChatMessage(receiverPic + "",
                                Long.valueOf(timestamp), (String) mapMessage.get("message") + "", 0, ChatMessage.Type.TYPE_VIEW_REC_MSG_AUDIO_6, (String) mapMessage.get("filename"), (String) mapMessage.get("fileSize")));
                    }
                }
            }

        }

    }
    private void getToken() {
        fireBaseKey = CommonUtils.getPreferencesString(mContext, AppConstants.FIREBASE_KEY);
        if (fireBaseKey != null && fireBaseKey.trim().equals("")) {
            Log.e("FCMKEY", "FCMKEy:::" + fireBaseKey);
            return;
        } else {
            fireBaseKey = FirebaseInstanceId.getInstance().getToken();
            CommonUtils.savePreferencesString(mContext, AppConstants.FIREBASE_KEY, fireBaseKey);
            Log.e("FCMKEY", "elseFCMKEy:::" + fireBaseKey);
            if (fireBaseKey == null) {
                //   Toast.makeText(mContext, "Please Try Again", Toast.LENGTH_SHORT).show();
                return;
            }

        }

    }

    private void callOnOffStatus(String type) {

        String user_id = CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("Status", type);
        // FirebaseDatabase.getInstance().getReference().child("UserStatus").child(user_id).updateChildren((objectMap));

        FirebaseDatabase.getInstance().getReference().child("UserStatus").child(user_id).child(chatId).setValue(objectMap);
    }

    private void callResetMsgCount() {


        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("msgCount", 0);
        FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(user_id).child(chatId).child("messages/").updateChildren((objectMap));

    }

    private void callGetOnOffStatus() {

        FirebaseDatabase.getInstance().getReference().child("UserStatus").child(notificationReceiverid).child(chatId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.getValue() != null) {
                    // Toast.makeText(mContext, "dataSnapshot::" + dataSnapshot, Toast.LENGTH_SHORT).show();
                    String message = (String) dataSnapshot.getValue();
                    if (message != null) {
                        userScreenStatus = message;
                        //  Toast.makeText(mContext, "userScreenStatus" + userScreenStatus, Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                if (dataSnapshot.getValue() != null) {
                    // Toast.makeText(mContext, "dataSnapshot::" + dataSnapshot, Toast.LENGTH_SHORT).show();
                    String message = (String) dataSnapshot.getValue();
                    if (message != null) {
                        userScreenStatus = message;
                        //  Toast.makeText(mContext, "userScreenStatus" + userScreenStatus, Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private int callCountMsg() {
        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
            int k = 0;
            List<String> group_user_list = new ArrayList<>();
            for (int i = 0; i < nodeId.length(); i++) {
                group_user_list = Arrays.asList(nodeId.split("_"));

            }

            //TODO Create UpdateList
            group_arrayCountList = new ArrayList<>();

            for (k = 0; k < group_user_list.size(); k++) {
                group_arrayCountList.add(0);
                group_arrayCountList.set(k, 0);
                String group_receiver_idArray = group_user_list.get(k).toString();

                if (group_receiver_idArray.equalsIgnoreCase(user_id)) {

                } else {

                    int finalK = k;
                    FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(group_receiver_idArray).child(chatId).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            if (dataSnapshot.getValue() != null) {
                                HashMap mapMessage = (HashMap) dataSnapshot.getValue();


                                try {
                                    Long count = (Long) mapMessage.get("msgCount");
                                    Integer firebasecount = count != null ? count.intValue() : null;
                                    serverCount = firebasecount;
                                    group_arrayCountList.set(finalK, serverCount);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //   group_arrayCountList.add(finalK,serverCount);
                                Log.e("arrayCountList", "counrgroup_arrayCountList" + group_arrayCountList);

                                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            if (dataSnapshot.getValue() != null) {
                                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                                try {
                                    Long count = (Long) mapMessage.get("msgCount");
                                    Integer firebasecount = count != null ? count.intValue() : null;
                                    serverCount = firebasecount;
                                    group_arrayCountList.set(finalK, serverCount);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //   group_arrayCountList.add(finalK,serverCount);
                                Log.e("ChatActivityChangesd", "ChatActivityChangesd" + group_arrayCountList);

                                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }


        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {

            int k = 0;

            List<String> group_user_list = new ArrayList<>();
            for (int i = 0; i < nodeId.length(); i++) {
                group_user_list = Arrays.asList(nodeId.split("_"));

            }

            //TODO Create UpdateList
            group_arrayCountList = new ArrayList<>();


            for (k = 0; k < group_user_list.size(); k++) {
                group_arrayCountList.add(0);
                group_arrayCountList.set(k, 0);
                String group_receiver_idArray = group_user_list.get(k).toString();

                if (group_receiver_idArray.equalsIgnoreCase(user_id)) {

                } else {
                    int finalK = k;
                    FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(group_receiver_idArray).child(chatId).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            if (dataSnapshot.getValue() != null) {
                                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                                Long count = (Long) mapMessage.get("msgCount");
                                try {
                                    Integer firebasecount = count != null ? count.intValue() : null;
                                    serverCount = firebasecount;
                                    group_arrayCountList.set(finalK, serverCount);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //   group_arrayCountList.add(finalK,serverCount);
                                Log.e("arrayCountList", "counrgroup_arrayCountList" + group_arrayCountList);

                                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            if (dataSnapshot.getValue() != null) {
                                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                                Long count = (Long) mapMessage.get("msgCount");
                                try {
                                    Integer firebasecount = count != null ? count.intValue() : null;
                                    serverCount = firebasecount;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                group_arrayCountList.set(finalK, serverCount);
                                //   group_arrayCountList.add(finalK,serverCount);
                                Log.e("ChatActivityChangesd", "ChatActivityChangesd" + group_arrayCountList);

                                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                            }


                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

        } else {
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(notificationReceiverid).child(chatId).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                  /*  if (dataSnapshot.getValue() != null) {
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        Long count = (Long) mapMessage.get("msgCount");
                        Integer firebasecount = count != null ? count.intValue() : null;
                        serverCount = firebasecount;
                        Log.e("counr", "counr" + firebasecount);

                        //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                    }*/
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                  /*  if (dataSnapshot.getValue() != null) {
                        HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                        Long count = (Long) mapMessage.get("msgCount");
                        Integer firebasecount = count != null ? count.intValue() : null;
                        serverCount = firebasecount;
                        Log.e("counr", "counr" + firebasecount);

                        //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
                    }*/
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

        return serverCount;
    }

    public void eventDialog() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mDialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        mDialog.getWindow().setAttributes(lp);
        View dialoglayout = inflater.inflate(R.layout.custommore, null);
        mDialog.setContentView(dialoglayout);
        setting = (TextView) mDialog.findViewById(R.id.setting);
        newgroup = (TextView) mDialog.findViewById(R.id.newgroup);
        newbroadcast = (TextView) mDialog.findViewById(R.id.newbroadcast);
        whatsappweb = (TextView) mDialog.findViewById(R.id.whatsappweb);
        starredmessage = (TextView) mDialog.findViewById(R.id.starredmeass);
        mDialog.show();
        newbroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, NewBRoasdCast.class));
                mDialog.dismiss();

            }
        });
        whatsappweb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, WhatsAppWebActivity.class));
                mDialog.dismiss();

            }
        });
        starredmessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, Starredmessage.class));
                mDialog.dismiss();
            }
        });

        newgroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, NewGroup.class));
                mDialog.dismiss();

            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, Setting.class));
                mDialog.dismiss();
            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();
        callOnOffStatus("0");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        callOnOffStatus("0");
    }


    private void setListners() {
        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
            @Override
            public boolean sendMessage(ChatMessage chatMessage) {
                globalTimeStamp = System.currentTimeMillis();
                if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
                    // Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();
                    Message newMessage = new Message();
                    sendermsg = chatMessage.getMessage();
                    newMessage.type = "1";
                    newMessage.message = chatMessage.getMessage();
                    newMessage.sender_id = String.valueOf(STPYE);
                    newMessage.reciever_id = nodeId;
                    newMessage.chatType = "Group";
                    newMessage.reciever_name = groupName;
                    newMessage.reciever_img = "";
                    newMessage.sender_name = user_name;
                    newMessage.sender_img = "";
                    newMessage.timestamp = String.valueOf(globalTimeStamp);
                    newMessage.time = CommonUtils.getDate(globalTimeStamp);
                    chatMsg = chatMessage.getMessage();
                    // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
                    FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage);
                    callUpdateMsgListUser(chatMessage.getMessage(), "1");
                    String login_auth = "";
                    String user_name = "";
                    String chatMsgWithUser = "";
                    if (chatMsg != null && !chatMsg.equalsIgnoreCase("")) {
                        if (CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE) != null && (CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) != null))
                            login_auth = CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE);
                        user_name = CommonUtils.getPreferences(mContext, AppConstants.USER_NAME);
                        chatMsgWithUser = user_name + " : " + chatMsg;
                        // callNotificationApi(login_auth,user_name,"group", notificationReceiverid,chatMsgWithUser);
                        callNotificationApi2(login_auth, user_name, "group", notificationReceiverid, chatMsgWithUser);
                    }

                } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {
                    // Toast.makeText(mContext, "recent_group", Toast.LENGTH_SHORT).show();
                    Message newMessage = new Message();
                    sendermsg = chatMessage.getMessage();
                    newMessage.type = "1";
                    newMessage.message = chatMessage.getMessage();
                    newMessage.sender_id = String.valueOf(STPYE);
                    newMessage.reciever_id = nodeId;
                    newMessage.chatType = "Group";
                    newMessage.reciever_name = groupName;
                    newMessage.reciever_img = "";
                    newMessage.sender_name = user_name;
                    newMessage.sender_img = "";
                    newMessage.timestamp = String.valueOf(globalTimeStamp);
                    newMessage.time = CommonUtils.getDate(globalTimeStamp);
                    chatMsg = chatMessage.getMessage();
                    // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
                    FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage);
                    callUpdateMsgListUser(chatMessage.getMessage(), "1");
                    String login_auth = "";
                    String user_name = "";
                    String chatMsgWithUser = "";
                    if (chatMsg != null && !chatMsg.equalsIgnoreCase("")) {
                        if (CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE) != null && (CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) != null))
                            login_auth = CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE);
                        user_name = CommonUtils.getPreferences(mContext, AppConstants.USER_NAME);
                        chatMsgWithUser = user_name + " : " + chatMsg;
                        // callNotificationApi(login_auth,user_name,"group", notificationReceiverid,chatMsgWithUser);
                        callNotificationApi2(login_auth, user_name, "group", notificationReceiverid, chatMsgWithUser);
                    }


                } else {


                    //   Toast.makeText(mContext, "single", Toast.LENGTH_SHORT).show();

                    Message newMessage = new Message();
                    sendermsg = chatMessage.getMessage();
                    newMessage.type = "1";
                    newMessage.message = chatMessage.getMessage();
                    newMessage.sender_id = String.valueOf(STPYE);
                    newMessage.reciever_id = String.valueOf(RTPYE);
                    newMessage.chatType = "oneToOne";
                    newMessage.reciever_name = reciver_name;
                    newMessage.reciever_img = receiverPic;
                    newMessage.sender_name = user_name;
                    newMessage.sender_img = sender_img_server;
                    newMessage.timestamp = String.valueOf(globalTimeStamp);
                    newMessage.time = CommonUtils.getDate(globalTimeStamp);
                    chatMsg = chatMessage.getMessage();
                    // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
                    FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage);

                    //TODO Create UpdateList
                    Message OpponentMsg = new Message();
                    sendermsg = chatMessage.getMessage();
                    OpponentMsg.type = "1";
                    OpponentMsg.message = chatMessage.getMessage();
                    OpponentMsg.oponent_id = String.valueOf(RTPYE);
                    OpponentMsg.oponent_name = reciver_name;
                    OpponentMsg.chatId = chatId;
                    OpponentMsg.chatType = "oneToOne";
                    OpponentMsg.oponent_img = receiverPic;
                    OpponentMsg.timestamp = String.valueOf(globalTimeStamp);
                    OpponentMsg.time = CommonUtils.getDate(globalTimeStamp);
                    chatMsg = chatMessage.getMessage();
                    FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(STPYE)).child(chatId).child("messages/").setValue(OpponentMsg);


                    Message MyMsg = new Message();
                    sendermsg = chatMessage.getMessage();
                    MyMsg.type = "1";
                    MyMsg.message = chatMessage.getMessage();
                    MyMsg.oponent_id = String.valueOf(STPYE);
                    MyMsg.chatId = chatId;
                    MyMsg.chatType = "oneToOne";

                    if (userScreenStatus.equalsIgnoreCase("1")) {
                        MyMsg.msgCount = 0;
                        Log.e("singleCountNot", "singleCountNot" + msgLocalCount);
                        Log.e("singleCountNot", "singleCountNot:::" + MyMsg.msgCount);
                        Log.e("singleCount", "singleCount:::" + userScreenStatus);

                    } else {
                        msgLocalCount = serverCount;
                        msgLocalCount++;
                        MyMsg.msgCount = msgLocalCount;
                        Log.e("singleCount", "singleCount" + msgLocalCount);
                        Log.e("singleCount", "singleCount:::" + MyMsg.msgCount);
                        Log.e("singleCount", "singleCount:::" + userScreenStatus);

                    }
                    MyMsg.oponent_name = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
                    MyMsg.oponent_img = CommonUtils.getPreferencesString(mContext, AppConstants.SENDER_PROFILE_PIC_server);
                    MyMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
                    MyMsg.timestamp = String.valueOf(System.currentTimeMillis());
                    chatMsg = chatMessage.getMessage();
                    // FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue((Map<String, Object>)MyMsg);

                    FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue((MyMsg));
                    String login_auth = "";
                    String user_name = "";
                    String chatMsgWithUser = "";
                    if (chatMsg != null && !chatMsg.equalsIgnoreCase("")) {

                        if (CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE) != null && (CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) != null))
                            login_auth = CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE);
                        user_name = CommonUtils.getPreferences(mContext, AppConstants.USER_NAME);
                        chatMsgWithUser = user_name + " : " + chatMsg;

                        callNotificationApi2(login_auth, user_name, "Single", notificationReceiverid, chatMsgWithUser);

                       /* try {
                            callNotificationApi2(login_auth,user_name,"Single", notificationReceiverid,chatMsgWithUser);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    }

                }
                return true;
            }
        });
    }

    private void callUpdateMsgListUser(String message, String type) {

        List<String> group_user_list = new ArrayList<>();
        for (int i = 0; i < nodeId.length(); i++) {
            group_user_list = Arrays.asList(nodeId.split("_"));

        }

        //  Toast.makeText(mContext, "group_list"+group_user_list.size(), Toast.LENGTH_SHORT).show();

        //TODO Create UpdateList

        for (int i = 0; i < group_user_list.size(); i++) {
            Message OpponentMsg = new Message();
            String sender_id = group_user_list.get(i).toString();
            OpponentMsg.type = type;
            OpponentMsg.message = message;
            //  OpponentMsg.oponent_id = chatId;
            OpponentMsg.oponent_id = nodeId;
            OpponentMsg.chatId = chatId;
            OpponentMsg.group_id = notificationReceiverid;
            OpponentMsg.oponent_name = groupName;
            OpponentMsg.chatType = "Group";

            if (user_id.equalsIgnoreCase(sender_id)) {


            } else {
                serverCount = group_arrayCountList.get(i);
                serverCount++;
                OpponentMsg.msgCount = serverCount;

                Log.e("OpponentMsg.msgCount", "OpponentMsg.msgCount" + OpponentMsg.msgCount);
            }






         /*   if(userScreenStatus.equalsIgnoreCase("1")) {
                OpponentMsg.msgCount = 0;
                Log.e("singleCountNot","singleCountNot"+msgLocalCount);
                Log.e("singleCountNot","singleCountNot:::"+OpponentMsg.msgCount);
                Log.e("singleCount","singleCount:::"+userScreenStatus);

            }
            else {
                msgLocalCount= serverCount;
                msgLocalCount++;
                OpponentMsg.msgCount = msgLocalCount;
                Log.e("singleCount","singleCount"+msgLocalCount);
                Log.e("singleCount","singleCount:::"+OpponentMsg.msgCount);
                Log.e("singleCount","singleCount:::"+userScreenStatus);

            }*/


            OpponentMsg.oponent_img = receiverPic;
            OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
            OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            chatMsg = message;
            // FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(sender_id)).child(chatId).child("messages/").updateChildren((Map<String, Object>) OpponentMsg);
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(sender_id)).child(chatId).child("messages/").setValue(OpponentMsg);

        }
    }

    public void captureImage(boolean isCam) {

        if (isCam) {

            Toast.makeText(this, "click", Toast.LENGTH_SHORT).show();

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                CommonUtils.hideKeyPad((Activity) mContext);
                CommonUtils.hide_keyboard((Activity) mContext);

                dialogSlectMedia();
                //    init_persistent_bottomsheet();

                //    addThemToViewDialog();

            }

        }
    }

    void dialogSlectMedia() {
        LinearLayout llCamera, llPhotos, llDoc;
        ImageView ivBack;
        dialog = new Dialog(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.gallery_popup_profile_pic, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        //Window window =this.getWindow();
        // window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        // int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.98);
        // int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.98);
        int height = WindowManager.LayoutParams.WRAP_CONTENT;
        int width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(view);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        llCamera = (LinearLayout) dialog.findViewById(R.id.llCamera);
        llPhotos = (LinearLayout) dialog.findViewById(R.id.llPhotos);
        llDoc = (LinearLayout) dialog.findViewById(R.id.llDoc);
        ivBack = (ImageView) dialog.findViewById(R.id.ivBack);
        llBorder.setBackgroundColor(getResources().getColor(R.color.black));
        dialog.show();
        ivBack.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                            }
        });

        llPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent1 = new Intent(mContext, ImagePickActivity.class);
                intent1.putExtra(IS_NEED_CAMERA, true);
                intent1.putExtra(Constant.MAX_NUMBER, 9);
                intent1.putExtra(IS_NEED_FOLDER_LIST, true);


                startActivityForResult(intent1, Constant.REQUEST_CODE_PICK_IMAGE);
                //behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                dialog.dismiss();
            }
        });
        llDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent4 = new Intent(mContext, NormalFilePickActivity.class);
                intent4.putExtra(Constant.MAX_NUMBER, 9);
                intent4.putExtra(IS_NEED_FOLDER_LIST, true);
                intent4.putExtra(NormalFilePickActivity.SUFFIX,
                        new String[]{"xlsx", "xls", "doc", "dOcX", "ppt", ".pptx", "pdf"});
                startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE);

                // behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                dialog.dismiss();

            }
        });


        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraPermissionMethod();
                // behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                dialog.dismiss();
            }
        });


    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    private void cameraPermissionMethod() {
        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }

        // }
    }

    protected boolean checkPermission(String[] permission) {

        boolean isPermission = true;

        for (String s : permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this, s) == PackageManager.PERMISSION_GRANTED;
            return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }

    private void onPermissionResult(int requestCode, boolean b) {
    }

    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }

    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, "SatyaConnect");
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry., There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(
                        mContext,
                        getApplicationContext()
                                .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList =
                            getPackageManager()
                                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }

    //For Video Integration
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    //   mCurrentPhotoUri=getImageContentUri(mContext,mCurrentPhotoPath);
                    Log.e("mCurrentPhotoPath", "mCurrentPhotoPath" + mCurrentPhotoUri);
                    Log.e("imageCaht", "imageCaht" + mCurrentPhotoPath);
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);


                    //    uploadImageData("2",mCurrentPhotoUri);
                    break;
                //   /*Madhu*/  galleryOperation(data);
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {
                        photoPaths.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");

                        mCurrentPhotoUri = getImageContentUri(mContext, mCurrentPhotoPath);
                        filename = mCurrentPhotoPath.substring(mCurrentPhotoPath.lastIndexOf("/") + 1);

                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        isFileImg = true;
                        uploadImageData("2", mCurrentPhotoUri);

                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;


                case Constant.REQUEST_CODE_PICK_IMAGE:
                    if (resultCode == RESULT_OK) {
                        ArrayList<ImageFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                        StringBuilder builder = new StringBuilder();
                        for (ImageFile file : list) {
                            String path = file.getPath();

                            builder.append(path + "\n");
                            // imagelist.add(path);
                            Log.e("imageCaht", "imageCaht" + path);
                            mCurrentPhotoUri = getImageContentUri(mContext, path);

                            Log.e("pathuri", "pathuri::" + mCurrentPhotoUri);
                            filename = path.substring(path.lastIndexOf("/") + 1);
                            //ChatMessage chatMessage=new ChatMessage("",path,timestamp,ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);

                            //    ivProgress.setVisibility(View.VISIBLE);
                            uploadImageData("2", mCurrentPhotoUri);
                        }
                    }
                    break;


                case AppConstants.AUDIO_CALL_STATUS:
                    if (resultCode == RESULT_OK) {
                        //   Toast.makeText(mContext, " Audio:::" +data.getStringExtra("AUDIO_PATH"), Toast.LENGTH_SHORT).show();
                        Uri fileUri = Uri.fromFile(new File(data.getStringExtra("AUDIO_PATH")));
                        Log.e(TAG, "onActivityResult: AudioPath::" + fileUri);
                        uploadAudioData("4", fileUri);

                    }
                    break;
                case Constant.REQUEST_CODE_PICK_FILE:

                    if (resultCode == RESULT_OK) {
                        ArrayList<NormalFile> list = data.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                        StringBuilder builder = new StringBuilder();
                        for (NormalFile file : list) {
                            String path = file.getPath();

                            String docfilename = path.substring(path.lastIndexOf("/") + 1);
                            Uri fileUri = Uri.fromFile(new File(path));
                            long fileSizeInKB = file.getSize() / 1024;
                            String filesize = "";
                            if (fileSizeInKB <= 1024) {

                                filesize = fileSizeInKB + " KB";
                            } else {

                                long fileSizeInMB = fileSizeInKB / 1024;

                                filesize = fileSizeInMB + " MB";

                            }
                            String filetype = path.substring(path.lastIndexOf(".") + 1);
                            //  fileName = file.getName();
                            Log.e("imageCaht", "imageCaht" + path);
                            Log.e("docsize", "docsize" + filesize);
                            Log.e("pathuri", "pathuri::" + filetype);
                            Log.e("docfilename", "docfilename::" + docfilename);
                            Log.e("pathuri", "pathuri::" + fileUri);
                            uploadDocData("3", fileUri, filetype, docfilename, filesize);
                        }
                    }

                    break;
            }


        }


    }

    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(mContext, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }


    /*Madhu*/
    private void uploadDocData(final String type, Uri mCurrentPhotoUri, String filetype, final String docfilename, final String docsize) {


        if (mCurrentPhotoUri != null) {
            Animation animation1 = AnimationUtils.loadAnimation(mContext, R.anim.move);
            ivProgress.startAnimation(animation1);
            ivProgress.setVisibility(View.VISIBLE);

            final StorageReference ref = storageReference.child(System.currentTimeMillis() + filetype);

            ref.putFile(mCurrentPhotoUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                    ivProgress.setVisibility(View.GONE);

                    final String imageUri = taskSnapshot.getResult().getUploadSessionUri().toString();

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUrl) {
                            //  Toast.makeText(ChatActivity.this, "Download", Toast.LENGTH_SHORT).show();
                            Log.e("downloadUrl", "downloadUrl:::" + downloadUrl);
                            Log.e("imageUri", "imageUri" + imageUri);
                            Log.e("Ddocfilename", "Ddocfilename" + docfilename);

                            chatMessage = new ChatMessage(
                                    docfilename,
                                    System.currentTimeMillis(),
                                    downloadUrl.toString(),
                                    0,
                                    ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9,
                                    docfilename,
                                    docsize
                            );

                            chatView.addMessage(chatMessage);
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                            callDocFirebase(downloadUrl.toString(), type, docfilename, docsize);
                            chatMsg = String.valueOf(downloadUrl);


                            //do something with downloadurl
                        }
                    });


                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            ivProgress.setVisibility(View.GONE);
                            Toast.makeText(ChatActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            ivProgress.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }
    private void uploadImageData(final String type, Uri mCurrentPhotoUri) {
        if (mCurrentPhotoUri != null) {
            Animation animation1 = AnimationUtils.loadAnimation(mContext, R.anim.move);
            ivProgress.startAnimation(animation1);
            ivProgress.setVisibility(View.VISIBLE);
            globalTimeStamp = System.currentTimeMillis();


            final StorageReference ref = storageReference.child(System.currentTimeMillis() + type);

            // final StorageReference ref = storageReference.child("SatyaImage/"+UUID.randomUUID()+".jpg");
            ref.putFile(mCurrentPhotoUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                    ivProgress.clearAnimation();
                    ivProgress.setVisibility(View.GONE);
                    final String imageUri = taskSnapshot.getResult().getUploadSessionUri().toString();

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUrl) {
                            //   Toast.makeText(ChatActivity.this, "Download", Toast.LENGTH_SHORT).show();
                            Log.e("downloadUrl", "downloadUrl:::" + downloadUrl);
                            Log.e("imageUri", "imageUri" + imageUri);
                           /* chatMessage = new ChatMessage(user_name,
                                    "",
                                    globalTimeStamp,
                                    downloadUrl.toString(),
                                    0,
                                    ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3

                            );*/

                            chatMessage = new ChatMessage(user_name,
                                    "",
                                    globalTimeStamp,
                                    downloadUrl.toString(),
                                    0,
                                    ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3,
                                    "2",
                                    "0",
                                    type,
                                    "0"
                            );
                            //    chatMessage = new ChatMessage( (String) mapMessage.get("sender_name"),"", System.currentTimeMillis(), (String) mapMessage.get("message"), 0, ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3);

                            chatView.addMessage(chatMessage);
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);
                            callImageFirebase(downloadUrl.toString(), type);
                            chatMsg = String.valueOf(downloadUrl);
                            //do something with downloadurl
                        }
                    });

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            CommonUtils.dismissProgress();
                            Toast.makeText(ChatActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            ivProgress.setVisibility(View.VISIBLE);
                            //  CommonUtils.dismissProgress();

                            //  CommonUtils.showProgress(mContext);


                            // progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    private void uploadAudioData(final String type, Uri mCurrentPhotoUri) {
        if (mCurrentPhotoUri != null) {

            Animation animation1 = AnimationUtils.loadAnimation(mContext, R.anim.move);
            ivProgress.startAnimation(animation1);
            ivProgress.setVisibility(View.VISIBLE);


            final StorageReference ref = storageReference.child(System.currentTimeMillis() + type);

            // final StorageReference ref = storageReference.child("SatyaImage/"+UUID.randomUUID()+".jpg");
            ref.putFile(mCurrentPhotoUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> taskSnapshot) {
                    ivProgress.clearAnimation();
                    ivProgress.setVisibility(View.GONE);

                    final String imageUri = taskSnapshot.getResult().getUploadSessionUri().toString();

                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri downloadUrl) {
                            //   Toast.makeText(ChatActivity.this, "Download", Toast.LENGTH_SHORT).show();
                            Log.e("downloadUrl", "downloadUrl:::" + downloadUrl);
                            Log.e("imageUri", "imageUri" + imageUri);


                            chatMessage = new ChatMessage(user_name, downloadUrl.toString(), System.currentTimeMillis(), downloadUrl.toString(), 0, ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7);
                            chatView.addMessage(chatMessage);
                            chatView.getBinding().llSelection.setVisibility(View.VISIBLE);

                            callAudioFirebase(downloadUrl.toString(), type);

                            chatMsg = String.valueOf(downloadUrl);
                            //do something with downloadurl
                        }
                    });

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            CommonUtils.dismissProgress();
                            Toast.makeText(ChatActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            ivProgress.setVisibility(View.VISIBLE);
                            //  CommonUtils.dismissProgress();

                            //  CommonUtils.showProgress(mContext);


                            // progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }


    }

    private void callDocFirebase(String uri, String type, String fileName, String docsize) {

        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
            //  Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();
            {

                Log.e("Ddocfilename", "DdocfilenameMadhu" + fileName);
                Message newMessage = new Message();
                sendermsg = uri;
                newMessage.message = uri;
                newMessage.fileSize = docsize;
                newMessage.filename = fileName;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.chatType = "Group";
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = receiverPic;
                newMessage.sender_name = user_name;
                newMessage.sender_img = sender_img_server;


                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //   Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                    }
                });

                callUpdateMsgListUser(uri, "3");

            }
        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {
            // Toast.makeText(mContext, "from_recent_group", Toast.LENGTH_SHORT).show();
            {
                Message newMessage = new Message();
                sendermsg = uri;
                newMessage.message = uri;
                newMessage.fileSize = docsize;
                newMessage.filename = fileName;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.chatType = "Group";
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = "";
                newMessage.sender_name = user_name;
                newMessage.sender_img = "";
                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        //   Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();
                    }
                });

                callUpdateMsgListUser(uri, "3");

            }
        } else {

            Message newMessage = new Message();
            sendermsg = uri;
            newMessage.message = uri;
            newMessage.fileSize = docsize;
            newMessage.filename = fileName;
            newMessage.type = type;
            newMessage.chatType = "oneToOne";
            newMessage.sender_id = String.valueOf(STPYE);
            newMessage.reciever_id = String.valueOf(RTPYE);
            newMessage.reciever_name = reciver_name;
            newMessage.reciever_img = receiverPic;
            newMessage.sender_name = user_name;
            newMessage.sender_img = sender_img_server;

            newMessage.timestamp = String.valueOf(System.currentTimeMillis());
            newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    //   Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                }
            });
            //TODO Create UpdateList

            Message OpponentMsg = new Message();
            sendermsg = chatMessage.getMessage();
            OpponentMsg.chatType = "oneToOne";
            OpponentMsg.type = "3";
            OpponentMsg.message = chatMessage.getMessage();
            OpponentMsg.oponent_id = String.valueOf(RTPYE);
            OpponentMsg.oponent_name = reciver_name;
            OpponentMsg.oponent_img = receiverPic;

            OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
            OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(STPYE)).child(chatId).child("messages/").setValue(OpponentMsg);

            Message MyMsg = new Message();
            sendermsg = chatMessage.getMessage();
            MyMsg.type = "3";
            MyMsg.message = chatMessage.getMessage();
            MyMsg.oponent_id = String.valueOf(STPYE);
            MyMsg.chatType = "oneToOne";

            if (userScreenStatus.equalsIgnoreCase("1")) {
                MyMsg.msgCount = 0;
                Log.e("singleCountNot", "singleCountNot" + msgLocalCount);
                Log.e("singleCountNot", "singleCountNot:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            } else {
                msgLocalCount = serverCount;
                msgLocalCount++;
                MyMsg.msgCount = msgLocalCount;
                Log.e("singleCount", "singleCount" + msgLocalCount);
                Log.e("singleCount", "singleCount:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            }
            MyMsg.oponent_name = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
            MyMsg.oponent_img = sender_img_server;
            ;
            MyMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            MyMsg.timestamp = String.valueOf(System.currentTimeMillis());
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue(MyMsg);


        }
    }

    private void callImageFirebase(String imageUri, String type) {


        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
            //  Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();
            {
                Message newMessage = new Message();
                sendermsg = imageUri;
                newMessage.message = imageUri;
                // newMessage.filename = filename;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = "";
                newMessage.chatType = "Group";
                newMessage.sender_name = user_name;
                newMessage.sender_img = "";
                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();
                    }
                });
                callUpdateMsgListUser(imageUri, "2");

            }
        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {
            // Toast.makeText(mContext, "from_recent_group", Toast.LENGTH_SHORT).show();
            {
                Message newMessage = new Message();
                sendermsg = imageUri;
                newMessage.message = imageUri;
                // newMessage.filename = filename;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = "";
                newMessage.chatType = "Group";
                newMessage.sender_name = user_name;
                newMessage.sender_img = "";
                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        // Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                    }
                });

                callUpdateMsgListUser(imageUri, "2");

            }
        } else {

            Message newMessage = new Message();
            sendermsg = imageUri;
            newMessage.message = imageUri;
            // newMessage.filename = filename;
            newMessage.type = type;
            newMessage.sender_id = String.valueOf(STPYE);
            newMessage.reciever_id = String.valueOf(RTPYE);
            newMessage.reciever_name = reciver_name;
            newMessage.reciever_img = receiverPic;
            newMessage.chatType = "oneToOne";
            newMessage.sender_name = sender_img_server;
            newMessage.sender_img = "";
            newMessage.timestamp = String.valueOf(System.currentTimeMillis());
            newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();
                }
            });
            Message OpponentMsg = new Message();
            sendermsg = chatMessage.getMessage();
            OpponentMsg.type = "2";
            OpponentMsg.message = chatMessage.getMessage();
            OpponentMsg.oponent_id = String.valueOf(RTPYE);
            OpponentMsg.oponent_name = reciver_name;
            OpponentMsg.oponent_img = receiverPic;
            OpponentMsg.chatType = "oneToOne";

            OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
            OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(STPYE)).child(chatId).child("messages/").setValue(OpponentMsg);

            Message MyMsg = new Message();
            sendermsg = chatMessage.getMessage();
            MyMsg.type = "2";
            MyMsg.message = chatMessage.getMessage();
            MyMsg.oponent_id = String.valueOf(STPYE);
            MyMsg.chatType = "oneToOne";


            if (userScreenStatus.equalsIgnoreCase("1")) {
                MyMsg.msgCount = 0;
                Log.e("singleCountNot", "singleCountNot" + msgLocalCount);
                Log.e("singleCountNot", "singleCountNot:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            } else {
                msgLocalCount = serverCount;
                msgLocalCount++;
                MyMsg.msgCount = msgLocalCount;
                Log.e("singleCount", "singleCount" + msgLocalCount);
                Log.e("singleCount", "singleCount:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            }
            MyMsg.oponent_name = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
            MyMsg.oponent_img = sender_img_server;
            ;
            MyMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            MyMsg.timestamp = String.valueOf(System.currentTimeMillis());
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue(MyMsg);

        }
    }

    private void callAudioFirebase(String imageUri, String type) {


        if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {
            //  Toast.makeText(mContext, "group", Toast.LENGTH_SHORT).show();
            {
                Message newMessage = new Message();
                sendermsg = imageUri;
                newMessage.message = imageUri;
                // newMessage.filename = filename;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = "";
                newMessage.chatType = "Group";
                newMessage.sender_name = user_name;
                newMessage.sender_img = "";
                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                    }
                });

                callUpdateMsgListUser(imageUri, "2");

            }
        } else if (getIntent().getStringExtra(AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {
            // Toast.makeText(mContext, "from_recent_group", Toast.LENGTH_SHORT).show();
            {
                Message newMessage = new Message();
                sendermsg = imageUri;
                newMessage.message = imageUri;
                // newMessage.filename = filename;
                newMessage.type = type;
                newMessage.sender_id = String.valueOf(STPYE);
                newMessage.reciever_id = nodeId;
                newMessage.reciever_name = groupName;
                newMessage.reciever_img = "";
                newMessage.chatType = "Group";
                newMessage.sender_name = user_name;
                newMessage.sender_img = "";
                newMessage.timestamp = String.valueOf(System.currentTimeMillis());
                newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

                FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {


                        // Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                    }
                });

                callUpdateMsgListUser(imageUri, "2");

            }
        } else {

            Message newMessage = new Message();
            sendermsg = imageUri;
            newMessage.message = imageUri;
            newMessage.fileSize = "467";
            newMessage.type = type;
            newMessage.sender_id = String.valueOf(STPYE);
            newMessage.reciever_id = String.valueOf(RTPYE);
            newMessage.reciever_name = reciver_name;
            newMessage.reciever_img = receiverPic;
            newMessage.chatType = "oneToOne";
            newMessage.sender_name = user_name;
            newMessage.sender_img = sender_img_server;
            newMessage.timestamp = String.valueOf(System.currentTimeMillis());
            newMessage.time = CommonUtils.getDate((System.currentTimeMillis()));

            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/").push().setValue(newMessage).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {


                    //Toast.makeText(mContext, "updated", Toast.LENGTH_SHORT).show();

                }
            });

            Message OpponentMsg = new Message();
            sendermsg = chatMessage.getMessage();
            OpponentMsg.type = "4";
            OpponentMsg.message = chatMessage.getMessage();
            OpponentMsg.fileSize = "5";
            OpponentMsg.oponent_id = String.valueOf(RTPYE);
            OpponentMsg.oponent_name = reciver_name;
            OpponentMsg.oponent_img = receiverPic;
            OpponentMsg.chatType = "oneToOne";

            OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
            OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(STPYE)).child(chatId).child("messages/").setValue(OpponentMsg);


            Message MyMsg = new Message();
            sendermsg = chatMessage.getMessage();
            MyMsg.type = "4";
            MyMsg.fileSize = "25";
            MyMsg.message = chatMessage.getMessage();
            MyMsg.oponent_id = String.valueOf(STPYE);
            MyMsg.chatType = "oneToOne";


            if (userScreenStatus.equalsIgnoreCase("1")) {
                MyMsg.msgCount = 0;
                Log.e("singleCountNot", "singleCountNot" + msgLocalCount);
                Log.e("singleCountNot", "singleCountNot:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            } else {
                msgLocalCount = serverCount;
                msgLocalCount++;
                MyMsg.msgCount = msgLocalCount;
                Log.e("singleCount", "singleCount" + msgLocalCount);
                Log.e("singleCount", "singleCount:::" + MyMsg.msgCount);
                Log.e("singleCount", "singleCount:::" + userScreenStatus);

            }
            MyMsg.oponent_name = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
            MyMsg.oponent_img = sender_img_server;
            ;
            MyMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
            MyMsg.timestamp = String.valueOf(System.currentTimeMillis());
            chatMsg = chatMessage.getMessage();
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue(MyMsg);

        }
    }


    private void setCompress(String path, String newPath1) {

        try {
            file = new File(new URI("file://" + path.replace(" ", "%20")));
            long fileSizeInBytes = file.length();

            Toast.makeText(this, "withoutCompress::" + path.length(), Toast.LENGTH_SHORT).show();
            String newPath = "/storage/emulated/0/" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";
            //  String desPath = "StemBuddy" + File.separator + "VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";

            long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            long fileSizeInMB = fileSizeInKB / 1024;
            Bitmap tmb = ThumbnailUtils.createVideoThumbnail(path,
                    MediaStore.Images.Thumbnails.MINI_KIND);
            String image_path = getRealPathFromURI(getImageUri(mContext, tmb));
            thumbImagefile1 = new File(new URI("file://" + image_path.replace(" ", "%20")));

            Toast.makeText(this, "file1Thumbnail::" + thumbImagefile1.length(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    private Locale getLocale() {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

    public Uri getImageContentUri(Context context, String filePath) {
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATA, filePath);
            return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);


        }
    }

    public void deleteReplyDailog(Context context, boolean deleteStatus, String imageStatus, View selectedView) {
        dialog = new Dialog(context);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // Prevent dialog close on back press button
                return keyCode == KeyEvent.KEYCODE_BACK;
            }
        });

        View dialogView = LayoutInflater.from(context).inflate(
                R.layout.copy_reply, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        //  dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(dialogView);
        WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
        layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        //   iv_forword=dialog.findViewById(R.id.iv_forword);
        ImageView iv_back = dialog.findViewById(R.id.iv_back);


        iv_copy = dialog.findViewById(R.id.iv_copy);
        iv_delete = dialog.findViewById(R.id.iv_delete);
        tv_forword = dialog.findViewById(R.id.tv_forword);
        // iv_reply=dialog.findViewById(R.id.iv_reply);
        ConstraintLayout llContainer = dialog.findViewById(R.id.llContainer);

       /* llContainer.setOnClickListener(v->{
            dialog.dismiss();
        });*/


        Log.e("Deletesenderid", "Deletesenderid" + senderid);
        // Log.e("Deletesenderid", "Deletesenderid" + senderid);
        Log.e("DeleteSTPYE", "DeleteSTPYE" + STPYE);

        if (deleteStatus) {
            if (imageStatus.equalsIgnoreCase("2") || imageStatus.equalsIgnoreCase("3")) {
                iv_delete.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.GONE);
                tv_forword.setVisibility(View.VISIBLE);

            } else {
                iv_delete.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.VISIBLE);
                tv_forword.setVisibility(View.VISIBLE);
            }
        } else {

            if (imageStatus.equalsIgnoreCase("2") || imageStatus.equalsIgnoreCase("3")) {
                iv_delete.setVisibility(View.GONE);
                iv_copy.setVisibility(View.GONE);
                tv_forword.setVisibility(View.VISIBLE);

            } else {

                iv_delete.setVisibility(View.GONE);
                tv_forword.setVisibility(View.VISIBLE);
                iv_copy.setVisibility(View.VISIBLE);
            }
        }

        iv_delete.setOnClickListener(v -> {

            dialog.dismiss();
            builder.setMessage("Do you want to Delete Selected message?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            // chatView.removeMessage(deletePosition);
                            callDeletFirebase(deletePosition);
                            //  Toast.makeText(mContext,"you choose yes action for alertbox", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            dialog.cancel();

                        }
                    });
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("Delete Message?");
            alert.show();
        });

        iv_back.setOnClickListener(v -> {
            dialog.dismiss();
            selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        });
        llContainer.setOnClickListener(v -> {
            dialog.dismiss();
            selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        });

        tv_forword.setOnClickListener(view -> {
            dialog.dismiss();
            selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
            String typeStr = firebaseTimestamp.get(deletePosition).type;
            // String type="1";
            Intent intent = new Intent(mContext, MsgForwordActivity.class);
            if (typeStr != null) {
                HashMap<String, String> hashMap = new HashMap<>();
                if (typeStr.equalsIgnoreCase("3")) {
                    hashMap.put("type", typeStr);
                    hashMap.put("message", firebaseTimestamp.get(deletePosition).message);
                    hashMap.put("fileName", firebaseTimestamp.get(deletePosition).filename);
                    hashMap.put("fileSize", firebaseTimestamp.get(deletePosition).fileSize);
                    intent.putExtra("map", hashMap);

                } else {
                    hashMap.put("type", typeStr);
                    hashMap.put("message", firebaseTimestamp.get(deletePosition).message);
                    intent.putExtra("map", hashMap);
                }
            }
            startActivity(intent);

        });

        iv_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                selectedView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                myClip = ClipData.newPlainText("text", firebaseTimestamp.get(deletePosition).message);
                myClipboard.setPrimaryClip(myClip);
                // Toast.makeText(mContext, "Message Copied ", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
    }

    private void callDeletFirebase(int position) {

        Log.e("deleteposion", position + "::");

        String timestamp = firebaseTimestamp.get(position).timestamp;
        Log.e("deleteposion", timestamp + "::");

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatId).child("messages/");
        final Query query = databaseReference.orderByChild("timestamp").equalTo(timestamp);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot2) {

                if (getIntent().getStringExtra(wehyphens.com.satyaconnect.utils.AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(wehyphens.com.satyaconnect.utils.AppConstants.FROM_GROUP).equalsIgnoreCase("from_group")) {

                    for (DataSnapshot childSnapshot : dataSnapshot2.getChildren()) {
                        //String serverTimestamp = childSnapshot.child(timestamp).getKey();
                        String serverKey = childSnapshot.getKey();
                        Message newMessage = new Message();
                        newMessage.type = "0";
                        newMessage.message = "You deleted  this message";
                        // newMessage.sender_id = String.valueOf(STPYE);
                        newMessage.sender_id = firebaseTimestamp.get(position).sender_id;
                        // newMessage.reciever_id = String.valueOf(RTPYE);
                        newMessage.reciever_id = firebaseTimestamp.get(position).reciever_id;
                        newMessage.chatType = "Group";
                        newMessage.reciever_name = firebaseTimestamp.get(position).reciever_name;
                        // newMessage.reciever_name ="";
                        newMessage.reciever_img = "";
                        newMessage.sender_name = firebaseTimestamp.get(position).sender_name;
                        newMessage.sender_img = "";
                        newMessage.timestamp = firebaseTimestamp.get(position).timestamp;
                        newMessage.time = firebaseTimestamp.get(position).time;
                        databaseReference.child(serverKey).setValue(newMessage);

                        Log.e("positionFirebase", position + "");
                        Log.e("firebaseTimestamp", firebaseTimestamp.size() + "");
                        if (position == firebaseTimestamp.size() - 1) {
                            //  updateMsg("Group");
                            callDeleteUpdateMsgListUser("You deleted  this message", "0", position);
                        } else {

                        }
                    }


                } else if (getIntent().getStringExtra(wehyphens.com.satyaconnect.utils.AppConstants.FROM_GROUP) != null && getIntent().getStringExtra(wehyphens.com.satyaconnect.utils.AppConstants.FROM_GROUP).equalsIgnoreCase("from_recent_group")) {

                    for (DataSnapshot childSnapshot : dataSnapshot2.getChildren()) {
                        // String servermessgae = childSnapshot.child("timestamp").getValue(String.class);
                        //String serverTimestamp = childSnapshot.child(timestamp).getKey();
                        String serverKey = childSnapshot.getKey();
                        Message newMessage = new Message();
                        newMessage.type = "0";
                        newMessage.message = "You deleted  this message";
                        // newMessage.sender_id = String.valueOf(STPYE);
                        newMessage.sender_id = firebaseTimestamp.get(position).sender_id;
                        //newMessage.reciever_id = String.valueOf(RTPYE);
                        newMessage.reciever_id = firebaseTimestamp.get(position).reciever_id;
                        newMessage.chatType = "Group";
                        newMessage.reciever_name = firebaseTimestamp.get(position).reciever_name;
                        newMessage.reciever_img = "";
                        newMessage.sender_name = firebaseTimestamp.get(position).sender_name;
                        newMessage.sender_img = "";
                        newMessage.timestamp = firebaseTimestamp.get(position).timestamp;
                        newMessage.time = firebaseTimestamp.get(position).time;
                        databaseReference.child(serverKey).setValue(newMessage);

                        Log.e("positionFirebase", position + "");
                        Log.e("firebaseTimestamp", firebaseTimestamp.size() + "");
                        if (position == firebaseTimestamp.size() - 1) {
                            //  updateMsg("Group");
                            callDeleteUpdateMsgListUser("You deleted  this message", "0", position);
                        } else {

                        }
                    }
                } else {

                    // loop through dataSnapshot
                    for (DataSnapshot childSnapshot : dataSnapshot2.getChildren()) {
                        // String servermessgae = childSnapshot.child("timestamp").getValue(String.class);
                        //String serverTimestamp = childSnapshot.child(timestamp).getKey();
                        String serverKey = childSnapshot.getKey();
                        Message newMessage = new Message();
                        newMessage.type = "0";
                        newMessage.message = "You deleted this message";
                        newMessage.sender_id = String.valueOf(STPYE);
                        // newMessage.reciever_id = String.valueOf(RTPYE);
                        newMessage.reciever_id = firebaseTimestamp.get(position).reciever_id;
                        newMessage.chatType = "oneToOne";
                        newMessage.reciever_name = reciver_name;
                        newMessage.reciever_img = "";
                        newMessage.sender_name = user_name;
                        newMessage.sender_img = "";
                        newMessage.timestamp = firebaseTimestamp.get(position).timestamp;
                        newMessage.time = firebaseTimestamp.get(position).time;
                        databaseReference.child(serverKey).setValue(newMessage);

                        Log.e("positionFirebase", position + "");
                        Log.e("firebaseTimestamp", firebaseTimestamp.size() + "");

                        if (position == firebaseTimestamp.size() - 1) {

                            updateMsg("oneToOne");
                        } else {

                        }

                    }
                }
                //  chatView.setAdapter();

                //  setAllMsgList(dataSnapshot2);

            }

            private void updateMsg(String oneToOne) {
                Message OpponentMsg = new Message();
                OpponentMsg.type = "0";
                OpponentMsg.message = "You deleted  this message";
                OpponentMsg.oponent_id = String.valueOf(RTPYE);
                OpponentMsg.oponent_name = reciver_name;
                OpponentMsg.oponent_img = receiverPic;
                OpponentMsg.chatType = oneToOne;
                OpponentMsg.msgCount = 0;

                OpponentMsg.timestamp = firebaseTimestamp.get(position).timestamp;
                OpponentMsg.time = firebaseTimestamp.get(position).time;
//                chatMsg = chatMessage.getMessage();
                FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(STPYE)).child(chatId).child("messages/").setValue(OpponentMsg);

                Message MyMsg = new Message();
                MyMsg.type = "0";
                MyMsg.message = " This message was deleted";
                MyMsg.oponent_id = String.valueOf(STPYE);
                MyMsg.chatType = oneToOne;
                MyMsg.msgCount = 0;

                MyMsg.oponent_name = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.USER_NAME);
                MyMsg.oponent_img = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
                MyMsg.time = firebaseTimestamp.get(position).time;
                MyMsg.timestamp = firebaseTimestamp.get(position).timestamp;
//                chatMsg = chatMessage.getMessage();
                FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(RTPYE)).child(chatId).child("messages/").setValue(MyMsg);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void callDeleteUpdateMsgListUser(String message, String type, int position) {
        List<String> group_user_list = new ArrayList<>();
        for (int i = 0; i < nodeId.length(); i++) {
            group_user_list = Arrays.asList(nodeId.split("_"));

        }

        //  Toast.makeText(mContext, "group_list"+group_user_list.size(), Toast.LENGTH_SHORT).show();

        //TODO Create UpdateList

        for (int i = 0; i < group_user_list.size(); i++) {
            Message OpponentMsg = new Message();
            String sender_id = group_user_list.get(i).toString();
            OpponentMsg.type = type;
            /* OpponentMsg.message = message;*/
            //  OpponentMsg.oponent_id = chatId;
            OpponentMsg.oponent_id = nodeId;
            OpponentMsg.chatId = chatId;
            OpponentMsg.group_id = notificationReceiverid;
            OpponentMsg.oponent_name = groupName;
            OpponentMsg.chatType = "Group";

            if (user_id.equalsIgnoreCase(sender_id)) {
                OpponentMsg.message = "You deleted this message";
            } else {
                serverCount = group_arrayCountList.get(i);
                serverCount = serverCount + 1;
                OpponentMsg.msgCount = serverCount;
                OpponentMsg.message = "This message was deleted";

                Log.e("OpponentMsg.msgCount", "OpponentMsg.msgCount" + OpponentMsg.msgCount);
            }
            OpponentMsg.oponent_img = receiverPic;
            OpponentMsg.timestamp = firebaseTimestamp.get(position).timestamp;
            OpponentMsg.time = firebaseTimestamp.get(position).time;
            chatMsg = message;


            // FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(sender_id)).child(chatId).child("messages/").updateChildren((Map<String, Object>) OpponentMsg);
            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(sender_id)).child(chatId).child("messages/").setValue(OpponentMsg);

        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        callOnOffStatus("0");
    }

    private void callNotificationApi2(String login_auth, String user_name, String single, String notificationReceiverid, String chatMsgWithUser) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = null;
        try {
            call = service.getNotification(notificationReceiverid, chatMsgWithUser, FirebaseInstanceId.getInstance().getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                Log.e("notificationReceiverid", "notificationReceiverid::" + notificationReceiverid);

                String str = "", msg = "";
                int status = 0;
                try {
                    if (response != null) {

                        if (response.body() != null) {

                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //  Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    //    CommonUtils.inActivieDailog(mContext,jObjError.getString("message"));
                    //   CommonUtils.inActivieDailog(context);

                } catch (Exception e) {
                    // Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }


                try {

                    if (str != null) {
                        JSONObject jsonTop = new JSONObject(str);
                        status = jsonTop.getInt("success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {

                    if (str != null) {
                        JSONObject jsonTop = new JSONObject(str);
                        msg = jsonTop.getString("message");
                        status = jsonTop.getInt("code");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // status = 420 ;
                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                    try {
                        JSONObject jsonTop = new JSONObject(str);
                        msg = jsonTop.getString("message");
                        Log.e("ASD", msg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    CommonUtils.snackBar(msg, leftarrow);

                }







                /*if(status>=1){
                    Log.e("send","send::"+status);
                    }

                    else    if(status==420){

                    Log.e("send","send::"+status);
                    Log.e("send","Please provide your valid device token");


                  }*/

          /*      loginModel =new LoginData();
                Gson gson=new Gson();
                loginModel= gson.fromJson(jsonData.toString(),LoginData.class);*/
                else {
                    Log.e("send", "send::" + status);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
