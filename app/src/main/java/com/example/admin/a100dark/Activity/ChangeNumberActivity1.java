package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.admin.a100dark.R;

public class ChangeNumberActivity1 extends AppCompatActivity {


    private LinearLayout rightTool;
    private ImageView back_number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number1);

        rightTool = findViewById(R.id.rightTool);
        back_number = findViewById(R.id.back_number);

        rightTool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(ChangeNumberActivity1.this, ChangeNumberActivity3.class));
                finish();
            }
        });


        back_number.setOnClickListener(v->{

            finish();
        });

        findViewById(R.id.back_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
