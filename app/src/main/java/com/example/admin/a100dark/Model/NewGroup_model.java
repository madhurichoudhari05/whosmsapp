package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/9/2017.
 */

public class NewGroup_model {

    public String getNewGroup_name() {

        return NewGroup_name;
    }

    public NewGroup_model(String newGroup_name, String newGroup_status, int newGroup_image) {
        NewGroup_name = newGroup_name;
        NewGroup_status = newGroup_status;
        NewGroup_image = newGroup_image;
    }

    public void setNewGroup_name(String newGroup_name) {
        NewGroup_name = newGroup_name;
    }

    public String getNewGroup_status() {
        return NewGroup_status;
    }

    public void setNewGroup_status(String newGroup_status) {
        NewGroup_status = newGroup_status;
    }

    public int getNewGroup_image() {
        return NewGroup_image;
    }

    public void setNewGroup_image(int newGroup_image) {
        NewGroup_image = newGroup_image;
    }

    private String NewGroup_name;
    private String NewGroup_status;
    private int NewGroup_image;



}
