package com.example.admin.a100dark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 11/1/2017.
 */

public class AllUserListModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("allUser")
    @Expose
    private List<AllUserListDetailsModel> allUser = null;
    @SerializedName("currentUser")
    @Expose
    private CurrentUserDetails currentUser;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AllUserListDetailsModel> getAllUser() {
        return allUser;
    }

    public void setAllUser(List<AllUserListDetailsModel> allUser) {
        this.allUser = allUser;
    }

    public CurrentUserDetails getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(CurrentUserDetails currentUser) {
        this.currentUser = currentUser;
    }



}
