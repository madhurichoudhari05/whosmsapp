package com.example.admin.a100dark.firebase;

import android.util.Log;

import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;



/**
 * Created by mamta on 11-04-2017.
 */

public class MyfirebaseInstanceidService extends FirebaseInstanceIdService {

    private  static  final  String  TOKEN="gettoken";
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token= FirebaseInstanceId.getInstance().getToken();
        Log.e(TOKEN,"token:::::::"+token);
        CommonUtils.savePreferencesString(MyfirebaseInstanceidService.this, AppConstants.FIREBASE_KEY,token);
    }
}
/*Legacy key*/
/*AIzaSyC_QW0rp1O2aWp32ee4g-dLECPyiJwMAbk*/