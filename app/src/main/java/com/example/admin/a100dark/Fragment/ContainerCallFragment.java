package com.example.admin.a100dark.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.a100dark.Adapter.CallAdapter;
import com.example.admin.a100dark.Model.CallModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import java.util.ArrayList;

/**
 * Created by Admin on 11/1/2017.
 */

public class ContainerCallFragment extends Fragment {


   private Context mcontext;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.container_call_screen, null);
        mcontext=getActivity();


        if(!CommonUtils.getPreferencesBoolean(mcontext, AppConstants.CURRENT_LONGI)){



            FragmentManager firstCallFragment=getChildFragmentManager();
             FragmentTransaction fragmentTransaction= firstCallFragment.beginTransaction();
            FirstCallFragment firstCallFragment1=new FirstCallFragment();
            firstCallFragment1.call();
             fragmentTransaction.replace(R.id.flcallContainer,firstCallFragment1);
             fragmentTransaction.commit();
        }
        else {

        }

        return vv;
    }

}
