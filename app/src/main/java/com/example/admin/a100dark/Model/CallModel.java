package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/1/2017.
 */

public class CallModel {
    private String userName;
    private String timing;
    private int profile_pic;

    public CallModel(String userName, String timing, int profile_pic) {
        this.userName = userName;
        this.timing = timing;
        this.profile_pic = profile_pic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public int getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(int profile_pic) {
        this.profile_pic = profile_pic;
    }
}
