package com.example.admin.a100dark.chat.app.ui;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;


import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.retofit.ApiInterface;
import com.example.admin.a100dark.chat.app.retofit.FcmApi;
import com.example.admin.a100dark.chat.dependency.handler.ChstRetrofitHandler;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.chat.dependency.handler.FCMRetrofitHandler;
import com.example.admin.a100dark.chat.interfaces.IConstants;

import java.util.concurrent.ExecutorService;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abul on 29/11/17.
 */

public abstract class SuperAndroidViewModel extends AndroidViewModel implements IConstants {

    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;
    protected FcmApi mApi;
    protected ApiInterface mApiInter;

    protected MutableLiveData<Integer> status;
    protected MutableLiveData<Throwable> error;
    protected MutableLiveData<String> msgSnak;
    protected MutableLiveData<String> msgToast;

    protected Observable<?> call;


    public SuperAndroidViewModel(@NonNull Application application) {
        super(application);

        mAppDb= MyApplication.getDb();
        mApi= FCMRetrofitHandler.getInstance().getApi();
        mApiInter= ChstRetrofitHandler.getInstance().getApi();
        mExecutor= ExecutorHandler.getInstance().getExecutor();

        status = new MutableLiveData<>();
        error = new MutableLiveData<>();
        msgSnak = new MutableLiveData<>();
        msgToast = new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getStatus() {
        return status;
    }

    public MutableLiveData<Throwable> getError() {
        return error;
    }

    public MutableLiveData<String> getMsgSnak() {
        return msgSnak;
    }

    public MutableLiveData<String> getMsgToast() {
        return msgToast;
    }

    protected Observable<?> getCall() {

        if (!isNetworkAvailable()) {
            msgSnak.setValue("Network not available");
            call = Observable.create(e -> {
                try {
                    e.onComplete();
                } catch (Exception ex) {
                    e.onError(ex);
                }

            });
        }
        status.setValue(INetwork.RET_START);
        return call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally(() -> status.setValue(INetwork.RET_COMPLETE));
    }

    protected <T> Observable<T> getCall(Class<T> clazz) {

        if (!isNetworkAvailable()) {
            msgSnak.setValue("Network not available");
            call = Observable.create(e -> {
                try {
                    e.onComplete();
                } catch (Exception ex) {
                    e.onError(ex);
                }

            });
        }
        status.setValue(INetwork.RET_START);
        return (Observable<T>)call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally(() -> status.setValue(INetwork.RET_COMPLETE));
    }

    /***************
     Network check
     *************/
    protected boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
