package com.example.admin.a100dark.chat.app.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.PlaybackParams;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Activity.MainActivity;
import com.example.admin.a100dark.Activity.ProfileInfo;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ActivityAudioRecording extends AppCompatActivity {

    LinearLayout ll_start_recording,ll_stop_reco,ll_play_pouse;
    TextView btn_send;
    public  boolean play=false;
    public    boolean start=true;
    TextView tv_play_pouse,tv_start_stop;
    ImageView img_mic,img_play;
    boolean status=false;
    boolean play_pause_status=false;

    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder ;
    Random random ;
    String RandomAudioFileName = "ABCDEFGHIJKLMNOP";
    public static final int RequestPermissionCode = 1;
    MediaPlayer mediaPlayer ;
    private Context context;
    private ImageView leftarrow;
     private static final String TAG = "ActivityAudioRecording";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_recording);

        context=ActivityAudioRecording.this;
        ll_play_pouse=findViewById(R.id.ll_play_pouse);
       // ll_stop_reco=findViewById(R.id.ll_stop);
        ll_start_recording=findViewById(R.id.ll_start_recording);
        leftarrow=findViewById(R.id.leftarrow);
        btn_send=findViewById(R.id.btn_send);
        tv_play_pouse=findViewById(R.id.tv_play_pouse);
        tv_start_stop=findViewById(R.id.tv_start_stop);
        img_mic=findViewById(R.id.img_mic);
        img_play=findViewById(R.id.img_play);

        random = new Random();

        leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ll_start_recording.setOnClickListener(new View.OnClickListener() {
       @Override
        public void onClick(View view)throws IllegalArgumentException,
            SecurityException, IllegalStateException {
              if(checkPermission()) {
                callRecordDialog();
                } else {
            requestPermission();

        }

    }
});



/*
ll_stop_reco.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        mediaRecorder.stop();
       */
/* buttonStop.setEnabled(false);
        buttonPlayLastRecordAudio.setEnabled(true);
        buttonStart.setEnabled(true);
        buttonStopPlayingRecording.setEnabled(false);*//*


        Toast.makeText(ActivityAudioRecording.this, "Recording Completed",
                Toast.LENGTH_LONG).show();
    }
});
*/

        ll_play_pouse.setOnClickListener(new View.OnClickListener() {

        public void onClick(View view)throws IllegalArgumentException,
            SecurityException, IllegalStateException {

            // Toast.makeText(context, "play_pause", Toast.LENGTH_SHORT).show();

            if (AudioSavePathInDevice != null) {

                if (!play_pause_status) {


                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(AudioSavePathInDevice);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    PlaybackParams params = new PlaybackParams();

                    params.setPitch(0.75f);
                    mediaPlayer.setPlaybackParams(params);

                    mediaPlayer.start();

                    img_play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                    Toast.makeText(ActivityAudioRecording.this, "Recording Playing",
                            Toast.LENGTH_LONG).show();

                    tv_play_pouse.setText("Pause");
                    play_pause_status = true;
                } else {

                    if (mediaPlayer != null) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        MediaRecorderReady();
                    }
                    img_play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
                    Toast.makeText(ActivityAudioRecording.this, "Recording Pause",
                            Toast.LENGTH_LONG).show();
                    play_pause_status = false;
                    tv_play_pouse.setText("play");
                }
            }

            else {
                Toast.makeText(context, "No any Audio Recorded", Toast.LENGTH_SHORT).show();
            }
        }});


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            System.out.print(AudioSavePathInDevice);


                Log.e(TAG, "onClick:AudioPath::"+AudioSavePathInDevice);
                Intent intent=new Intent();
                intent.putExtra("AUDIO_PATH",AudioSavePathInDevice);
                setResult(RESULT_OK,intent);
                finish();



             //   Toast.makeText(ActivityAudioRecording.this, "value of file"+AudioSavePathInDevice, Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    public String CreateRandomAudioFileName(int string){
        StringBuilder stringBuilder = new StringBuilder( string );
        int i = 0 ;
        while(i < string ) {
            stringBuilder.append(RandomAudioFileName.charAt(random.nextInt(RandomAudioFileName.length())));
            i++ ;
        }
        return stringBuilder.toString();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(ActivityAudioRecording.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length> 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(ActivityAudioRecording.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ActivityAudioRecording.this,"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;


    }
    private void callRecordDialog() {

        TextView very_high,high,medium,low,tv_start_stop;

        ImageView iv_high,iv_low,iv_very_high,iv_medium,tvRecord,iv_play_pouse,tvCancel;

        Dialog dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popup_type_record);
        // dialog.getWindow().getAttributes().windowAnimations = animationdialog;
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        very_high=dialog.findViewById(R.id.very_high);
        tvRecord=dialog.findViewById(R.id.tvRecord);
        iv_high=dialog.findViewById(R.id.iv_high);
        iv_low=dialog.findViewById(R.id.iv_low);
        iv_very_high=dialog.findViewById(R.id.iv_very_high);
        iv_medium=dialog.findViewById(R.id.iv_medium);
        high=dialog.findViewById(R.id.high);
        medium=dialog.findViewById(R.id.medium);
        tvCancel=dialog.findViewById(R.id.tvCancel);
        low=dialog.findViewById(R.id.low);
        iv_play_pouse=dialog.findViewById(R.id.iv_play_pouse);
        tv_start_stop=dialog.findViewById(R.id.tv_start_stop);


        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }

             /*   playAudio();
                dialog.dismiss();
                prviewAudio(2.0f);*/
            }


        });

        low.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view)throws IllegalArgumentException,
                    SecurityException, IllegalStateException {
                iv_low.setVisibility(View.VISIBLE);
                iv_high.setVisibility(View.GONE);
                iv_medium.setVisibility(View.GONE);
                iv_very_high.setVisibility(View.GONE);
               // playAudio();
                CommonUtils.savePreferencesString(context, AppConstants.PITCH,"0.5f");
                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }
                prviewAudio();


             /*   playAudio();
                dialog.dismiss();
                prviewAudio(2.0f);*/
            }


        });
        medium.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
               // playAudio();

                iv_low.setVisibility(View.GONE);
                iv_high.setVisibility(View.GONE);
                iv_medium.setVisibility(View.VISIBLE);
                iv_very_high.setVisibility(View.GONE);
                CommonUtils.savePreferencesString(context, AppConstants.PITCH,"1.0f");
                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }
                prviewAudio();

              //  dialog.dismiss();


              //  prviewAudio(1.5f);
            }
        });

     high.setOnClickListener(new View.OnClickListener() {
   // @RequiresApi(api = Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {


        iv_low.setVisibility(View.GONE);
        iv_high.setVisibility(View.VISIBLE);
        iv_medium.setVisibility(View.GONE);
        iv_very_high.setVisibility(View.GONE);
       // playAudio();

        CommonUtils.savePreferencesString(context, AppConstants.PITCH,"1.5f");
        if(mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
            MediaRecorderReady();
        }
        prviewAudio();
     }
       });
       very_high.setOnClickListener(new View.OnClickListener() {
          @RequiresApi(api = Build.VERSION_CODES.M)
           @Override
           public void onClick(View view) {

              iv_low.setVisibility(View.GONE);
              iv_high.setVisibility(View.GONE);
              iv_medium.setVisibility(View.GONE);
              iv_very_high.setVisibility(View.VISIBLE);
              CommonUtils.savePreferencesString(context, AppConstants.PITCH,"2.0f");
              if(mediaPlayer != null){
                  mediaPlayer.stop();
                  mediaPlayer.release();
                  MediaRecorderReady();
              }
              prviewAudio();
           }

       });

        iv_play_pouse.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {


                prviewAudio();
            }
        });

        tvRecord.setOnClickListener(new View.OnClickListener() {
             @RequiresApi(api = Build.VERSION_CODES.M)

            @Override
            public void onClick(View view)throws IllegalArgumentException,
                     SecurityException, IllegalStateException  {

                 if(checkPermission()) {
                     if(status==false){


                     AudioSavePathInDevice =
                             Environment.getExternalStorageDirectory().getAbsolutePath() + "/" +
                                     CreateRandomAudioFileName(5) + "AudioRecording.3gp";

                     MediaRecorderReady();


                     try {
                         mediaRecorder.prepare();
                         mediaRecorder.start();
                         status=true;
                         play=true;
                     } catch (IllegalStateException e) {
                         // TODO Auto-generated catch block
                         e.printStackTrace();
                     } catch (IOException e) {
                         // TODO Auto-generated catch block
                         e.printStackTrace();
                     }
                         tvRecord.setImageResource(R.drawable.ic_mic_off_black_24dp);
                         tv_start_stop.setText("Stop");
                     Toast.makeText(getApplicationContext(), "Recording Started", Toast.LENGTH_LONG).show();
                 }else {
                         tv_start_stop.setText("Start");
                         tvRecord.setImageResource(R.drawable.ic_mic_black_24dp);
                         mediaRecorder.stop();
                         status=false;
                         Toast.makeText(getApplicationContext(), "Recording Completed", Toast.LENGTH_LONG).show();
                     }
                 }else {
                     requestPermission();
                 }

            }

        });
        dialog.show();
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void prviewAudio() {

        Float f= Float.valueOf(CommonUtils.getPreferencesString(context, AppConstants.PITCH));
        if(f==null)
        {
            f=0.5f;
        }

        if(play==true){
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(AudioSavePathInDevice);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PlaybackParams params = new PlaybackParams();

        params.setPitch(f);
        mediaPlayer.setPlaybackParams(params);

        mediaPlayer.start();


        img_play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
        Toast.makeText(ActivityAudioRecording.this, "Recording Playing",
                Toast.LENGTH_LONG).show();

    }
    }
    public void playAudio() {

        status=true;
        if(start==true) {
            tv_start_stop.setText("Stop");
          //  img_mic.setImageResource(R.drawable.ic_mic_off_black_24dp);

            AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + CreateRandomAudioFileName(5) + "AudioRecording.3gp";
            MediaRecorderReady();

            try {
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            // ll_start_recording.setEnabled(false);
            ll_play_pouse.setEnabled(true);

           // Toast.makeText(ActivityAudioRecording.this, "Recording started", Toast.LENGTH_LONG).show();
            // play=true;

            start=false;
            ll_play_pouse.setEnabled(true);
        }
        else {

            tv_start_stop.setText("Start");
            img_mic.setImageResource(R.drawable.ic_mic_black_24dp);
            mediaRecorder.stop();
            Toast.makeText(ActivityAudioRecording.this, "Recording stoped", Toast.LENGTH_LONG).show();
            start=true;

        }


    }

}





