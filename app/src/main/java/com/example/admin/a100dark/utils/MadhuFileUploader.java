package com.example.admin.a100dark.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.webkit.MimeTypeMap;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by abul on 16/11/17.
 */

public class MadhuFileUploader {

    private Context mContext;
    private Map<String, RequestBody> partMap;
    private MultipartBody.Part partBody;
    private MultipartBody.Part[] partBodyArr;


    public Map<String, RequestBody> getPartMap() {
        return partMap;
    }

    public void setPartMap(Map<String, RequestBody> partMap) {
        this.partMap = partMap;
    }

    public MultipartBody.Part getPartBody() {
        return partBody;
    }

    public void setPartBody(MultipartBody.Part partBody) {
        this.partBody = partBody;
    }

    public MultipartBody.Part[] getPartBodyArr() {
        return partBodyArr;
    }

    public void setPartBodyArr(MultipartBody.Part[] partBodyArr) {
        this.partBodyArr = partBodyArr;
    }



    public void uploadFileEntry(Map<String, String> data, ArrayList<ImageEntry> uris, String imageName, Context context) {
        mContext = context;
        partMap=prepareData(data);
        partBodyArr=prepareFilePartArray(imageName,uris);

    }

    public void uploadFileUri(Map<String, String> data, ArrayList<Uri> uris, String imageName, Context context) {
        mContext = context;
        partMap=prepareData(data);
        partBodyArr=prepareFilePartUri(imageName,uris);

    }

    public void uploadFile(Map<String, String> data, ArrayList<String> paths, String imageName, Context context) {
        mContext = context;
        partMap=prepareData(data);
        partBodyArr=prepareFilePart(imageName,paths);

    }

    public void uploadFile(Map<String, String> data, Uri uri, String imageName, Context context) {
        mContext = context;
        partMap=prepareData(data);
        partBody=prepareFilePart(imageName,uri);
    }

    public void uploadFile(Map<String, String> data, String path, String imageName, Context context) {
        mContext = context;
        partMap=prepareData(data);
        partBody=prepareFilePart(imageName,imageName);

    }






















    private Map<String, RequestBody> prepareData(Map<String, String> data) {
        Map<String, RequestBody> result = new HashMap<>();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), entry.getValue());
            result.put(entry.getKey(),body);
        }
        return result;
    }


    /*part creation*/
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(getImagePath(mContext, fileUri));
        //        File file = new File(fileUri);


        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(mContext.getContentResolver().getType(fileUri)),
                        file

                        //madhuri
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    private MultipartBody.Part[] prepareFilePartUri(String partName, ArrayList<Uri> fileUri) {
        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for(int i=0;i<fileUri.size();i++){

            File file = new File(getImagePath(mContext, fileUri.get(i)));
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(mContext.getContentResolver().getType(fileUri.get(i))),
                            file
                    );
            arrayImage[i] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }

        return arrayImage;
    }

    @NonNull
    private MultipartBody.Part[] prepareFilePartArray(String partName, ArrayList<ImageEntry> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            String image = fileUri.get(index).path;
            File file = new File(String.valueOf(image));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(image);
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to */*
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        }
        // MultipartBody.Part is used to send also the actual file name
        return arrayImage;
    }

    public String getImagePath(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String fileUri) {
        File file = new File(fileUri);
        String type = null;
        final String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        if (type == null) {
            type = "image/*"; // fallback type. You might set it to */*
        }
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(type),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @NonNull
    private MultipartBody.Part[] prepareFilePart(String partName, ArrayList<String> fileUri) {

        MultipartBody.Part[] arrayImage = new MultipartBody.Part[fileUri.size()];

        for (int index = 0; index < fileUri.size(); index++) {
            File file = new File(fileUri.get(index));
            String type = null;
            final String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri.get(index));
            if (extension != null) {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
            }
            if (type == null) {
                type = "image/*"; // fallback type. You might set it to */*
            }
            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(type),
                            file
                    );
            arrayImage[index] = MultipartBody.Part.createFormData(partName, file.getName(), requestFile);

        }

        return arrayImage;
    }

}
