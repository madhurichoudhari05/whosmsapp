package com.example.admin.a100dark.chat.app.db.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.admin.a100dark.chat.app.db.dao.ChatDao;
import com.example.admin.a100dark.chat.app.db.dao.UserDao;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;

import static com.example.admin.a100dark.chat.interfaces.IConstants.IDb.USER_DB_VERSION;


/**
 * Created by abul on 23/11/17.
 */

@Database(entities = {
        ChatMessageDto.class,
        UserStatusDto.class},
        version = USER_DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ChatDao chatDao();

    public abstract UserDao userDao();
}
