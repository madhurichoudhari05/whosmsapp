package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/9/2017.
 */

public class Counterycode_Model {

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getC_country_name() {
        return c_country_name;
    }

    public void setC_country_name(String c_country_name) {
        this.c_country_name = c_country_name;
    }

    public String getC_country_code() {
        return c_country_code;
    }

    public void setC_country_code(String c_country_code) {
        this.c_country_code = c_country_code;
    }

    private  String c_id;
    private  String c_country_name;
    private  String c_country_code;

}
