package com.example.admin.a100dark.chat.app.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.example.admin.a100dark.chat.interfaces.IConstants;

import co.intentservice.chatui.models.ChatMessage;


/**
 * Created by abul on 23/11/17.
 */

@Entity(tableName = "chat_table")
public class ChatMessageDto implements Cloneable {

    @PrimaryKey
    @NonNull
    private String dateTime;
    private String myId;
    private String myName;
    private String myPic;
    private String toId;
    private String toName;
    private String fcmToken;
    private boolean isRead;
    private boolean isSend;
    private boolean isSender;
    private int msgType;
    private String msg;
    private String filePath;

    public ChatMessageDto() {
    }

    @Ignore
    public ChatMessageDto(@NonNull String dateTime, String myId, String myName, String myPic, String toId, String toName, String fcmToken, boolean isRead, boolean isSend, boolean isSender, int msgType, String msg, String filePath) {
        this.dateTime = dateTime;
        this.myId = myId;
        this.myName = myName;
        this.myPic = myPic;
        this.toId = toId;
        this.toName = toName;
        this.fcmToken = fcmToken;
        this.isRead = isRead;
        this.isSend = isSend;
        this.isSender = isSender;
        this.msgType = msgType;
        this.msg = msg;
        this.filePath = filePath;
    }

    @NonNull
    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(@NonNull String dateTime) {
        this.dateTime = dateTime;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyPic() {
        return myPic;
    }

    public void setMyPic(String myPic) {
        this.myPic = myPic;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public boolean isSender() {
        return isSender;
    }

    public void setSender(boolean sender) {
        isSender = sender;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public void switchToSendType(ChatMessageDto dto){
        myId=dto.getToId();
        myName=dto.getToName();
        toId=dto.getMyId();
        toName=dto.getMyName();
        isSender=false;
        isSend=true;

        switch (dto.getMsgType()){
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1:
                msgType=ChatMessage.Type.TYPE_VIEW_REC_MSG_TEXT_0;
                break;
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_IMG_3:
                msgType=ChatMessage.Type.TYPE_VIEW_REC_MSG_IMG_2;
                break;
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_AUDIO_7:
                msgType=ChatMessage.Type.TYPE_VIEW_REC_MSG_AUDIO_6;
                break;
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_VIDEO_5:
                msgType=ChatMessage.Type.TYPE_VIEW_REC_MSG_VIDEO_4;
                break;
            case ChatMessage.Type.TYPE_VIEW_SEN_MSG_DOC_9:
                msgType=ChatMessage.Type.TYPE_VIEW_REC_MSG_DOC_8;
                break;



        }

    }

    public ChatMessageDto clone() {
        try {
            return (ChatMessageDto)super.clone();
        }
        catch (CloneNotSupportedException e) {
            // This should never happen
        }
        return null;
    }
}