package com.example.admin.a100dark.Activity;

import android.app.Dialog;
import android.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.Adapter.ViewPagerAdapter;
import com.example.admin.a100dark.Fragment.CallFragment;
import com.example.admin.a100dark.Fragment.ChatFragment;
import com.example.admin.a100dark.Fragment.ContainerChatFragment;
import com.example.admin.a100dark.Fragment.CurrentChatFragment;
import com.example.admin.a100dark.Fragment.NewUserFragment;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

public class MainActivity extends AppCompatActivity {


    ViewPagerAdapter viewPagerAdapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    Toolbar toolBar;
    TextView setting, newgroup, newbroadcast, whatsappweb, starredmessage;
    ImageView search;
    ImageView more;
    FloatingActionButton fab, fabedit;
    private Context context;

    Dialog mDialog;

    private int[] tabIcons = {
            R.drawable.camera,

    };


    public static Intent getPendingIntent(Context activity, int type) {
        Intent i = new Intent(activity, MainActivity.class);
        i.putExtra(IConstants.IApp.PARAM_1, type);
        return i;
    }

    int noOfReq = 0, noOfChat = 0, total = 0;
    private FcmMessengerViewModel messenger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = MainActivity.this;
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewpager1);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        search = (ImageView) findViewById(R.id.search);
        more = (ImageView) findViewById(R.id.more);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabedit = (FloatingActionButton) findViewById(R.id.fabedit);


        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Setting.class));
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeachhActivity.class));

            }
        });
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), context);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(1);
        viewPager.getCurrentItem();
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main2, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onRestart() {
        super.onRestart();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (CommonUtils.getPreferencesBoolean(context, AppConstants.FIRST_TIME_LOGIN)) {
            Toast.makeText(context, "MainActivitytrue", Toast.LENGTH_SHORT).show();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            CurrentChatFragment frag = new CurrentChatFragment();
            transaction.replace(R.id.viewpager1, frag);
            transaction.commit();


        } else {
            Toast.makeText(context, "false", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.Fragment f = fm.findFragmentById(R.id.activity_login);
        Toast.makeText(context, "onBackPressed", Toast.LENGTH_SHORT).show();


        if (fm.getBackStackEntryCount() == 0) {
            Toast.makeText(context, "true00", Toast.LENGTH_SHORT).show();


            fm.popBackStack();
        }

        if (fm.getBackStackEntryCount() == 1) {
            Toast.makeText(context, "true111", Toast.LENGTH_SHORT).show();


            fm.popBackStack();
        }
        if (fm.getBackStackEntryCount() == 2) {
            Toast.makeText(context, "true 22", Toast.LENGTH_SHORT).show();


            fm.popBackStack();
        } else {
            finish();
        }
    }
}




