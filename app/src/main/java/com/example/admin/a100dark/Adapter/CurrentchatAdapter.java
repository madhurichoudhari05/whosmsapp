package com.example.admin.a100dark.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Activity.HomeActivity;
import com.example.admin.a100dark.Model.LatestMessage;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.ExecutorService;

import de.hdodenhof.circleimageview.CircleImageView;


public class CurrentchatAdapter extends RecyclerView.Adapter<CurrentchatAdapter.ViewHolder> {

    private com.example.admin.a100dark.Model.LatestMessage LatestMessage;
    ExecutorService mExecutor;
    List<LatestMessage> arrayList;
      Context context;
      private AppDatabase mAppDb;
    private String user_id="";
    public static int userCounter=0;



    public CurrentchatAdapter(List<LatestMessage> arrayList, Context context){

    this.arrayList=arrayList;
    this.context=context;
    mAppDb = MyApplication.getDb();
    mExecutor = ExecutorHandler.getInstance().getExecutor();

    user_id=CommonUtils.getPreferencesString(context, wehyphens.com.satyaconnect.utils.AppConstants.USER_ID);



}

    @Override
    public CurrentchatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.current_chat_row_item, parent, false);

        return new ViewHolder(view);


    }


    @Override
    public void onBindViewHolder(CurrentchatAdapter.ViewHolder holder, int position) {
        LatestMessage =arrayList.get(position);
        if((!TextUtils.isEmpty(arrayList.get(position).getOponent_img()))&&arrayList.get(position).getOponent_img()!=null) {
            Picasso.with(context).load(arrayList.get(position).getOponent_img()).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(holder.user_pic);
        }

        if(!TextUtils.isEmpty(arrayList.get(position).getOponent_name())) {
        //    holder.userName.setText(CommonUtils.NameCaps(arrayList.get(position).getOponent_name()));
            holder.userName.setText(CommonUtils.NameCaps(arrayList.get(position).getOponent_name()));
        }

        if(arrayList.get(position).getMsgCount()!=0){
            holder.tvMsgCount.setVisibility(View.VISIBLE);
            holder.tvMsgCount.setText(arrayList.get(position).getMsgCount()+"");
        }
        else {
            holder.tvMsgCount.setVisibility(View.GONE);
            HomeActivity.tvMsgCount.setVisibility(View.GONE);
        }

        if(holder.tvMsgCount.getVisibility()==View.VISIBLE){
            userCounter++;
            HomeActivity.tvMsgCount.setVisibility(View.VISIBLE);
            HomeActivity.tvMsgCount.setText(userCounter+"");

        }
        else {

            }
        if (arrayList.get(position).getType().equalsIgnoreCase("0")) {

            if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                holder.ivImage.setVisibility(View.VISIBLE);
                holder.tvMsgCount.setVisibility(View.GONE);
                holder.ivImage.setImageResource(R.drawable.unavailable);
                holder.chatTitle.setText(arrayList.get(position).getMessage());
                // holder.chatTitle.setText("This message was deleted");
            }

        } else if (arrayList.get(position).getType().equalsIgnoreCase("1")) {

            if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                holder.ivImage.setVisibility(View.GONE);
                holder.chatTitle.setText(arrayList.get(position).getMessage() + "");
            }

        } else if (arrayList.get(position).getType().equalsIgnoreCase("2")) {

            holder.chatTitle.setText("Photos");
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.ivImage.setImageResource(R.drawable.ic_camera_alt_black_24dp);

            if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                   /* holder.chatTitle.setText("Photos");
                    holder.ivImage.setVisibility(View.VISIBLE);
                    holder.ivImage.setImageResource(R.drawable.ic_camera_alt_black_24dp);*/
            }

        } else if (arrayList.get(position).getType().equalsIgnoreCase("3")) {

            holder.chatTitle.setText("File");
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.ivImage.setImageResource(R.drawable.file);

              /*  if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.chatTitle.setText("File");
                    holder.ivImage.setVisibility(View.VISIBLE);
                    holder.ivImage.setImageResource(R.drawable.file);
                }*/

        }else if(!TextUtils.isEmpty(arrayList.get(position).getType())) {
            if (arrayList.get(position).getType().equalsIgnoreCase("1")) {

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {

                    holder.chatTitle.setText(arrayList.get(position).getMessage() + "");
                }

            }

            else if (arrayList.get(position).getType().equalsIgnoreCase("3")) {

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.chatTitle.setText("File");
                    holder.ivImage.setVisibility(View.VISIBLE);

                    holder.ivImage.setImageResource(R.drawable.file);


                }

            }


            else if (arrayList.get(position).getType().equalsIgnoreCase("2")) {

                if (!TextUtils.isEmpty(arrayList.get(position).getMessage())) {
                    holder.chatTitle.setText("Photos");
                    holder.ivImage.setVisibility(View.VISIBLE);

                    holder.ivImage.setImageResource(R.drawable.ic_camera_alt_black_24dp);
                }

            }
            
        }

        if(arrayList.get(position).getTime()!=null&&!TextUtils.isEmpty(arrayList.get(position).getTime())){
          String date[]=arrayList.get(position).getTime().split("-");
          Log.e("date",date[0]);
            holder.lastSeen.setText(date[1]);

        }



        holder.llUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               userCounter=0;
                CommonUtils.savePreferencesString(context,AppConstants.RECEIVER_ID, arrayList.get(position).getOponent_id());
                CommonUtils.saveStringPreferences(context,AppConstants.RECEIVER__PIC,arrayList.get(position).getOponent_img());
                Log.e("receiver_id","Current_receiver_id"+arrayList.get(position).getOponent_id());
                    Intent intent = new Intent(new Intent(context, ChatActivity.class));
                    intent.putExtra(AppConstants.RECEIVER__PIC, arrayList.get(position).getOponent_img());
                    intent.putExtra(wehyphens.com.satyaconnect.utils.AppConstants.RECEIVER__USER_NAME2, arrayList.get(position).getOponent_name());
                    intent.putExtra(wehyphens.com.satyaconnect.utils.AppConstants.RECEIVER_ID, arrayList.get(position).getOponent_id());
                    context.startActivity(intent);

            }


        });


        if(position % 2==1){
            // holder.vView.setVisibility(View.GONE);
            // holder.ll_row_container.setBackgroundColor(Color.parseColor("#f1ecec"));
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.grayline));


        }
        else {
            //  holder.vView.setVisibility(View.VISIBLE);
            holder.llUser.setBackgroundColor(context.getResources().getColor(R.color.white));

        }
    }

    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
           ImageView ivImage;
        TextView userName,chatTitle,lastSeen,tvMsgCount;
        RelativeLayout llUser;
        View vView;


        public ViewHolder(View itemView) {
            super(itemView);
            user_pic=itemView.findViewById(R.id.civ_user_pic);
            userName=itemView.findViewById(R.id.tv_userName);
            chatTitle=itemView.findViewById(R.id.tv_chat_title);
            lastSeen=itemView.findViewById(R.id.tv_last_seen);
            llUser=itemView.findViewById(R.id.llUser);
            ivImage=itemView.findViewById(R.id.ivImage);
            tvMsgCount=itemView.findViewById(R.id.tvMsgCount);

        }
    }

    public void filterList(List<LatestMessage> filterdNames) {
        arrayList = filterdNames;
        notifyDataSetChanged();
    }

    }
