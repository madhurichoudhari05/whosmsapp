package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.R;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class TimerActivity extends AppCompatActivity {

    TextView Counter1,Counter2,Counter3,Counter4,textView6,tv_start_stop;

    int OneSecond=8000;
    int count1=0;
    int count2=0;
    int count3=0;
    int count4=0;
    String correctStatus="0";
    long millisInFuture = 25000; //25 seconds
    //The interval along the way to receive onTick(long) callbacks
    long countDownInterval = 200;
    private int init=0;
    private Handler handler= new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

         Counter1=findViewById(R.id.textView1);
         Counter2=findViewById(R.id.textView2);
         Counter3=findViewById(R.id.textView3);
         Counter4=findViewById(R.id.textView4);
         textView6=findViewById(R.id.textView6);
        tv_start_stop=findViewById(R.id.tv_start_stop);
        int rando=0;
        final Runnable updater = new Runnable() {
            @Override
            public void run() {
                Counter1.setBackgroundColor(getResources().getColor(R.color.primary_light));



                handler.postDelayed(this, 30);

            }
        };  final Runnable updater2 = new Runnable() {
            @Override
            public void run() {
                Counter2.setBackgroundColor(getResources().getColor(R.color.redcolor));


                handler.postDelayed(this, 30);

            }
        };  final Runnable updater3 = new Runnable() {
            @Override
            public void run() {
                Counter3.setBackgroundColor(getResources().getColor(R.color.green));


                handler.postDelayed(this, 30);

            }
        };  final Runnable update4 = new Runnable() {
            @Override
            public void run() {

                Counter4.setBackgroundColor(getResources().getColor(R.color.blue));


                handler.postDelayed(this, 30);

            }
        };





        new CountDownTimer(millisInFuture,countDownInterval){
            public void onTick(long millisUntilFinished){


                //do something in every tick
                //Display the remaining seconds to app interface
                //1 second = 1000 milliseconds

                Random rand = new Random();
                for (int j=0;j < 8;j++)
                {
                    System.out.printf("%12d ",rand.nextInt());
                    for (int i = 0; i <rand.nextInt() ; i++) {
                        init = -1;
                        handler.postDelayed(updater, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(updater2, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(updater3, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(update4, 200+(new Random().nextInt(8000)));
                        try {
                            Thread.sleep(1000);

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                }


            }
            public void onFinish(){
                //Do something when count down finished
                tv_start_stop.setText("Time over...");
                Counter1.setBackgroundColor(getResources().getColor(R.color.white));
                Counter2.setBackgroundColor(getResources().getColor(R.color.white));
                Counter3.setBackgroundColor(getResources().getColor(R.color.white));
                Counter4.setBackgroundColor(getResources().getColor(R.color.white));
            }
        }.start();








        tv_start_stop.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {



                Random rand = new Random();
                for (int j=0;j < 8;j++)
                {
                    System.out.printf("%12d ",rand.nextInt());
                    for (int i = 0; i <rand.nextInt() ; i++) {
                        init = -1;
                        handler.postDelayed(updater, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(updater2, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(updater3, 200+(new Random().nextInt(8000)));
                        handler.postDelayed(update4, 200+(new Random().nextInt(8000)));


                    }

                }


                // Toast.makeText(TimerActivity.this, "Timeout", Toast.LENGTH_SHORT).show();





            }
        });









        //  startTimerCounter1(1);
         //startTimerCounter2(1);
         //startTimerCounter3(1);
         //startTimerCounter4(1);



        Counter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(correctStatus.equalsIgnoreCase("1")){

                    count1++;
                }
                else
                    count1--;

                textView6.setText("totalcount"+count1);




            }
        }); Counter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(correctStatus.equalsIgnoreCase("2")){

                    count1++;
                }
                else
                    count1--;

                textView6.setText("totalcount"+count1);
            }
        }); Counter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(correctStatus.equalsIgnoreCase("3")){

                    count1++;
                }
                else
                    count1--;

                textView6.setText("totalcount"+count1);
            }
        }); Counter4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(correctStatus.equalsIgnoreCase("4")){

                    count1++;
                }
                else
                    count1--;

                textView6.setText("totalcount"+count1);

            }
        });





    }

    private void startTimerCounter1(int noOfMinutes) {

        new Thread(new Runnable() {
            public void run() {
                correctStatus="1";
                Counter1.setBackgroundColor(getResources().getColor(R.color.redcolor));
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                correctStatus="2";
                Counter2.setBackgroundColor(getResources().getColor(R.color.orange_deep));
            }
        }).start();
        new Thread(new Runnable() {
            public void run() {
                correctStatus="3";
                Counter1.setBackgroundColor(getResources().getColor(R.color.green));
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                correctStatus="4";
                Counter1.setBackgroundColor(getResources().getColor(R.color.blue));
            }
        }).start();






    }

    private void startTimerCounter2(int noOfMinutes) {


        try {
            Thread.sleep(OneSecond);
            Counter2.setBackgroundColor(getResources().getColor(R.color.orange_deep));
        }
        catch (InterruptedException e) {

            e.printStackTrace();
        }



    }
    private void startTimerCounter3(int noOfMinutes) {
        try {
            Thread.sleep(OneSecond);
            Counter3.setBackgroundColor(getResources().getColor(R.color.green));
        }
        catch (InterruptedException e) {

            e.printStackTrace();
        }

    }
    private void startTimerCounter4(int noOfMinutes) {
        try {
            Thread.sleep(OneSecond);
            Counter4.setBackgroundColor(getResources().getColor(R.color.md_deep_purple_50));
        }
        catch (InterruptedException e) {

            e.printStackTrace();
        }

    }
}
