package com.example.admin.a100dark.Logic;

import android.location.Location;

/**
 * Created by Madhuri on 6/12/2018.
 */

public interface UpdateLocationInterface {

    public Location getLocation();
}
