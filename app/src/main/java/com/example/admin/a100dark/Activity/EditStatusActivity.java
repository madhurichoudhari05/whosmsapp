package com.example.admin.a100dark.Activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.admin.a100dark.R;

import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import org.apache.http.HttpException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.SocketTimeoutException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class EditStatusActivity extends AppCompatActivity {


    EditText userStatusEdit;

    private RelativeLayout cancelRelative, okRelative;
    private String selectedName;
    ImageView emojiIcon;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_status);
        userStatusEdit = findViewById(R.id.et_status);
        cancelRelative = findViewById(R.id.cancelRelative);
        okRelative = findViewById(R.id.okRelative);
        emojiIcon = findViewById(R.id.emojiStatusIcon);
        mContext=EditStatusActivity.this;


        userStatusEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return true;
                }
                return false;
            }
        });

        final View rootView = findViewById(R.id.statusRootView);




       // userStatusEdit.setText(CommonUtils.getPreferencesString(EditUsernameActivity.this, AppConstants.USER_NAME));

        cancelRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        okRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedName = userStatusEdit.getText().toString();
                callProfileData();
                finish();


            }
        });

        emojiIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // callProfileData();

            }
        });
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {

        iconToBeChanged.setImageResource(drawableResourceId);
    }


    private void callProfileData() {
        CommonUtils.showProgress(mContext);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call=null;
       String  user_id=CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
        RequestBody user_id1 = RequestBody.create(MediaType.parse("text/plain"),user_id);
        RequestBody emoji_staus = RequestBody.create(MediaType.parse("text/plain"),userStatusEdit.getText().toString());
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"),CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME) );
        call = service.uploadFileWithPartMap(user_id1, emoji_staus,name);
        call.enqueue(new Callback<ResponseBody>() {
            public boolean status;
            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();
                String str = "";
                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        boolean status = jsonObject.getBoolean("status");
                        String messageimg = jsonObject.getString("message");
                        CommonUtils.savePreferencesString(mContext,AppConstants.USER_PROFILE_STATUS,userStatusEdit.getText().toString());
                        // CommonUtils.savePreferencesString(mContext,AppConstants.USER_NAME,etName.getText().toString());
                        // CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
                        if (status) {
                            //  CommonUtils.snackBar(messageimg,cameraopen);
                            Toast.makeText(EditStatusActivity.this, messageimg, Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.dismissProgress();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }

}
