package com.example.admin.a100dark.chat.interfaces.lemda;

/**
 * Created by abul on 12/12/17.
 */
@FunctionalInterface
public interface Fun1ParamRet<T,R> extends Fun{
    R done(T t);
}
