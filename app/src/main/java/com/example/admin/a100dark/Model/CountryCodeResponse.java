package com.example.admin.a100dark.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by happypesa on 5/8/17.
 */

  public class CountryCodeResponse {

    @SerializedName("data")
    @Expose
    private List<CountryCodeDetails> data = null;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public List<CountryCodeDetails> getData() {
        return data;
    }

    public void setData(List<CountryCodeDetails> data) {
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    

}
