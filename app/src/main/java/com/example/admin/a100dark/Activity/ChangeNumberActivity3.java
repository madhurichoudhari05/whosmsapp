package com.example.admin.a100dark.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeNumberActivity3 extends AppCompatActivity {

    LinearLayout rightDoneLinear;

    EditText old_code_edit, new_code_edit, old_number_edit, new_number_edit;

    String oldCountryCode, oldMobileNo;
    private Context context;
    String userId = "";
    private ProgressDialog progressDialog;

    ImageView back_number3;

    FileUploadInterface fileUploadInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number3);
        rightDoneLinear = findViewById(R.id.rightDoneLinear);
        old_code_edit = findViewById(R.id.old_country_edit);
        new_code_edit = findViewById(R.id.new_country_edit);
        old_number_edit = findViewById(R.id.old_phone_edit);
        new_number_edit = findViewById(R.id.new_phone_edit);
        back_number3 = findViewById(R.id.back_number3);

        context = ChangeNumberActivity3.this;
        fileUploadInterface = RetrofitHandler.getInstance().getApi();

        oldCountryCode = CommonUtils.getPreferencesString(context, AppConstants.COUNTRY_CODE);
        oldMobileNo = CommonUtils.getPreferencesString(context, AppConstants.Registerd_MOBILE_NO);
        userId = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);

        old_code_edit.setText(oldCountryCode);
        old_number_edit.setText(oldMobileNo);
        new_code_edit.setText(oldCountryCode);

        back_number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rightDoneLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldumber = old_number_edit.getText().toString();
                String newumber = new_number_edit.getText().toString();
                String oldCode = old_code_edit.getText().toString();
                String newCode = new_code_edit.getText().toString();
                if (old_code_edit.length() == 0 || new_code_edit.length() == 0) {
                    showAlertDialog("Please enter phone number");
                } else if (oldumber.equals(newumber)) {
                    showAlertDialog("Old number and new number are same");
                } else {
                    progressDialog = new ProgressDialog(ChangeNumberActivity3.this);
                    progressDialog.setMessage("Connecting...");
                    progressDialog.show();
                    submitNumber(oldumber, newumber, oldCode, newCode);
                }
            }
        });
    }

    private void submitNumber(String oldumber, String newumber, String oldCode, String newCode) {

        Call<ResponseBody> bodyCall = fileUploadInterface.updateUserNo(oldumber, newumber, oldCode, newCode, userId);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                if (response != null) {
                    try {
                        String res = response.body().string();
                        JSONArray jsonArray = new JSONArray(res);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        Log.v("response", jsonObject.toString());
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            String OTP = jsonObject.getString("OTP");
                            CommonUtils.savePreferencesString(context, AppConstants.Registerd_MOBILE_NO, newumber);
                            showOTPDialog(OTP);
                        } else {
                            Toast.makeText(context, "Query Not Executed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Log.e("Error", t.toString());
            }
        });
    }

    private void showOTPDialog(String otp) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChangeNumberActivity3.this);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.otp_dialog, null);
        AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Enter OTP");
        alertDialog.setView(view);
        EditText otpEdit = view.findViewById(R.id.otp_edit);
        Button otpbutton = view.findViewById(R.id.otp_button);
        otpbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpEdit.length() == 0) {
                    Toast.makeText(context, "Please enter OTP", Toast.LENGTH_SHORT).show();
                } else if (!otpEdit.getText().toString().equals(otp)) {
                    Toast.makeText(context, "Please enter correct OTP", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Number Updated successfully", Toast.LENGTH_SHORT).show();
                    alertDialog.dismiss();
                    finish();
                }
            }
        });

        alertDialog.show();
    }

    private void showAlertDialog(String s) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ChangeNumberActivity3.this);
        builder.setMessage(s);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.show();
    }
}
