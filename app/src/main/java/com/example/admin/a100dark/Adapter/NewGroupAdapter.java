package com.example.admin.a100dark.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Model.Contact_model;
import com.example.admin.a100dark.Model.NewGroup_model;
import com.example.admin.a100dark.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Admin on 11/9/2017.
 */

public class NewGroupAdapter extends  RecyclerView.Adapter<NewGroupAdapter.ViewHolder> {

    ArrayList<NewGroup_model> arrayList = new ArrayList<>();
    Context context;

    public NewGroupAdapter(ArrayList<NewGroup_model> arrayList, Context context) {

        this.arrayList = arrayList;
        this.context = context;
    }
    @Override
    public NewGroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.newgroup_item, parent, false);

        NewGroupAdapter.ViewHolder recyclerViewHolder = new NewGroupAdapter.ViewHolder(view);
        return recyclerViewHolder;

    }

    @Override
    public void onBindViewHolder(NewGroupAdapter.ViewHolder holder, int position) {
        NewGroup_model newGroup_model = arrayList.get(position);
        holder.newgroupuserName.setText(newGroup_model.getNewGroup_name());
        holder.newgroputstatus.setText(newGroup_model.getNewGroup_status());

        Picasso.with(context).load(newGroup_model.getNewGroup_image()).into(holder.profileImage);

       /* if (position == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#00BFFF"));
        } else if (position == 1) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#E85E7F"));
        } else if (position == 2) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#0abc70"));
        } else if (position % 3 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#521155"));
        } else if (position % 4 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#f5b022"));
        } else if (position % 5 == 0) {
            holder.linearLayout.setBackgroundColor(Color.parseColor("#267dc5"));
        }*/

    }
    @Override
    public int getItemCount() {

        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView newgroupuserName, newgroputstatus;
        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            newgroupuserName = (TextView) itemView.findViewById(R.id.newgroupusername);
            newgroputstatus = (TextView) itemView.findViewById(R.id.newgroupusernametime);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_imagenewgropu);


        }
    }
}

