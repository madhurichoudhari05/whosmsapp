package com.example.admin.a100dark.Logic;

/**
 * Created by abma on 14/4/18.
 */

public class StarHalfPyramid {
    public static void main (String args[])
    {
        int n=5;
        for (int i = 0; i < 5; i++) {

            for (int j = i; j <=n/2+1 ; j++) {

                System.out.print(" ");

            }

            for (int k = 0; k <i*2+1 ; k++) {
                System.out.print("*");

            }
            System.out.println();
        }

    }
}
