package com.example.admin.a100dark.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.admin.a100dark.Adapter.MsgForwordAdapter;
import com.example.admin.a100dark.Adapter.SearchAdapter;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.Model.Message;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.RecyclerTouchListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Callback;


public class MsgForwordActivity extends AppCompatActivity implements View.OnClickListener ,MsgForwordAdapter.OnItemClicked {
    ImageView leftarrow;
    RecyclerView recycler_contac_list;
    int serverCount;
    int serverCountforGroup;
    String userScreenStatus = "";
    int msgLocalCount = 0;
    FloatingActionButton fab_send;
    public boolean isSelected=false;
   // List<DepartmentListDetailPojo> departmentList;
    List<AllUserListDetailsModel> userList = new ArrayList<>();
    List<AllUserListDetailsModel> searchUserList = new ArrayList<>();

    SharedPreferences sharedPreferences;
    static SharedPreferences.Editor editor;
    Context mContext;
  //  MsgForwordAdapter msgForwordAdapter;
    MsgForwordAdapter msgForwordAdapter,othersearchAdapter;

    EditText et_search;
    String token = "";
    boolean isSearch = false;
    boolean holderViewSelected=true;
    int selectedPosition=-1;
    int globalListSize;
    ArrayList<String> selectedID;
    ArrayList selectedObject;
    HashMap<String, String> hashMap;
    // String senderID = (CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID));
    String senderID = "";
    String senderName = "";
    String imageBaseUrl = "";
    private int user_id = 0;
    private List<Integer> group_arrayCountList;
    private static final String TAG = "MsgForwordActivity";
    ArrayList al=new ArrayList();
    ArrayList<Integer> selectionPosition;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideKeyboard(MsgForwordActivity.this);
        setContentView(R.layout.activity_msg_forword_activity);
        initializeViews();
        leftarrow.setOnClickListener(this);
        fab_send.setOnClickListener(this);

        callUserListApi();
       /* searchGroupList = new ArrayList<>();
        searchContactList = new ArrayList<>();*/
        selectedID = new ArrayList<>();
        selectedObject = new ArrayList<>();
        selectionPosition=new ArrayList<>();

        if(CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID)!=null&&(!CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID).equalsIgnoreCase("")))
            try {
                user_id = Integer.valueOf(CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID));
            } catch (Exception e) {
                e.printStackTrace();
            }

        msgForwordAdapter = new MsgForwordAdapter(userList, mContext,userList);
        recycler_contac_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recycler_contac_list.setAdapter(msgForwordAdapter);
        msgForwordAdapter.setOnClick(MsgForwordActivity.this);

        senderID = CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
        // senderName=CommonUtils.getPreferences(mContext,AppConstants.SENDER_USER_NAME);

        if (CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME) != null) {
            senderName = CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME);
        }

       /// imageBaseUrl = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
        hashMap = (HashMap<String, String>) getIntent().getSerializableExtra("map");
        // Log.e("HashMapTest", hashMap.get("type"));
        Log.e(TAG, "map Value " + hashMap.get("type"));
        Log.e(TAG, "map Value " + hashMap.get("message"));
        Log.e(TAG, "map Value " + hashMap.get("sender_id"));
        Log.e(TAG, "map Value " + hashMap.get("chatType"));
        Log.e(TAG, "map Value " + hashMap.get("sender_name"));
        Log.e(TAG, "map Value " + hashMap.get("timestamp"));
        Log.e(TAG, "map Value " + hashMap.get("time"));

        setupUI(recycler_contac_list);
        et_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    et_search.setCursorVisible(true);
                }else {
                    et_search.setCursorVisible(false);
                }


            }
        });


        token = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.LOGIN_AUTHENTICATE);
       // getGroupData(token);
        sharedPreferences = mContext.getSharedPreferences("STORE_FILE_NAME", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String serializedObject = sharedPreferences.getString("DEPARTMENTDATA", null);
        String serializedGroup = sharedPreferences.getString("GROUPDATA", null);


        if (token != null && !token.equalsIgnoreCase("")) {
            Log.e("GroupToken", "GroupToken::" + token);

        }

        Log.e(TAG, "size"+globalListSize );

        for (int i = 0; i <globalListSize ; i++) {
            al.add(i,false);
        }

/*
        recycler_contac_list.addOnItemTouchListener(new RecyclerTouchListener(this, recycler_contac_list, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                String id = null;
                HashMap<String, String> mapForSelectedObject = new HashMap();
                selectedID = new ArrayList<>();
                isSelected=true;
                id=  userList.get(position).getId();
                mapForSelectedObject.put("reciverName", userList.get(position).getUserName());

            if(isSelected){
              id=  userList.get(position).getId();
                mapForSelectedObject.put("reciverName", userList.get(position).getUserName());

               */
/* if (selectedID.contains(id)) {
                    selectedID.remove(id);
                    selectedObject.remove(mapForSelectedObject);
                } else {
                    selectedID.add(id);
                    selectedObject.add(mapForSelectedObject);
                }*//*


            }

                    selectedID.add(0,id);
                    selectedObject.add(0,mapForSelectedObject);

                mapForSelectedObject.size();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
*/



    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {

                    //hideSoftKeyboard(getActivity());
                    CommonUtils.hideKeyPad(MsgForwordActivity.this);
                    // et_search.setCursorVisible(false);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    protected void onResume() {
        super.onResume();

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    isSearch = false;
                    setAdapterr(userList);
                } else {
                    isSearch = true;
                   // gl = getSearchGroupList(s.toString());
                   // sc = getSearchContactList(s.toString());
                   setAdapterr( getSearchContactList(s.toString()));
                   // setAdapterr(sc, gl);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("TextWatcherTest", "beforeTextChanged:\t" + s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("TextWatcherTest", "afterTextChanged:\t" + s.toString());


            }
        });

    }
    public List<AllUserListDetailsModel> getSearchContactList(String charString) {
        List<AllUserListDetailsModel> filteredContactList = new ArrayList<>();
        for (int i = 0; i <= userList.size() - 1; i++) {
            AllUserListDetailsModel obj = new AllUserListDetailsModel();
            obj = userList.get(i);
            if (userList.get(i).getUserName()!= null) {
                if (userList.get(i).getUserName().toLowerCase().contains(charString.toLowerCase())) {
                    filteredContactList.add(obj);
                }
            }
        }
       // searchContactList = filteredContactList;


        return filteredContactList;
    }

    public void setAdapterr(List<AllUserListDetailsModel> userListdata) {
        msgForwordAdapter = new MsgForwordAdapter(userListdata, mContext, userListdata);
        recycler_contac_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recycler_contac_list.setAdapter(msgForwordAdapter);
        msgForwordAdapter.setOnClick(MsgForwordActivity.this);



        msgForwordAdapter.notifyDataSetChanged();

    }





    private void initializeViews() {
        mContext = MsgForwordActivity.this;
        leftarrow = findViewById(R.id.leftarrow);
        fab_send = findViewById(R.id.fab_send);
        recycler_contac_list = findViewById(R.id.recycler_contac_list);
        et_search = findViewById(R.id.et_search);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.leftarrow:
                onBackPressed();
                break;
            case R.id.fab_send:
                HashMap<String, String> msgMapData = new HashMap<String, String>();
                selectionPosition.size();

               // Toast.makeText(mContext, "selected Objects " + selectedObject.size(), Toast.LENGTH_SHORT).show();
              //  Toast.makeText(mContext, "Selected IDs " + selectedID.size(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, "selected List Size : "+selectedObject.size() );
                for (int i = 0; i <= selectedID.size() - 1; i++) {
                    String idStr = selectedID.get(i);
                    if (idStr.contains("Group_")) {
                        msgMapData = (HashMap<String, String>) selectedObject.get(i);
                        getMsgCountForGroup(idStr, msgMapData);
                        forwordToGroup(idStr, msgMapData);
                    } else {
                        msgMapData = (HashMap<String, String>) selectedObject.get(i);
                        //   String s=((HashMap) selectedObject.get(i)).getOrDefault("recieverName","");
                        String chatID = "";
                        int senderID = Integer.parseInt((CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID)));
                        int recieverID = Integer.parseInt(idStr);

                        if (senderID > recieverID) {
                            chatID = recieverID + "_" + senderID;
                        } else {
                            chatID = senderID + "_" + recieverID;
                        }
                        getReciverStatus(chatID, String.valueOf(recieverID), msgMapData);
                        forwordIndivisual(chatID, String.valueOf(recieverID), msgMapData);

                    }
                    // MsgForwordActivity.this.finish();
                }

                MsgForwordActivity.this.finish();

                break;

            case R.id.recycler_contac_list:

                break;
        }

    }


    public void forwordIndivisual(String chatID, String recieverID, HashMap detailObject) {
//        String imageurl = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
        String recieverName = detailObject.get("reciverName").toString();
     //  String recieverImqage = imageurl + detailObject.get("reciverImage").toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String time = CommonUtils.getDate((System.currentTimeMillis()));
        String type = hashMap.get("type");
        String message = hashMap.get("message");

        if (type.equalsIgnoreCase("3")) {
            String fileName = hashMap.get("fileName");
            String fileSize = hashMap.get("fileSize");

            String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
            Message newMessage = new Message();
            newMessage.type = type;
            newMessage.message = message;
            newMessage.filename = fileName;
            newMessage.fileSize = fileSize;
            newMessage.sender_id = senderID;
            newMessage.reciever_id = recieverID;
            newMessage.chatType = "oneToOne";
            newMessage.reciever_name = recieverName;
     //       newMessage.reciever_img = recieverImqage;
            newMessage.sender_name = senderName;
            newMessage.sender_img = senderImage;
            newMessage.timestamp = timeStamp;
            newMessage.isForwardedMsg = "1";
            newMessage.time = time;
            // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatID).child("messages/").push().setValue(newMessage);

        } else {

            String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
            Message newMessage = new Message();
            newMessage.type = type;
            newMessage.message = message;
            newMessage.sender_id = senderID;
            newMessage.reciever_id = recieverID;
            newMessage.chatType = "oneToOne";
            newMessage.reciever_name = recieverName;
           // newMessage.reciever_img = recieverImqage;
            newMessage.sender_name = senderName;
            newMessage.sender_img = senderImage;
            newMessage.timestamp = timeStamp;
            newMessage.isForwardedMsg = "1";
            newMessage.time = time;
            // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatID).child("messages/").push().setValue(newMessage);


            Message OpponentMsg = new Message();
            OpponentMsg.type = type;
            OpponentMsg.message = message;
            OpponentMsg.oponent_id = recieverID;
            OpponentMsg.oponent_name = recieverName;
            OpponentMsg.chatId = chatID;
            OpponentMsg.chatType = "oneToOne";
          //  OpponentMsg.oponent_img = recieverImqage;
            OpponentMsg.timestamp = timeStamp;
            OpponentMsg.time = time;

            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(senderID).child(chatID).child("messages/").setValue(OpponentMsg);

            Message updatedMsgForReciver = new Message();
            updatedMsgForReciver.type = type;
            updatedMsgForReciver.message = message;
            updatedMsgForReciver.oponent_id = senderID;
            updatedMsgForReciver.oponent_name = senderName;
            updatedMsgForReciver.chatId = chatID;
            updatedMsgForReciver.chatType = "oneToOne";
            updatedMsgForReciver.oponent_img = senderImage;
            updatedMsgForReciver.timestamp = timeStamp;
            updatedMsgForReciver.time = time;
            updatedMsgForReciver.msgCount = msgLocalCount;
//            if (userScreenStatus.equalsIgnoreCase("1")) {
//                updatedMsgForReciver.msgCount = 0;
//            } else {
//               /* serverCount++;
//                updatedMsgForReciver.msgCount = serverCount;*/
//                msgLocalCount = serverCount;
//                msgLocalCount = msgLocalCount + 1;
//                updatedMsgForReciver.msgCount = msgLocalCount;
//
//            }

            //FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(recieverID).child(chatID).child("messages/").setValue(updatedMsgForReciver);


        }

        String login_auth = "";
        String user_name = "";
        String chatMsgWithUser = "";

        // if (CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE) != null && (CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) != null))
        login_auth = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.LOGIN_AUTHENTICATE);
        user_name = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.USER_NAME);
       // chatMsgWithUser = user_name + " : " + message;


        if(type.equalsIgnoreCase("2")){
            chatMsgWithUser = user_name + " : " + "Shared an image";
        }else if(type.equalsIgnoreCase("3")){
            chatMsgWithUser = user_name + " : " + "Shared a file";
        }else {
            chatMsgWithUser = user_name + " : " + message;}



        // callNotificationApi(login_auth,user_name,"group", notificationReceiverid,chatMsgWithUser);
      //  callNotificationApi2(login_auth, user_name, "Single", recieverID, chatMsgWithUser);

    }

    public void forwordToGroup(String chatID, HashMap detailObject) {
       /* String nodeID = detailObject.get("groupNodeID").toString();
        String reciverName = detailObject.get("groupName").toString();
        String groupID = detailObject.get("groupID").toString();
        String imageurl = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
        String recieverImqage = imageurl + detailObject.get("groupImage").toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String time = CommonUtils.getDate((System.currentTimeMillis()));
        String type = hashMap.get("type");
        String message = hashMap.get("message");
        if (type.equalsIgnoreCase("3")) {
            String fileName = hashMap.get("fileName");
            String fileSize = hashMap.get("fileSize");

            String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
            Message newMessage = new Message();
            newMessage.type = type;
            newMessage.message = message;
            newMessage.filename = fileName;
            newMessage.fileSize = fileSize;
            newMessage.sender_id = senderID;
            newMessage.reciever_id = nodeID;
            newMessage.chatType = "Group";
            newMessage.reciever_name = reciverName;
            newMessage.reciever_img = recieverImqage;
            newMessage.sender_name = senderName;
            newMessage.sender_img = senderImage;
            newMessage.timestamp = timeStamp;
            newMessage.isForwardedMsg = "1";
            newMessage.time = time;
            // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatID).child("messages/").push().setValue(newMessage);


        } else {

            String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
            Message newMessage = new Message();
            newMessage.type = type;
            newMessage.message = message;
            newMessage.sender_id = senderID;
            newMessage.reciever_id = nodeID;
            newMessage.chatType = "Group";
            newMessage.reciever_name = reciverName;
            newMessage.reciever_img = recieverImqage;
            newMessage.sender_name = senderName;
            newMessage.sender_img = senderImage;
            newMessage.timestamp = timeStamp;
            newMessage.isForwardedMsg = "1";
            newMessage.time = time;
            // newMessage.time=CommonUtils.getTimeFormatMilis(String.valueOf(System.currentTimeMillis()));
            FirebaseDatabase.getInstance().getReference().child("chatlist").child(chatID).child("messages/").push().setValue(newMessage);


        }

        Message OpponentMsg = new Message();
        OpponentMsg.type = type;
        OpponentMsg.message = message;
        //  OpponentMsg.oponent_id = chatId;
        OpponentMsg.oponent_id = nodeID;
        OpponentMsg.chatId = chatID;
        OpponentMsg.group_id = groupID;
        OpponentMsg.oponent_name = reciverName;
        OpponentMsg.chatType = "Group";
        OpponentMsg.oponent_img = recieverImqage;
        OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
        OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
        FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(senderID)).child(chatID).child("messages/").setValue(OpponentMsg);

        String login_auth = "";
        String user_name = "";
        String chatMsgWithUser = "";

        // if (CommonUtils.getPreferences(mContext, AppConstants.LOGIN_AUTHENTICATE) != null && (CommonUtils.getPreferences(mContext, AppConstants.USER_NAME) != null))
        login_auth = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.LOGIN_AUTHENTICATE);
        user_name = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.USER_NAME);

        if(type.equalsIgnoreCase("2")){
            chatMsgWithUser = user_name + " : " + "Shared an image";
        }else if(type.equalsIgnoreCase("3")){
            chatMsgWithUser = user_name + " : " + "Shared a file";
        }else {
            chatMsgWithUser = user_name + " : " + message;}
*/
      //  chatMsgWithUser = user_name + " : " + message;

        // callNotificationApi(login_auth,user_name,"group", notificationReceiverid,chatMsgWithUser);
       // callNotificationApi2(login_auth, user_name, "group", groupID, chatMsgWithUser);


    }
  //  private void callNotificationApi2(String login_auth, String user_name, String single, String notificationReceiverid, String chatMsgWithUser) {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        retrofit2.Call<ResponseBody> call = null;
      /*  try {
            call = service.getNotification(login_auth, single, notificationReceiverid, chatMsgWithUser, FirebaseInstanceId.getInstance().getToken(), user_name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                Log.e("notificationReceiverid", "notificationReceiverid::" + notificationReceiverid);
                String str = "", msg = "";
                int status = 0;
                try {
                    if (response != null) {
                        if (response.body() != null) {
                            str = response.body().string();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                    //Toast.makeText(mContext, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    if (jObjError.getString("message").equalsIgnoreCase("inactive")) {
                        CommonUtils.inActivieDailog(mContext, jObjError.getString("message"), "inactive");
                        //   CommonUtils.inActivieDailog(context);
                    }

                } catch (Exception e) {
                    // Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }

                try {

                    if (str != null) {
                        JSONObject jsonTop = new JSONObject(str);
                        status = jsonTop.getInt("success");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {

                    if (str != null) {
                        JSONObject jsonTop = new JSONObject(str);
                        msg = jsonTop.getString("message");
                        status = jsonTop.getInt("code");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status == 409) {
                    // CommonUtils.logoutNotificationApi(context);
                    CommonUtils.inActivieDailog(mContext, msg, "inactive");
                    Log.e("send", "send::" + status);
                    Log.e("send", "You are Deactivated  by Admin");

                }

                // status = 420 ;
                if (status == 420) {
                    CommonUtils.logoutNotificationApi(mContext);
                    Log.e("send", "send::" + status);
                    Log.e("send", "Please provide your valid device token");

                    try {
                        JSONObject jsonTop = new JSONObject(str);
                        msg = jsonTop.getString("message");
                        Log.e("ASD", msg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    CommonUtils.snackBar(msg, leftarrow);

                }

                if (status == 421) {
                    if ((msg).equalsIgnoreCase("suspend")) {
                        CommonUtils.inActivieDailog(mContext, msg, "suspend");

                        Log.e("send", "send::" + status);
                        Log.e("inactive", "Please provide your valid device token");
                    }

                }
                else {
                    Log.e("send", "send::" + status);
                }

            }

            @Override
            public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {
            }
        });
    }
*/


    private void getReciverStatus(String chatID, String recieverID, HashMap detailObject) {
     //   String imageurl = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
        String recieverName = detailObject.get("reciverName").toString();
     //   String recieverImqage = imageurl + detailObject.get("reciverImage").toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String time = CommonUtils.getDate((System.currentTimeMillis()));
        String type = hashMap.get("type");
        String message = hashMap.get("message");
        FirebaseDatabase.getInstance().getReference().child("UserStatus").child(recieverID).child(chatID).child("Status").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange-");
                userScreenStatus = (String) dataSnapshot.getValue();

                if(userScreenStatus==null){
                    userScreenStatus="0";
                }

                if (userScreenStatus.equalsIgnoreCase("0")) {
                    getMsgContIndividual(chatID, recieverID, detailObject);
                } else {
                    msgLocalCount = 0;
                    String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
                    Message updatedMsgForReciver = new Message();
                    updatedMsgForReciver.type = type;
                    updatedMsgForReciver.message = message;
                    updatedMsgForReciver.oponent_id = senderID;
                    updatedMsgForReciver.oponent_name = senderName;
                    updatedMsgForReciver.chatId = chatID;
                    updatedMsgForReciver.chatType = "oneToOne";
                    updatedMsgForReciver.oponent_img = senderImage;
                    updatedMsgForReciver.timestamp = timeStamp;
                    updatedMsgForReciver.time = time;
                    updatedMsgForReciver.msgCount = msgLocalCount;


               //     FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(recieverID).child(chatID).child("messages/").setValue(updatedMsgForReciver);







                    // getMsgContIndividual(chatID, recieverID, detailObject);

                   // forwordIndivisual(chatID, recieverID, detailObject);
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "onCancelled-");
            }
        });

    }

    public void getMsgContIndividual(String chatID, String recieverID, HashMap detailObject) {

     ///   String imageurl = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
        String recieverName = detailObject.get("reciverName").toString();
     //   String recieverImqage = imageurl + detailObject.get("reciverImage").toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String time = CommonUtils.getDate((System.currentTimeMillis()));
        String type = hashMap.get("type");
        String message = hashMap.get("message");

        FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(recieverID).child(chatID).child("messages").child("msgCount").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange-");
           // String counnt=String.valueOf(dataSnapshot.getValue());
           /// int c=Integer.paI

                Long l=(Long)dataSnapshot.getValue();
                if((Long)dataSnapshot.getValue()==null){
                    serverCount=0;
                }else {
                    serverCount=l.intValue();
                }

               // serverCount= (Integer) dataSnapshot.getValue();
               /* if(counnt==null||counnt.equalsIgnoreCase("null")){
                    serverCount=0;
                }
                serverCount=Integer.parseInt(counnt);*/

                //serverCount = Integer.parseInt(String.valueOf(dataSnapshot.getValue()));
                msgLocalCount = serverCount;
                msgLocalCount = msgLocalCount+1;
                String senderImage = CommonUtils.getPreferences(mContext, wehyphens.com.satyaconnect.utils.AppConstants.PROFILE_PIC);
                Message updatedMsgForReciver = new Message();
                updatedMsgForReciver.type = type;
                updatedMsgForReciver.message = message;
                updatedMsgForReciver.oponent_id = senderID;
                updatedMsgForReciver.oponent_name = senderName;
                updatedMsgForReciver.chatId = chatID;
                updatedMsgForReciver.chatType = "oneToOne";
                updatedMsgForReciver.oponent_img = senderImage;
                updatedMsgForReciver.timestamp = timeStamp;
                updatedMsgForReciver.time = time;
                updatedMsgForReciver.msgCount = msgLocalCount;


                FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(recieverID).child(chatID).child("messages/").setValue(updatedMsgForReciver);
               // forwordIndivisual(chatID, recieverID, detailObject);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "onCancelled-");
            }
        });






   /* FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(recieverID).child(chatID).addChildEventListener(new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            if (dataSnapshot.getValue() != null) {
                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                Long count = (Long) mapMessage.get("msgCount");
                Integer firebasecount = count != null ? count.intValue() : null;
                serverCount = firebasecount;
                Log.e(TAG, "onChildAdded msgCount-" + firebasecount);

                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            if (dataSnapshot.getValue() != null) {
                HashMap mapMessage = (HashMap) dataSnapshot.getValue();
                Long count = (Long) mapMessage.get("msgCount");
                Integer firebasecount = count != null ? count.intValue() : null;
                serverCount = firebasecount;
                Log.e(TAG, "child change msgcount-" + firebasecount);

                //    Toast.makeText(mContext, "GetCount::" + st, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }


    });
*/

//return serverCount;
    }

    public void getMsgCountForGroup(String chatID, HashMap detailObject) {
        String nodeID = detailObject.get("groupNodeID").toString();
        String reciverName = detailObject.get("groupName").toString();
        String groupID = detailObject.get("groupID").toString();
      //  String imageurl = CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.IMAGE_URL);
     //   String recieverImqage = imageurl + detailObject.get("groupImage").toString();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String time = CommonUtils.getDate((System.currentTimeMillis()));
        String type = hashMap.get("type");
        String message = hashMap.get("message");

        int k = 0;
        List<String> group_user_list = new ArrayList<>();
        for (int i = 0; i < nodeID.length(); i++) {
            group_user_list = Arrays.asList(nodeID.split("_"));
        }

        //TODO Create UpdateList
        group_arrayCountList = new ArrayList<>();

        for (k = 0; k < group_user_list.size(); k++) {
            group_arrayCountList.add(0);
            group_arrayCountList.set(k, 0);
            String group_receiver_idArray = group_user_list.get(k).toString();

            if (group_receiver_idArray.equalsIgnoreCase(senderID)) {

            } else {
                int finalK = k;
                FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(group_receiver_idArray).child(chatID).child("messages").child("msgCount").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.e(TAG, "onDataChange-");
                        //serverCount = Integer.parseInt(String.valueOf(dataSnapshot.getValue()));
                        // forwordIndivisual(chatID, group_receiver_idArray, detailObject);

                        try {
                            serverCountforGroup = Integer.parseInt(String.valueOf(dataSnapshot.getValue()));
                            serverCountforGroup = serverCountforGroup + 1;
                            Message OpponentMsg = new Message();
                            OpponentMsg.type = type;
                            OpponentMsg.message = message;
                            //  OpponentMsg.oponent_id = chatId;
                            OpponentMsg.oponent_id = nodeID;
                            OpponentMsg.chatId = chatID;
                            OpponentMsg.group_id = groupID;
                            OpponentMsg.oponent_name = reciverName;
                            OpponentMsg.chatType = "Group";
                   //         OpponentMsg.oponent_img = recieverImqage;
                            OpponentMsg.timestamp = String.valueOf(System.currentTimeMillis());
                            OpponentMsg.time = CommonUtils.getDate((System.currentTimeMillis()));
                            OpponentMsg.msgCount = serverCountforGroup;
                            FirebaseDatabase.getInstance().getReference().child("UpdatedMsg").child(String.valueOf(group_receiver_idArray)).child(chatID).child("messages/").setValue(OpponentMsg);


                            // Long count = (Long) dataSnapshot.get("msgCount");
                            //Integer firebasecount = count != null ? count.intValue() : null;
                            // serverCount = firebasecount;
                            //  group_arrayCountList.set(finalK, serverCount);
                            //  group_arrayCountList.size();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled-");
                    }
                });

            }
        }

    }

    private void callUserListApi() {
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
        retrofit2.Call<List<AllUserListModel>> call = service.getUserListApi(user_id);
        call.enqueue(new Callback<List<AllUserListModel>>() {
            @Override
            public void onResponse(retrofit2.Call<List<AllUserListModel>> call, retrofit2.Response<List<AllUserListModel>> response) {
                List<AllUserListModel> userListModelList = response.body();
                userList.clear();
                try {
                    if (userListModelList.get(0).getCurrentUser().getUserName() != null) {
                        CommonUtils.savePreferencesString(mContext, AppConstants.USER_NAME, userListModelList.get(0).getCurrentUser().getUserName());
                        CommonUtils.savePreferencesString(mContext,AppConstants.SENDER_PROFILE_PIC_server,userListModelList.get(0).getCurrentUser().getUserPic());
                    }
                }catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (userListModelList != null && userListModelList.size() > 0) {
                    for (int i = 0; i < userListModelList.size(); i++) {


                        if(userListModelList.get(i).getAllUser().get(i).getId().equalsIgnoreCase(String.valueOf(user_id))){
                            //current user will not display in UserList
                        }else {
                            userList.addAll(userListModelList.get(i).getAllUser());
                        }

                    }
                    msgForwordAdapter.notifyDataSetChanged();
                } else {
                    CommonUtils.snackBar("Not Fetched", leftarrow);
                }

                //   Toast.makeText(mContext, "Chat", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(retrofit2.Call<List<AllUserListModel>> call, Throwable t) {
            }
        });
    }


    @Override
    public void onItemClick(int position) {
      //  selectionPosition = new ArrayList<>();
        String id = null;
        HashMap<String, String> mapForSelectedObject = new HashMap();
        selectedID = new ArrayList<>();
        isSelected=true;
        id=  userList.get(position).getId();
        mapForSelectedObject.put("reciverName", userList.get(position).getUserName());

       /* if (selectionPosition.contains(position)){
            selectionPosition.remove(selectionPosition.indexOf(position));
        } else {
            selectionPosition.add(position);
                }*/
        selectionPosition.size();
        selectedID.add(0,id);
        selectedObject.add(0,mapForSelectedObject);

    }
}
