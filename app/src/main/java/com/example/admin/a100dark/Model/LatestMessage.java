package com.example.admin.a100dark.Model;



public class LatestMessage implements Comparable {

    public void setMessage(String message) {
        this.message = message;
    }

    public String message;
    public String type;
    public String time;
    public String oponent_id;

    public int getMsgCount() {
        return msgCount;
    }

    public void setMsgCount(int msgCount) {
        this.msgCount = msgCount;
    }

    public int msgCount;

    public boolean isCountCheck() {
        return countCheck;
    }

    public void setCountCheck(boolean countCheck) {
        this.countCheck = countCheck;
    }

    public boolean countCheck;






    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String chatId;

    public String getChatType() {
        return chatType;
    }

    public void setChatType(String chatType) {
        this.chatType = chatType;
    }

    public String chatType;

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }

    public String getTime() {
        return time;
    }

    public String getOponent_id() {
        return oponent_id;
    }

    public String getOponent_name() {
        return oponent_name;
    }

    public String getOponent_img() {
        return oponent_img;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String oponent_name;
    public String oponent_img;
    public String timestamp;
    public String group_id;















   /* public boolean getIsonline() {
        return isonline;
    }

    public void setIsonline(boolean isonline) {
        this.isonline = isonline;
    }

    public boolean isonline;

    public long getTimestamp() {
        return timestamp;
    }*/

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public int compareTo(Object another) {
        String tmpDate = ((LatestMessage) another).getTimestamp();
        return this.getTimestamp().compareToIgnoreCase(tmpDate);
    }


}