package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 10/31/2017.
 */

public class ChatModel {


    private String name;
    private String status;
    private String time;
    private int image;

    public ChatModel(String name, String status, String time, int image) {
        this.name = name;
        this.status = status;
        this.time = time;
        this.image = image;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
