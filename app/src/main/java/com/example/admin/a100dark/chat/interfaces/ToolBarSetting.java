package com.example.admin.a100dark.chat.interfaces;

/**
 * Created by abul on 1/12/17.
 */

public interface ToolBarSetting {
    void setTitle(String title);
    void hideToolbar();
    void showToolbar();
    void hideBack();
}
