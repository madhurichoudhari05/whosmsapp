package com.example.admin.a100dark.utils;

import android.support.v4.app.Fragment;

public class AppConstants extends Fragment {
    public static final int MULTI_RESPONSE_CODE = 11;
    public static  final String IS_LOGIN = "false";
    public static  final String FIRST_TIME_LOGIN = "false";
    public static  final String FIRST_TIME_LOGOUT = "false";
    public static final String USER_STATUS = "user_status";
    public static  final String SIGNUP_OTP = "otp";
    public static  final String FORGOT_PASSWORD = "true";
    public static final String SENDER_RECEIVER_ID ="callerId";
    public static  final String PRIVACY = "true";
    public static  final String FORGOT_EMAIL_NUMBER = "email";

    public static  final String FIREBASE_KEY = "fire_base";
    public static  final String REQUEST_CHAT_KEY = "true";
    public static  final String UNREAD_MESSAGE_KEY = "request_chat";
    public static  final String REQUEST_MESSAGE_KEY = "message_chat";
    public static  final String NUMBER_OF_REQUEST_KEY = "request_chat";
    public static  final String CONTACt_EMail_KEY = "email";
    public static  final String CONTACt_EMail_KEY_panding = "emailpanding";

    public static  final String COUNT_KEY = "count";
    public static  final String USER_PROFILE_STATUS = "Available";
    public static  final String PITCH = "pitch";
    public static  final String KEY_FILTER = "false";
    public static  final String KEY_OTP = "otp";

    public static final String FILTER_DATA = "filter_data";



    public static final String USER_EMAIL = "email";


    public static final String SELECTED_CITY_CURRENT = "selected_city_current";
    public static final String CURRENT_LAT = "current_lat";
    public static final String USER_ID = "user_id";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String DRAWER_PIC = "drawer_pic";
    public static final String MOBILE_NO = "mobile_no";
    public static final String Registerd_MOBILE_NO = "mobile_no";
    public static final String PROFILE_PERCENTAGE = "profile_percentage";
    public static final String USER_NAME = "user_name";
    public static final String COUNTRY_CODE = "country_code";
    public static final String SENDER_USER_NAME = "sender_name";
    public static final String SENDER_PROFILE_PIC = "sender_pic";
    public static final String SENDER_PROFILE_PIC_server = "sender_pic_server";

    public static final String CURRENT_LONGI = "current_longi";
    public static String LAT = "lat";
    public static String LONGI = "longi";
    public static final String CurrentCity="city_name";
    public static final String StateName ="country_name";
    public static final String RECEIVER_USER_ID ="sender_id";
    public static final String RECEIVER_MOBILE ="sender_id";
    public static final String RECEIVER__USER_NAME2 ="sender_name";
    public static final String RECEIVER__PIC ="receiver_pic";
    public static final String CURRENT_USER ="current_user";
  //  public static final String HASH_KEY ="hash_key";
    public static final String HASHTAG_KEY = "hashtag";
    public static final String ALL_HASHTAG_KEY = "hashtagkey";
    public static final String NOTIFICATION_KEY = "notification";
    public static final String FOLLOWED_POST_KEY = "followed_post";


    public static final int FRAGMENT_HOME=0;
    public static final int FRAGMENT_NOTIFICATION=1;
    public static final int FRAGMENT_ALL=2;


    public static final int FRAGMENT_POSTS=0;
    public static final int FRAGMENT_POLLS=1;
    public static final int FRAGMENT_FOLLOED_POSTS=2;
    public static final int FRAGMENT_FOLLOWED_POLLS=3;
    public static final int FRAGMENT_COMMENT_REPLY=4;

    /*For calling */

    public static final String SENDER_CALLER_ID ="callerId";
    public static final String RECEIVER_CALLER_ID ="recipientId";
    public static final String RECEIVER_ID ="receiver_id";
    public static final String DEVICE_TOKEN ="device_token";
    public static final String PHONE_CALL_STATUS ="status";
    public static final int AUDIO_CALL_STATUS =12;



















    public static final String LOGIN_AUTHENTICATE = "login";

    public static  final String FNAME = "fname";
    public static  final String PASSWORD = "password";
    public static  final String CHECK_CHECKED = "checked";
    public static  final String USER_GROUP = "user";



    public static  final String VIEW_TYPE = "help";






    public static final String FROM_GROUP = "group";
    public static final String GROUP_NODE = "group_node";
    public static final String GROUP_USER_LIST_NAME = "grouplist";
    public static final String GROUP_ID = "group_id";


    public static boolean isProfileData=false;
    public static String GROUP_PIC="group_pic";
    public static String GROUP__USER_NAME="group_user_name";
    public static String GROUP_NAME="group_name";
    public static String FROM_RECENT_GROUP="group_recent_chat";
    public static String FIREBASE_GROUP_ID="firebase_group_id";




    public static class SharedPrefKeys {
        public static final String MY_SHAREDPREF_NAME = "com.netwrko";
        public static final String contentType = "application/json";



    }

}
