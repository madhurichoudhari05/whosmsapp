package com.example.admin.a100dark.interfaces;

import com.example.admin.a100dark.Model.CountryCodeDetails;
import com.example.admin.a100dark.Model.CountryCodeResponse;

import java.util.List;

/**
 * Created by abma on 10/2/18.
 */

public interface StateInterface {
    public  void setValue(String name, String id,boolean selected);

    void setValue(List<CountryCodeDetails> currentSelected, int type);
}
