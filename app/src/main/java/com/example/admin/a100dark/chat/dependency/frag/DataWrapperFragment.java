package com.example.admin.a100dark.chat.dependency.frag;

import android.os.Bundle;

import com.example.admin.a100dark.chat.app.ui.SuperAndroidViewModel;
import com.example.admin.a100dark.chat.interfaces.IConstants;


/**
 * Created by abul on 29/11/17.
 */

public abstract class DataWrapperFragment extends SuperFragment {

    protected SuperAndroidViewModel mViewModel;


    protected boolean isUserLoaded;
    protected boolean isFlatLoaded;
    protected boolean isBothLoaded;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    /*View model retrofit control*/
    protected void setStatusListener(SuperAndroidViewModel viewModel) {
        mViewModel=viewModel;
        mViewModel.getStatus().observe(this, staus -> {
            switch (staus) {
                case IConstants.INetwork.RET_START:
                    showProgress("Please Wait");
                    break;
                case IConstants.INetwork.RET_COMPLETE:
                    dismissProgress();
                    break;
                case IConstants.INetwork.RET_ERROR:

                    break;
                case IConstants.INetwork.RET_NEXT:

                    break;
            }
        });

        mViewModel.getError().observe(this, e -> {
            showToast(e.getMessage() + "");
            printLog(e.getMessage() + "");
        });

        mViewModel.getMsgSnak().observe(this, msg -> {
            showSnackBar(msg + "");
            printLog(msg + "");
        });

        mViewModel.getMsgToast().observe(this, msg -> {
            showToast(msg+ "");
            printLog(msg + "");
        });
    }
    /*===========================================================*/
}
