package com.example.admin.a100dark.retrofit;



import com.example.admin.a100dark.Model.AllUserListModel;
import com.example.admin.a100dark.Model.CountryCodeResponse;
import com.example.admin.a100dark.Model.LoginChatResponse;
import com.example.admin.a100dark.Model.OTPVerifyModel;
import com.example.admin.a100dark.Model.RegisterrationModel;
import com.example.admin.a100dark.Model.UploadImage;
import com.example.admin.a100dark.Model.UserTokenModel;
import com.example.admin.a100dark.chat.app.retofit.model.TokenResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface FileUploadInterface {








    @FormUrlEncoded
    @POST("users/logout.json")
    Call<ResponseBody> getLogout(@Header("Authorization") String authorization,
                                 @Field("device_token") String device_token,
                                 @Field("type") String type
    );


    @FormUrlEncoded
    @POST("register.php")
    Call<List<RegisterrationModel>> getImahe(@Field("user_id") String user_id,
                                          @Field("status") String satauus,
                                          @Field("name") String name);

    @FormUrlEncoded
    @POST("register.php")
    Call<List<RegisterrationModel>> getRegister(@Field("deviceToken") String device_token,
                                                @Field("user_mobile") String user_mobile,
                                                @Field("mobile_code") String mobile_code);


    @Multipart
    @POST("userPic.php")
    Call<ResponseBody> uploadFileProfileile(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part[] file);





    @Multipart
    @POST("userPic.php")
    Call</*ArrayList<PostResponse>*/ResponseBody> uploadFileWithPartMap(
            @Part("user_id") RequestBody user_id,
            @Part("status") RequestBody status,
            @Part("name") RequestBody name);




    @FormUrlEncoded
    @POST("chat/device_registration.php")
    Call<LoginChatResponse> getChatIntegration(@Field("user_id") int user_id, @Field("token") String token);


    @FormUrlEncoded
    @POST("verified-otp.php")
    Call<List<OTPVerifyModel>> getOTPVerify(@Field("user_id") Integer user_id, @Field("otp") Integer otp);

    @FormUrlEncoded
    @POST("country-code.php")
    Call<CountryCodeResponse> getCountry_Code(@Field("user_id") String user_id);

 /*   @FormUrlEncoded
    @POST("/NetWrko/WebServices/chat/devicetoken_get.php")
    Observable<TokenResponse> getFcmToken(@Field("user_id") String user_id);*/



    @FormUrlEncoded
    @POST("userDetail.php")
   // Observable<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);
    Call<List<UserTokenModel>> getFcmToken(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("allUser.php")
    Call<List<AllUserListModel>> getUserListApi(@Field("user_id") Integer user_id);

    @Multipart
    @POST("imageUpload.php")
    Call<List<UploadImage>> uploadImageList(
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part[] file);


    @POST("change_number.php")
    @FormUrlEncoded
    Call<ResponseBody> updateUserNo(@Field("old_number") String oldNo,@Field("new_number") String newNo,
                                    @Field("old_country_code") String oldCode,@Field("new_country_code") String newCode,
                                    @Field("user_id") String user_id);

    @POST("delete_number.php")
    @FormUrlEncoded
    Call<ResponseBody> deleteUser(@Field("mobile_number") String mobile_number);

    @FormUrlEncoded
    @POST("notification.php")
    Call<ResponseBody> getNotification(
                                       @Field("id") String to,
                                       @Field("message") String message_body,
                                       @Field("device_token") String device_token);

}
