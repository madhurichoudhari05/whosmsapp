package com.example.admin.a100dark.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.admin.a100dark.Adapter.ChatArrayAdapter;
import com.example.admin.a100dark.Fragment.ChatMessage;
import com.example.admin.a100dark.R;



import java.io.File;
import java.io.IOException;

import static android.R.attr.bottom;
import static android.R.attr.data;

/**
 * Created by Admin on 11/2/2017.
 */

public class ChatActivirtyMesssage extends AppCompatActivity {

private  Context context;
    Toolbar toolbar;
    View vv;
    ImageView leftarrow;
    private static final String TAG = "ChatActivity";
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private boolean side = false;
    ImageView arrowmessenger;
   EditText emojiconEditText;
    private ImageView emojiButton;

    ImageView sendText;
    ImageView camera;
    int TAKE_PHOTO_CODE = 0;
    public static int count = 0;
    ImageView attachfile;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.userchatmassangar);
 context = ChatActivirtyMesssage.this;
        sendText = (ImageView)findViewById(R.id.send) ;

        listView = (ListView) findViewById(R.id.msgview);

        final View rootView = findViewById(R.id.root_view);

        attachfile = (ImageView)findViewById(R.id.attachfile);

        attachfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                eventDialog() ;

               /* final android.support.v7.app.AlertDialog dialog = new android.support.v7.app.AlertDialog.Builder(context).create();

                LayoutInflater inflater = LayoutInflater.from(context);

                View dialogLayout = inflater.inflate(R.layout.attachfile,null);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.setContentView(R.layout.dilaogeboxforemailid);
                dialog.setView(dialogLayout);
//                dialog.getWindow().setGravity(bottom);
                dialog.getWindow().getAttributes();
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.BOTTOM;
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                //  dialog.getWindow().setLayout(1000, 500);
                dialog.show();

                dialog.setCanceledOnTouchOutside(true);

                // Set dialog title
                // set values for custom dialog components - text, image and button
                dialog.show();*/

            }
        });
        camera = (ImageView)findViewById(R.id.camera);

        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";

        File newdir = new File(dir);

        newdir.mkdirs();

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            count++;
            String file = dir+count+".jpg";
            File newfile = new File(file);
            try {
                newfile.createNewFile();
            }
            catch (IOException e)
            {
            }
            Uri outputFileUri = Uri.fromFile(newfile);

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }

        });
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.rightchat);
        listView.setAdapter(chatArrayAdapter);
        emojiconEditText = (EditText)findViewById(R.id.emojicon_edit_text);
        emojiButton = (ImageView)findViewById(R.id.emoji_btn);

        arrowmessenger = (ImageView)findViewById(R.id.arrowmessenger);
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(ChatActivirtyMesssage.this,MainActivity.class));
            }
        });

      //  chatText = (EditText)findViewById(R.id.msg);
        emojiconEditText.setOnKeyListener(new View.OnKeyListener() {
                                      public boolean onKey(View v, int keyCode, KeyEvent event) {
                                          if
                                                  (
                                                  (event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                                              return sendChatMessage();
                                          }
                                          return false;
                                      }
                                  }
        )
        ;


        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);
        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
                                                     @Override
                                                     public void onChanged() {
                                                         super.onChanged();
                                                         listView.setSelection(chatArrayAdapter.getCount() - 1);
                                                     }
                                                 }
        )
        ;
    }
    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }
    private boolean sendChatMessage() {
        chatArrayAdapter.add(new ChatMessage(side, emojiconEditText.getText().toString()));
        emojiconEditText.setText("");
        side = !side;
        return  true;
    }

   // MagicalTakePhoto magicalTakePhoto =  new MagicalTakePhoto(this);

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        magicalTakePhoto.resultPhoto(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
//            Log.d("CameraDemo", "Pic saved");
        }
    }


    public void eventDialog()  {

        TextView tvShare,tvView,tvCreateEvent;
        LayoutInflater inflater = LayoutInflater.from(context);

        final Dialog mDialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);

        mDialog.setCanceledOnTouchOutside(true);

        mDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        mDialog.getWindow().setGravity(Gravity.BOTTOM|Gravity.RIGHT);

        WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
        lp.dimAmount = 0.75f;
        mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow();
        mDialog.getWindow().setAttributes(lp);
        View dialoglayout = inflater.inflate(R.layout.attachfile, null);

        mDialog.setContentView(dialoglayout);

        mDialog.show();

    }

}

