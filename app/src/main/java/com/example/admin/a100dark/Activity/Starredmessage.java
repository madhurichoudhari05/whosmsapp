package com.example.admin.a100dark.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.R;

public class Starredmessage extends AppCompatActivity {
    ImageView back;
    TextView toolbarTitle;

    LinearLayout arrowmessenger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_starredmessage);
        back= (ImageView) findViewById(R.id.toolbar_back);
        toolbarTitle= (TextView) findViewById(R.id.toolbar_title);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbarTitle.setText("Starred messages");

/*
        arrowmessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Starredmessage.this,MainActivity.class));
            }
        });
        arrowmessenger = (LinearLayout)findViewById(R.id.arrowmessenger);*/
    }
}
