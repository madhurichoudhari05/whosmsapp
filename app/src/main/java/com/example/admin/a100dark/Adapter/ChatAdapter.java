package com.example.admin.a100dark.Adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Activity.MainActivity;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.db.entities.ChatMessageDto;
import com.example.admin.a100dark.chat.app.db.entities.UserStatusDto;
import com.example.admin.a100dark.chat.app.retofit.ApiInterface;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.dependency.handler.ChstRetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 10/31/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Dialog mDialog;
    ImageView pro_pic;
    TextView userName;
    boolean isImageFitToScreen;
    private String user_id = "";


    //List<AllUserListDetailsModel> arrayList = new ArrayList<>();
    List<UserStatusDto> arrayList = new ArrayList<>();
    Context context;
    private ApiInterface mInterface;
    private ProgressDialog progressDialog;
    private String selctedOtpion = "";
    private MainActivity homeActivity;
    private FcmMessengerViewModel fcmVM;


    public ChatAdapter(List<UserStatusDto> arrayList, FragmentActivity activity, MainActivity mainActivity,FcmMessengerViewModel fcmVM) {
        this.arrayList = arrayList;
        this.context = activity;
        mInterface = ChstRetrofitHandler.getInstance().getApi();
        homeActivity = mainActivity;
        this.fcmVM=fcmVM;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_item, parent, false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserStatusDto modelDetail = arrayList.get(position);
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        try {
            user_id = CommonUtils.getPreferencesString(context, AppConstants.USER_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!TextUtils.isEmpty(modelDetail.getToName()) && modelDetail.getToName()!= null) {
            holder.userName.setText(CommonUtils.NameCaps(modelDetail.getToName()));

        } else {
            String name="No name";
            if(modelDetail.getToName()!=null && !modelDetail.getToName().trim().equals("")){
                name=modelDetail.getToName();
            }
           // holder.userName.setText(name+ (modelDetail.getUserMobile()));
            holder.userName.setTextColor(context.getResources().getColor(R.color.redcolor));
        }

        if (!TextUtils.isEmpty(modelDetail.getLatestMsg()) && modelDetail.getLatestMsg() != null) {
            holder.status.setText(modelDetail.getLatestMsg());
        } else {
            holder.status.setText("No message");
        }



        if (!TextUtils.isEmpty(modelDetail.getToImage()) && modelDetail.getToImage() != null) {
            Picasso.with(context).load(modelDetail.getToImage()).into(holder.profileImage);
        }


        fcmVM.getLatestMsgObs(modelDetail.getToId()).observe(homeActivity, new Observer<ChatMessageDto>() {
            @Override
            public void onChanged(@Nullable ChatMessageDto chatMessageDto) {
                if(chatMessageDto!=null){
                    holder.status.setText(chatMessageDto.getMsg()+"");
                    holder.timing.setVisibility(View.VISIBLE);
                    holder.timing.setText(CommonUtils.getTimeFormatMilis(chatMessageDto.getDateTime()));
                }else {
                    holder.timing.setVisibility(View.GONE);
                }


            }
        });

        fcmVM.getCountObs(modelDetail.getToId(),false).observe(homeActivity, new Observer<Long>() {
            @Override
            public void onChanged(@Nullable Long aLong) {
                Log.e("count",aLong+"");
                if(aLong==null ||aLong==0){
                    holder.count.setVisibility(View.GONE);
                }else {
                    holder.count.setVisibility(View.VISIBLE);
                    holder.count.setText(aLong+"");
                }
            }
        });

        holder.chat_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                CommonUtils.savePreferencesString(context,AppConstants.RECEIVER__PIC,modelDetail.getToImage());


                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(AppConstants.RECEIVER__USER_NAME2, modelDetail.getToName());
                intent.putExtra(AppConstants.RECEIVER__PIC, modelDetail.getToImage());
               // intent.putExtra(AppConstants.RECEIVER_MOBILE, modelDetail.getUserMobile());
                intent.putExtra(AppConstants.RECEIVER_USER_ID, modelDetail.getToId());
                context.startActivity(intent);
            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                mDialog.setCanceledOnTouchOutside(true);
                mDialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mDialog.getWindow().setGravity(Gravity.CENTER);
                WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
                lp.dimAmount = 0.85f;
                mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow();
                mDialog.getWindow().setAttributes(lp);
                View dialoglayout = inflater.inflate(R.layout.popup_profile_pic, null);
                mDialog.setContentView(dialoglayout);
                pro_pic = (ImageView) mDialog.findViewById(R.id.popup_pro_pic);
                userName = (TextView) mDialog.findViewById(R.id.userName);

                if (!TextUtils.isEmpty(modelDetail.getToImage()) && modelDetail.getToImage() != null) {

                    Picasso.with(context).load(modelDetail.getToImage()).into(pro_pic);
                }
                if (!TextUtils.isEmpty(modelDetail.getToImage()) && modelDetail.getToImage() != null) {
                    userName.setText(CommonUtils.NameCaps(modelDetail.getToName()));

                }



                // pro_pic.setImageResource(R.id.);
                mDialog.show();
                pro_pic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                     /*   Intent intent = new Intent(context, PopUpUserInfo.class);
                        intent.putExtra("USERNAME", modelDetail.getToName());
                        intent.putExtra("PROFILEPIC", modelDetail.getToImage());
                        mDialog.dismiss();
                        context.startActivity(intent);*/
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView userName, status, timing,count;
        LinearLayout linearLayout;
        RelativeLayout chat_layout;
        CircleImageView profileImage;


        public ViewHolder(View itemView) {

            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            chat_layout = (RelativeLayout) itemView.findViewById(R.id.chat_layout);
            userName = (TextView) itemView.findViewById(R.id.chat_user_name);
            status = (TextView) itemView.findViewById(R.id.chat_status);
            timing = (TextView) itemView.findViewById(R.id.time);
            profileImage = (CircleImageView) itemView.findViewById(R.id.chat_profile_image);
            count= (TextView) itemView.findViewById(R.id.count);
        }

    }






   /* private void requestForChat1(String fcmToken) {
        FcmMessage msg = new FcmMessage(
                String.valueOf(user_id),
                CommonUtils.getPreferences(context, AppConstants.USER_NAME),
                AllUserListDetailsModel.getId(),
                System.currentTimeMillis() + "",
                FirebaseInstanceId.getInstance().getToken(),
                IConstants.IFcm.FCM_NEW_REQUEST,
                "madhuri"

        );
    }*/


}
