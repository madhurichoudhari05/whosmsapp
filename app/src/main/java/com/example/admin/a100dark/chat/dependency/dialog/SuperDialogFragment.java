package com.example.admin.a100dark.chat.dependency.dialog;


import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.example.admin.a100dark.chat.dependency.activity.SuperActivity;
import com.example.admin.a100dark.chat.interfaces.IConstants;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public abstract class SuperDialogFragment extends DialogFragment implements IConstants {

    protected View mView;
    protected Context mContext;
    protected Activity mActivity;
    protected ProgressDialogFragment mProgress;
    protected String TAG="";

    public View inflateLayout(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, group,bool);
    }

    public ViewDataBinding inflateLayoutBinging(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return DataBindingUtil.inflate(inflater, layout, group, bool);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
        mActivity=getActivity();
        TAG= this.getClass().getSimpleName();
    }

    public void showToast(String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public void showToastTesting(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    /****************
     * Snack bar
     * ***********/
    public void showSnackBar(String msg) {
        ((SuperActivity)mActivity).showSnackBar(msg);
    }

    protected void showToastL(String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    public void printLog(String msg){
        Log.e(TAG, msg);
    }

    /*************
     * show mProgress
     * ******************/
    public void showProgress(String msg) {
        if (mProgress == null) {
            mProgress= (ProgressDialogFragment) getChildFragmentManager().findFragmentByTag(TAG);
            if(mProgress==null){
                mProgress = new ProgressDialogFragment().newInstance(msg);
            }
        }

        if (!mProgress.isVisible()) {
            try {
                mProgress.show(getChildFragmentManager(), TAG);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    /*************
     * dismiss mProgress
     * ******************/
    public void dismissProgress() {
        if (mProgress == null) {
            mProgress= (ProgressDialogFragment) getChildFragmentManager().findFragmentByTag(TAG);
        }
        if (mProgress != null) {
            try{
                getChildFragmentManager().beginTransaction().remove(mProgress).commitAllowingStateLoss();
            }catch (Exception e){}
//            mProgress.dismissAllowingStateLoss();
        }
    }


    protected boolean isNetworkAvailable(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnectedOrConnecting()){
            return true;
        }
        showToast(INetwork.NETWORK_ERROR);
        return false;
    }

}
