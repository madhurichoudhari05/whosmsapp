package com.example.admin.a100dark.chat.dependency.frag;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.retofit.model.PermissionDto;
import com.example.admin.a100dark.chat.dependency.dialog.ProgressDialogFragment;
import com.example.admin.a100dark.chat.dependency.handler.ExecutorHandler;
import com.example.admin.a100dark.chat.interfaces.IConstants;
import com.example.admin.a100dark.chat.interfaces.lemda.Fun;
import com.example.admin.a100dark.chat.interfaces.lemda.FunNoParam;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public abstract class SuperFragment extends Fragment implements IConstants {

    protected AppDatabase mAppDb;
    protected ExecutorService mExecutor;

    protected Context mContext;
    protected Activity mActivity;
    protected ProgressDialogFragment mProgress;
    protected String TAG="";
    protected FunNoParam mPogressStartLisner;
    protected FunNoParam mPogressStopListener;
    protected List<PermissionDto> mPermission=new ArrayList<>();
    protected Fun mListener;


    public View inflateLayout(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(layout, group,bool);
    }

    public ViewDataBinding inflateLayoutBinging(int layout, ViewGroup group, boolean bool){
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        return DataBindingUtil.inflate(inflater, layout, group, bool);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
        mActivity=getActivity();

        TAG= this.getClass().getSimpleName();
        mAppDb= MyApplication.getDb();
        mExecutor= ExecutorHandler.getInstance().getExecutor();
    }

    public void setmProgressListener(FunNoParam mPogressStartLisner,FunNoParam mPogressStopLisner) {
        this.mPogressStartLisner = mPogressStartLisner;
        this.mPogressStopListener=mPogressStopLisner;
    }

    /**********************
     *      Back To Home
     * *****************/

    public void onBackPressed() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    /*************
     * show toast
     * ******************/
    public void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }


    /****************
     * Snack bar
     * ***********/
    public void showSnackBar(String msg) {
//        ((SuperActivity)mActivity).showSnackBar(msg);
        showToast(msg);
    }

    /*************
     * show toast Long
     * ******************/
    protected void showToastL(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    public void showToastTesting(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    /*************
     * print log
     * ******************/
    public void printLog(String msg) {
        Log.e(TAG, msg);
    }


    /*************
     * show mProgress
     * ******************/
    public void showProgress(String msg) {
        if(mPogressStartLisner!=null){
            mPogressStartLisner.done();
            return;
        }
        if (mProgress == null) {
            mProgress= (ProgressDialogFragment) getChildFragmentManager().findFragmentByTag(TAG);
            if(mProgress==null){
                mProgress = new ProgressDialogFragment().newInstance(msg);
            }
        }

        if (!mProgress.isVisible()) {
            try {
                if(mProgress.isAdded()){
                    getChildFragmentManager().beginTransaction().remove(mProgress).commitAllowingStateLoss();
                }
                mProgress.show(getChildFragmentManager(), TAG);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    /*************
     * dismiss mProgress
     * ******************/
    public void dismissProgress() {
        if(mPogressStopListener!=null){
            mPogressStopListener.done();
            return;
        }
        if (mProgress == null) {
            mProgress= (ProgressDialogFragment) getChildFragmentManager().findFragmentByTag(TAG);
        }
        if (mProgress != null) {
            try{
                getChildFragmentManager().beginTransaction().remove(mProgress).commitAllowingStateLoss();
            }catch (Exception e){}
//            mProgress.dismissAllowingStateLoss();
        }
    }

    /****************
     * permissions
     * ****************/
    protected boolean isPerGiven(String per) {
        if (ContextCompat.checkSelfPermission(mActivity, per)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    protected boolean isPermissionRequired(String[] requestPer, int reqId) {
        List<String> requiredPerm = new ArrayList<>();
        for (String per : requestPer) {
            if (ContextCompat.checkSelfPermission(mActivity, per) != PackageManager.PERMISSION_GRANTED) {
                requiredPerm.add(per);
            }
        }

        if (requiredPerm.size() > 0) {
            String[] perArray = new String[requiredPerm.size()];
            perArray = requiredPerm.toArray(perArray);
            requestPermissions( perArray, reqId);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        boolean isCompGranted=true;
        for(PermissionDto perm:mPermission){
            if (!isPerGiven(perm.getPermission())) {
                if(perm.isCompalsary()){
                    isCompGranted=false;
//                    checkPermission(requestCode);
                    break;
                }else {
                    perm.setGranted(true);
                }
            }
        }
        if(isCompGranted){
            List<String> requiredPerm = new ArrayList<>();
            String[] per = null;
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    requiredPerm.add(permissions[i]);
                }
                per = new String[requiredPerm.size()];
            }
            userResponse(requiredPerm.toArray(per), requestCode);
        }else {
            showSnackBar("Above permissions are compulsory");
        }

    }

    protected void checkPermission(int reqId){

        List<String> permissions = new ArrayList<>();

        for(PermissionDto perm:mPermission){
            if (!isPerGiven(perm.getPermission())) {
                permissions.add(perm.getPermission());
            }
        }


        if (permissions.size() >= 1) {
            String[] arr = new String[permissions.size()];
            permissions.toArray(arr);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isPermissionRequired(arr, reqId);
            } else {
                userResponse(null,reqId);
            }
        } else {
            userResponse(null,reqId);
        }
    }

    protected void userResponse(String[] permission, int reqId) {


    }

    protected boolean isNetworkAvailable(Context context){
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(netInfo != null && netInfo.isConnected()){
            return true;
        }
        showToast(INetwork.NETWORK_ERROR);
        return false;
    }


    public void setListener(Fun listener){
        this.mListener=listener;
    }
}
