package com.example.admin.a100dark.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.a100dark.Activity.PopUpUserInfo;
import com.example.admin.a100dark.Model.AllUserListDetailsModel;
import com.example.admin.a100dark.R;
import com.example.admin.a100dark.chat.app.MyApplication;
import com.example.admin.a100dark.chat.app.db.database.AppDatabase;
import com.example.admin.a100dark.chat.app.retofit.ApiInterface;
import com.example.admin.a100dark.chat.app.ui.ChatActivity;
import com.example.admin.a100dark.chat.app.ui.FcmMessengerViewModel;
import com.example.admin.a100dark.chat.dependency.handler.ChstRetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Admin on 11/3/2017.
 */


public class MsgForwordAdapter extends  RecyclerView.Adapter<MsgForwordAdapter.ViewHolder> implements Filterable {
    Dialog mDialog;
    ImageView pro_pic;
    TextView userName;
    boolean isImageFitToScreen;
    private int user_id = 0;
    List<AllUserListDetailsModel> arrayList = new ArrayList<>();
    Context context;
    private ApiInterface mInterface;
    private FcmMessengerViewModel fcmVM;
    private ValueFilter valueFilter;
    private List<AllUserListDetailsModel> mStringFilterList;
    private List<AllUserListDetailsModel> bothList;
    private AppDatabase mAppDb;
    private OnItemClicked onClick;
    public interface OnItemClicked {
        void onItemClick(int position);
    }
    private final ArrayList<Integer> seleccionados = new ArrayList<>();

    public MsgForwordAdapter(List<AllUserListDetailsModel> arrayList, Context activity , List<AllUserListDetailsModel> bothList) {
        this.arrayList = arrayList;
        this.bothList = bothList;
        this.context = activity;
        mStringFilterList=arrayList;
        mInterface = ChstRetrofitHandler.getInstance().getApi();
        this.fcmVM=fcmVM;
        mAppDb = MyApplication.getDb();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.searchchat_item, parent, false);
        ViewHolder recyclerViewHolder = new ViewHolder(view);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AllUserListDetailsModel modelDetail = arrayList.get(position);

       // Toast.makeText(context, "bothlist::"+bothList.size(), Toast.LENGTH_SHORT).show();

        user_id = Integer.valueOf(CommonUtils.getPreferencesString(context, AppConstants.USER_ID));
        if (!TextUtils.isEmpty(modelDetail.getUserName()) && modelDetail.getUserName() != null) {
            holder.userName.setText(CommonUtils.NameCaps(modelDetail.getUserName()));

        } else {
            String name="No name";
            if(modelDetail.getUserName()!=null && !modelDetail.getUserName().trim().equals("")){
                name=modelDetail.getUserName();
            }
            holder.userName.setText(name+ (modelDetail.getUserMobile()));
            holder.userName.setTextColor(context.getResources().getColor(R.color.redcolor));
        }

        if (!TextUtils.isEmpty(modelDetail.getUserMobile()) && modelDetail.getUserMobile() != null) {
            holder.status.setText(modelDetail.getUserMobile());
        } else {
            holder.status.setText("No Number");
        }
        if (!TextUtils.isEmpty(modelDetail.getUserPic()) && modelDetail.getUserPic() != null) {
            Picasso.with(context).load(modelDetail.getUserPic()).into(holder.profileImage);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClick.onItemClick(position);

            }
        });
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName, status, timing,count;
        LinearLayout linearLayout;
        CircleImageView profileImage;
        public ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            userName = (TextView) itemView.findViewById(R.id.chat_user_name);
            status = (TextView) itemView.findViewById(R.id.chat_status);
            timing = (TextView) itemView.findViewById(R.id.time);
            profileImage = (CircleImageView) itemView.findViewById(R.id.chat_profile_image);
            count= (TextView) itemView.findViewById(R.id.count);
        }
    }

    @Override
    public Filter getFilter() {
        if(valueFilter==null) {

            valueFilter=new ValueFilter();
        }

        return valueFilter;
    }
    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<AllUserListDetailsModel> filterList=new ArrayList<AllUserListDetailsModel>();
                for(int i=0;i<bothList.size();i++){
                    if((bothList.get(i).getUserName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        AllUserListDetailsModel contacts = new AllUserListDetailsModel();
                        contacts.setUserName(bothList.get(i).getUserName());
                       contacts.setUserMobile(bothList.get(i).getUserMobile());
                        filterList.add(contacts);
                    }
                }
                results.count=filterList.size();
                results.values=filterList;
            }else{
                results.count=bothList.size();
                results.values=bothList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            arrayList=(ArrayList<AllUserListDetailsModel>) results.values;
            notifyDataSetChanged();
        }
    }

    public void setOnClick(OnItemClicked onClick)
    {
        this.onClick=onClick;
    }
}

