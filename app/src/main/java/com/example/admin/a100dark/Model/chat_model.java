package com.example.admin.a100dark.Model;

/**
 * Created by Admin on 11/3/2017.
 */

public class chat_model {

    private int chat_image;

    public chat_model(String s, String s1, String s2, int images) {

        this.chat_name=s;
        this.chat_stauts= s1;
        this.chat_time= s2;
        this.chat_image= images;

    }

    public int getChat_image() {
        return chat_image;
    }

    public void setChat_image(int chat_image) {
        this.chat_image = chat_image;
    }

    public String getChat_name() {
        return chat_name;
    }

    public void setChat_name(String chat_name) {
        this.chat_name = chat_name;
    }

    public String getChat_stauts() {
        return chat_stauts;
    }

    public void setChat_stauts(String chat_stauts) {
        this.chat_stauts = chat_stauts;
    }

    public String getChat_time() {
        return chat_time;
    }

    public void setChat_time(String chat_time) {
        this.chat_time = chat_time;
    }

    private  String chat_name;
    private  String chat_stauts;
    private  String chat_time;


}
