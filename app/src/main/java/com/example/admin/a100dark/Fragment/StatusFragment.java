package com.example.admin.a100dark.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.a100dark.Adapter.ChatAdapter;
import com.example.admin.a100dark.Adapter.StatusAdapter;
import com.example.admin.a100dark.Model.ChatModel;
import com.example.admin.a100dark.Model.StatusModel;
import com.example.admin.a100dark.R;

import java.util.ArrayList;

/**
 * Created by Admin on 11/1/2017.
 */

public class StatusFragment extends Fragment {

    ArrayList<StatusModel>arrayList=new ArrayList<>();
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vv = inflater.inflate(R.layout.status_screen, null);
        recyclerView=(RecyclerView)vv.findViewById(R.id.status_recyclerview);
        init();
        StatusAdapter statusAdapter = new StatusAdapter(arrayList,getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(statusAdapter);

        return vv;
    }
    public void init()
    {
        for (int i = 0 ; i<10;i++) {
            StatusModel statusModel = new StatusModel(R.drawable.images,"John","Yesterday 3:00 p.m.");
            arrayList.add(statusModel);
        }
    }
}

