package com.example.admin.a100dark.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.a100dark.R;
import com.example.admin.a100dark.retrofit.FileUploadInterface;
import com.example.admin.a100dark.retrofit.RetrofitHandler;
import com.example.admin.a100dark.utils.AppConstants;
import com.example.admin.a100dark.utils.CommonUtils;
import com.example.admin.a100dark.utils.CropActivity;
import com.example.admin.a100dark.utils.GlobalAccess;
import com.example.admin.a100dark.utils.MadhuFileUploader;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileActivty extends AppCompatActivity implements View.OnClickListener {
    ImageView back,iv_edit_pro_pic;
    TextView toolbarTitle;
    Context mContext;
    public static final String EXCEPTION_MSG = "We are facing some issues. Please check back later."/*"Please try again later."*/;
    public static final String EXCEPTION_MSG_TIMEOUT = "Poor network connection"/*"Please try again later."*/;
    private final int CAMERA_REQUEST_CODE = 1;
    private final int GALLERY_REQUEST_CODE = 2;
    private final int CROP_REQUEST_CODE = 4;
    private final int REQUEST_CAMERA = 111;
    private final int REQUEST_GALLERY = 222;
    private String mCurrentPhotoPath = "";
    private ArrayList<String> imagelist;
    private boolean isFileImg;
    private TextView userNameText, editUser, currentText;
    CircleImageView profile_image;
    String user_id="";
    TextView tv_mobile_no;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_activty);
        mContext = ProfileActivty.this;
        imagelist = new ArrayList<>();
        back = (ImageView) findViewById(R.id.toolbar_back);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        userNameText = (TextView) findViewById(R.id.userNameText);
        editUser = (TextView) findViewById(R.id.editUser);
        currentText = (TextView) findViewById(R.id.tv_current_status);
        profile_image=findViewById(R.id.profile_image);
        iv_edit_pro_pic=findViewById(R.id.iv_edit_pro_pic);
        tv_mobile_no=findViewById(R.id.tv_mobile_no);

        iv_edit_pro_pic.setOnClickListener(this);

        currentText.setText(CommonUtils.getPreferencesString(mContext,AppConstants.USER_PROFILE_STATUS));

        tv_mobile_no.setText(CommonUtils.getPreferencesString(mContext, AppConstants.Registerd_MOBILE_NO));

        String profilepic = co.intentservice.chatui.activity.CommonUtils.getPreferencesString(mContext, wehyphens.com.satyaconnect.utils.AppConstants.SENDER_PROFILE_PIC);
        Picasso.with(mContext)
                .load("file://" + profilepic)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(co.intentservice.chatui.R.drawable.placeholder_user)
                .error(co.intentservice.chatui.R.drawable.placeholder_user)
                .into(profile_image);


        editUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // startActivity(new Intent(ProfileActivty.this, EditUsernameActivity.class));

                Intent i = new Intent(ProfileActivty.this, EditUsernameActivity.class);
                startActivityForResult(i, 1);
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbarTitle.setText("Profile");


        currentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* Intent i=new Intent(mContext,ChangeStatusActivity.class);
                i.putExtra("userName",userNameText.getText().toString());
                startActivity(i);*/

                startActivity(new Intent(ProfileActivty.this, ChangeStatusActivity.class));
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        userNameText.setText(CommonUtils.getPreferencesString(mContext, AppConstants.USER_NAME));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_edit_pro_pic:

                openCameraGalleryDialog();
                break;
        }
    }



    public void openCameraGalleryDialog() {
        final CharSequence[] items = {"Open Camera", "Open Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(
                this);
        builder.setTitle("Select : ");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Open Camera")) {
                    cameraPermissionMethod();
                } else if (items[item].equals("Open Gallery")) {
                    gallleryPermissionMethod();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }private void cameraPermissionMethod() {
        if (requestPermission(CAMERA_REQUEST_CODE, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
            activityForCamera();
        }}private void gallleryPermissionMethod() {
        if (requestPermission(GALLERY_REQUEST_CODE, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE})) {
            activityForGallery();
        }
    }
    //  }


    private void activityForCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getBaseContext(), "Sorry, There is some problem!", Toast.LENGTH_SHORT).show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri apkURI = FileProvider.getUriForFile(mContext, getApplicationContext()
                        .getPackageName() + ".provider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                } else {
                    List<ResolveInfo> resInfoList = getPackageManager()
                            .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, apkURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }


    private void activityForGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select : "),
                REQUEST_GALLERY);
    }


    private void cameraOperation(String imageUrl) {
        if (imageUrl != null && !imageUrl.isEmpty()) {
            GlobalAccess.setImagePath(imageUrl);
            Intent i = new Intent(mContext, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }

    private void galleryOperation(Intent data) {
        Uri selectedImage = data.getData();
        String picturePath = null;
        if (selectedImage == null) {
            Toast.makeText(mContext, "Image selection Failed!", Toast.LENGTH_SHORT).show();
        } else {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                if (picturePath == null)
                    picturePath = selectedImage.getPath();
                cursor.close();
            } else {
                picturePath = selectedImage.getPath();
            }
        }
        if (picturePath != null) {
            GlobalAccess.setImagePath(picturePath);
            Intent i = new Intent(mContext, CropActivity.class);
            startActivityForResult(i, CROP_REQUEST_CODE);
        }
    }


    protected void onPermissionResult(int requestCode, boolean isPermissionGranted) {


        if (isPermissionGranted) {
            if (requestCode == CAMERA_REQUEST_CODE) {
                activityForCamera();
            } else if (requestCode == GALLERY_REQUEST_CODE) {
                activityForGallery();
            }
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    if(resultCode == RESULT_OK) {
                       // String strEditText = data.getStringExtra("editTextValue");
                        userNameText.setText(data.getStringExtra("editTextValue"));
                    }

                    break;


                case REQUEST_CAMERA:
                    String imageFile = mCurrentPhotoPath;
                    cameraOperation(imageFile);
                    break;
                case REQUEST_GALLERY:
                    galleryOperation(data);
                    break;
                case CROP_REQUEST_CODE:
                    if (data.getExtras().containsKey("path")) {
                        imagelist.clear();
                        mCurrentPhotoPath = data.getExtras().getString("path");
                        imagelist.add(mCurrentPhotoPath);
                        Log.e("mCurrentPhotoPath", mCurrentPhotoPath);
                        Log.e("numerofimages", imagelist.toString());
                        Log.e("sizenumerofimages", String.valueOf(imagelist.size()));
                        isFileImg = true;
                        //  setProfileData();



                        Picasso.with(mContext)
                                .load("file://" + mCurrentPhotoPath)
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.ic_launcher)
                                .into(profile_image);
                        callProfileData(mCurrentPhotoPath);


                    } else {
                        mCurrentPhotoPath = null;
                    }
                    break;
            }
        }
    }
    public File createImageFile() throws IOException {
        mCurrentPhotoPath = "";
        String imageFileName = "JPEG_temp_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getCacheDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                appFile      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(this, permission, requestCode);

        return isAlreadyGranted;

    }
    protected boolean checkPermission(String[] permission){

        boolean isPermission = true;

        for(String s: permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(this,s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    private void callProfileData(String mCurrentPhotoPath) {
        CommonUtils.showProgress(mContext);
        FileUploadInterface service = RetrofitHandler.getInstance().getApi();
        Call<ResponseBody> call=null;
        if (imagelist != null && imagelist.size() > 0) {
            user_id=CommonUtils.getPreferencesString(mContext, AppConstants.USER_ID);
            Map<String, String> params = new HashMap<String, String>();
            params.put("user_id", user_id);
            params.put("status","busy");
            params.put("name", userNameText.getText().toString());
            MadhuFileUploader uploader = new MadhuFileUploader();
            uploader.uploadFile(params, imagelist, "fileToUpload", mContext);
            Log.e("retoImageupload", params.toString());
            for (String s : imagelist) {
                Log.e("imagelstttttt", s);
            }
            call = service.uploadFileProfileile(uploader.getPartMap(), uploader.getPartBodyArr());
        }
        else {

           /* imagelist.clear();
            RequestBody user_id1 = RequestBody.create(MediaType.parse("text/plain"),user_id);
            RequestBody emoji_staus = RequestBody.create(MediaType.parse("text/plain"),"busy");
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), etName.getText().toString());
            call = service.uploadFileWithPartMap(user_id1, emoji_staus,name);
 */       }
             call.enqueue(new Callback<ResponseBody>() {
            public boolean status;
            @Override
            public void onResponse(Call<ResponseBody> call, final retrofit2.Response<ResponseBody> response) {
                CommonUtils.dismissProgress();
                String str = "";

                try {
                    str = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.e("retoImageupload", str);

                try {
                    JSONArray jsonArray = new JSONArray(str);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        boolean profilepic = jsonObject.getBoolean("status");
                        String messageimg = jsonObject.getString("message");
                       // CommonUtils.savePreferencesString(mContext,AppConstants.USER_NAME,etName.getText().toString());
                        // CommonUtils.savePreferencesBoolean(context,AppConstants.FIRST_TIME_LOGIN,true);
                        if (profilepic) {
                            //  CommonUtils.snackBar(messageimg,cameraopen);
                            if (messageimg != null && !messageimg.isEmpty()) {
                                CommonUtils.savePreferencesString(mContext,AppConstants.SENDER_PROFILE_PIC,mCurrentPhotoPath);
                              //  callcongratulation();
                            }
                            else {
                                Log.e("userpic", "null");}
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                CommonUtils.dismissProgress();
                String msg = "";
                if (t instanceof SocketTimeoutException) {
                    msg = EXCEPTION_MSG_TIMEOUT;
                } else if (t instanceof IOException) {
                    msg = EXCEPTION_MSG_TIMEOUT;

                } else if (t instanceof HttpException) {
                    msg = EXCEPTION_MSG;
                } else {
                    msg = EXCEPTION_MSG;
                }
                Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
                Log.e("server exception", t.getMessage() + "");
            }
        });


    }


}
