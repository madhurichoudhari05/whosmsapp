package co.intentservice.chatui.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import co.intentservice.chatui.R;

public class PDFActivity extends AppCompatActivity {
    private String docPath = "";

    private ProgressDialog progressDialog;
    private WebView webviewFaq;
    //    private String url = "http://wehyphens.com";
    // private String url = "https://staging.v-coins.global/index.php/vcs-wallet/english/wallet_welcome_en";
    private Context mcontext;
    private Dialog dialog;
    ImageView ivProgress;
    private String url = "https://wallet.v-coins.global";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mcontext = PDFActivity.this;


        Intent intent = getIntent();
        if (getIntent() != null) {
            if (getIntent().getStringExtra("DocPath") != null && getIntent().getStringExtra("DocPath") != null)
                docPath = intent.getStringExtra("DocPath");
        }

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(docPath));
        startActivity(browserIntent);

/*
        WebView webView = new WebView(PDFActivity.this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setWebViewClient(new Callback());

        String pdfURL = "http://dl.dropboxusercontent.com/u/37098169/Course%20Brochures/AND101.pdf";
        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + docPath);
        setContentView(webView);*/

    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

}

