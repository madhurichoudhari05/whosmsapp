package co.intentservice.chatui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import co.intentservice.chatui.R;

public class VideoActivity extends AppCompatActivity {


    private Context mcontext;
    private String videoPath="";
    VideoView videoView;
    MediaController mediaControls;
    MediaController mediaController;
    private ImageView play;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        videoView = findViewById(R.id.VideoView);
        play=findViewById(R.id.play);
        mcontext = VideoActivity.this;
        Intent intent = getIntent();
        if (getIntent() != null) {
            if (getIntent().getStringExtra("VideoPath") != null && getIntent().getStringExtra("VideoPath") != null)
                videoPath = intent.getStringExtra("VideoPath");
        }
        mediaController=new MediaController(mcontext);
        mediaController.setAnchorView(videoView);
        DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.RelativeLayout.LayoutParams params = (android.widget.RelativeLayout.LayoutParams) videoView.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);
        videoView.setBackgroundResource(0);
        videoView.setVideoPath(videoPath);
        //   videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
        videoView.setVideoPath("http://abhiandroid-8fb4.kxcdn.com/ui/wp-content/uploads/2016/04/videoviewtestingvideo.mp4");
        // videoView.setVideoPath(videoPath);
        videoView.start();
        play.setVisibility(View.GONE);



        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {

            public void onPrepared(MediaPlayer mediaPlayer)
            {
                // if we have a position on savedInstanceState, the video
                // playback should start from here
                videoView.seekTo(position);


                if (position == 0)
                {
                    videoView.start();
                } else
                {
                    // if we come from a resumed activity, video playback will
                    // be paused
                    videoView.pause();
                }
            }
        });

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play.setVisibility(View.VISIBLE);
                videoView.stopPlayback();
            }
        });

    }

}






