package co.intentservice.chatui;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import co.intentservice.chatui.activity.ChatProfileActivity;
import co.intentservice.chatui.activity.CommonUtils;
import co.intentservice.chatui.activity.UIChatProfileActivity;
import co.intentservice.chatui.activity.VideoActivity;
import co.intentservice.chatui.adapters.GenericMultiViewAdapter;
import co.intentservice.chatui.databinding.AdminAddDeleteBinding;
import co.intentservice.chatui.databinding.ChatAudioItemRcvBinding;
import co.intentservice.chatui.databinding.ChatAudioItemSentBinding;
import co.intentservice.chatui.databinding.ChatDocItemRcvBinding;
import co.intentservice.chatui.databinding.ChatDocItemSentBinding;
import co.intentservice.chatui.databinding.ChatImageItemRcvBinding;
import co.intentservice.chatui.databinding.ChatImageItemSentBinding;
import co.intentservice.chatui.databinding.ChatTextItemRcvBinding;
import co.intentservice.chatui.databinding.ChatTextItemSentBinding;
import co.intentservice.chatui.databinding.ChatVideoItemRcvBinding;
import co.intentservice.chatui.databinding.ChatVideoItemSentBinding;
import co.intentservice.chatui.databinding.ChatViewBinding;

import co.intentservice.chatui.fab.FloatingActionsMenu;
import co.intentservice.chatui.lemda.Fun1ParamRet;
import co.intentservice.chatui.models.ChatMessage;
import madhu.MediaPlayerHandler;
import wehyphens.com.satyaconnect.utils.AppConstants;

import static android.content.ContentValues.TAG;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_SETTLING;


public class ChatView extends RelativeLayout {

    private static final int FLAT = 0;
    private static final int ELEVATED = 1;

    private ChatViewBinding binding;
    private CardView inputFrame;
    private RecyclerView chatListView;
    private EditText inputEditText;
    private AppCompatImageView ivCamera, ivAttachment, ivRecordVoice;
    private boolean statusAudiio;
    private boolean statusDelete;

  //  private FloatingActionsMenu actionsMenu;
    private AppCompatImageView actionsMenu;
    private RelativeLayout inputLayout;
    private LinearLayout ll_forword_msg;
    private boolean previousFocusState = false, useEditorAction, isTyping;
    private TextView textView,tvChatDate;

    public ChatViewBinding getBinding() {
        return binding;
    }

    private Runnable typingTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (isTyping) {
                isTyping = false;
                if (typingListener != null) typingListener.userStoppedTyping();
            }
        }
    };
    private TypingListener typingListener;
    private OnSentMessageListener onSentMessageListener;
    private OnAttachmentClickListener attachmentListener;
    private OnEmojiClickListener emojiClickListener;
    private GenericMultiViewAdapter<
            ChatTextItemRcvBinding, ChatTextItemSentBinding, ChatImageItemRcvBinding, ChatImageItemSentBinding,
            ChatVideoItemRcvBinding, ChatVideoItemSentBinding, ChatAudioItemRcvBinding, ChatAudioItemSentBinding,
            ChatDocItemRcvBinding, ChatDocItemSentBinding,AdminAddDeleteBinding,
            ChatMessage> chatViewListAdapter;

    private int inputFrameBackgroundColor, backgroundColor;
    private int inputTextSize, inputTextColor, inputHintColor;
    private int sendButtonBackgroundTint, sendButtonIconTint;

    private float bubbleElevation;

    private int bubbleBackgroundRcv, bubbleBackgroundSend; // Drawables cause cardRadius issues. Better to use background color
    private Drawable sendButtonIcon, buttonDrawable;
    private TypedArray attributes, textAppearanceAttributes;
    private Context context;


    ChatView(Context context) {
        this(context, null);
    }

    public ChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {

        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);

    }


    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.chat_view, this, true);
//        LayoutInflater.from(getContext()).inflate(R.layout.chat_view, this, true);
        this.context = context;
        initializeViews();
        getXMLAttributes(attrs, defStyleAttr);
        setViewAttributes();
        setListAdapter();
        setButtonClickListeners();
        setUserTypingListener();
        setUserStoppedTypingListener();
        setUserClickListener();
    }

    private void initializeViews() {
        chatListView = (RecyclerView) findViewById(R.id.chat_list);
        inputFrame = (CardView) findViewById(R.id.input_frame);
        inputEditText = (EditText) findViewById(R.id.input_edit_text);
        actionsMenu = (AppCompatImageView) findViewById(R.id.sendButton);
        ivAttachment = findViewById(R.id.ivAttach);
        ivRecordVoice = (AppCompatImageView) findViewById(R.id.ivRecordVoice);
        inputLayout = findViewById(R.id.rlInput);
        ll_forword_msg = findViewById(R.id.ll_forword_msg);


        tvChatDate=findViewById(R.id.tvChatDate);
        tvChatDate.animate().translationY(-tvChatDate.getHeight())
                .setInterpolator(new AccelerateInterpolator(1));

        final View rootView = findViewById(R.id.rootView);

        animateDate();

    }

    private boolean isVisible=false;
    private void animateDate(){
        chatListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("drag",newState+"" );
                if(newState==0){
                    //Log.e("drag",newState+"00000" );

                }

                if(newState==SCROLL_STATE_IDLE){
                    isVisible=false;

                    tvChatDate.animate().translationY(-tvChatDate.getHeight())
                            .setInterpolator(new AccelerateInterpolator(1));
                }else {
                    if(isVisible){
                        return;
                    }
                    isVisible=true;
                    tvChatDate.animate().translationY(0).setInterpolator
                            (new DecelerateInterpolator(1)).start();
                }
            }
        });

       /* chatListView.addOnScrollListener(new HidingScrollListener() {

            @Override
            public void onHide()
            {


            }

            @Override
            public void onShow()
            {

            }
        });*/
    }

    private void getXMLAttributes(AttributeSet attrs, int defStyleAttr) {
        attributes = context.obtainStyledAttributes(attrs, R.styleable.ChatView, defStyleAttr, R.style.ChatViewDefault);
        getChatViewBackgroundColor();
        getAttributesForBubbles();
        getAttributesForInputFrame();
        getAttributesForInputText();
        getAttributesForSendButton();
        getUseEditorAction();
        attributes.recycle();
    }

    Fun1ParamRet<Integer, Integer> fun = (pos) -> {
        return chatViewListAdapter.getList().get(pos).getType();
    };

    private void setListAdapter() {

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setStackFromEnd(true);

        chatListView.setLayoutManager(manager);


        chatViewListAdapter = new GenericMultiViewAdapter<
                ChatTextItemRcvBinding, ChatTextItemSentBinding, ChatImageItemRcvBinding, ChatImageItemSentBinding,
                ChatVideoItemRcvBinding, ChatVideoItemSentBinding, ChatAudioItemRcvBinding, ChatAudioItemSentBinding,
                ChatDocItemRcvBinding, ChatDocItemSentBinding,AdminAddDeleteBinding,
                ChatMessage>(
                context,
                R.layout.chat_text_item_rcv,
                R.layout.chat_text_item_sent,
                R.layout.chat_image_item_rcv,
                R.layout.chat_image_item_sent,
                R.layout.chat_video_item_rcv,
                R.layout.chat_video_item_sent,
                R.layout.chat_audio_item_rcv,
                R.layout.chat_audio_item_sent,
                R.layout.chat_doc_item_rcv,
                R.layout.chat_doc_item_sent,
                R.layout.admin_add_delete,
                fun) {
            @Override
            public void commonFun(int position, List<ChatMessage> mList) {
                String date=mList.get(position).getFormattedDate();

                if(!tvChatDate.getText().toString().trim().equalsIgnoreCase(date)){
                    if(!date.contains("1970")){
                        tvChatDate.setText(date);
                    }
                }
//                Log.e("date",mList.get(position).getTimestamp()+"  "+date);
              /*  if(date.equalsIgnoreCase("19 Jan 1970")){
                    tvChatDate.setVisibility(GONE);
                    Log.e("dateWRONG",date );
                }
                else {
                    tvChatDate.setText(date);

                }*/


            }

            @Override
            public void customFun0(ChatTextItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

               if(mList.get(position).getFilePath()!=null&&(!TextUtils.isEmpty(mList.get(position).getFilePath()))&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                   Picasso.with(context).load(mList.get(position).getFilePath()).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);

               }

                Log.e("profilePicReciever","file://"+mList.get(position).getFilePath());

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                if(mList.get(position).getForwordStatus()!=null){
                    if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.llForwordMsg.setVisibility(VISIBLE);
                    }else {
                        binding.llForwordMsg.setVisibility(GONE);
                    }

                }




                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {

                        Toast.makeText(context, "Long Click", Toast.LENGTH_SHORT).show();
                        statusDelete=true;
                        if (mList.get(position).getFiletype()!=null && !mList.get(position).getFiletype().equalsIgnoreCase("1")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            typingListener.userDeleteClick(position, mList.get(position).getMessage(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v -> {
                   // Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();
                });

            }

            @Override
            public void customFun1(ChatTextItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                if(mList.get(position).getTimestamp()>System.currentTimeMillis()) { }

                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                if(profilepic!=null&&(!TextUtils.isEmpty(profilepic)&&!profilepic.equalsIgnoreCase(""))) {
                    Picasso.with(context).load("file://"+profilepic).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);

                }
               /* binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#2bb3de"));
                    Toast.makeText(context, "position -> "+position, Toast.LENGTH_SHORT).show();
                });


*/

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                if(mList.get(position).getForwordStatus()!=null){
                    if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.llForwordMsg.setVisibility(VISIBLE);
                    }else {
                        binding.llForwordMsg.setVisibility(GONE);
                    }}


                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {




                        statusDelete=true;
                        if (mList.get(position).getFilePath()!=null&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            // binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, mList.get(position).getMessage(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                            // binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                            final Handler handler = new Handler();

                        }
                        return false;
                    }
                });

            }

            @Override
            public void customFun2(ChatImageItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                Log.e("imageRece", "imageRece" + mList.get(position).getFilePath());

                if (mList.get(position).getReadStatus() == 1) {
                    binding.tvReciverName.setVisibility(VISIBLE);
                    binding.tvReciverName.setText(CommonUtils.NameCaps(mList.get(position).getUserName()));

                } else {

                    binding.tvReciverName.setVisibility(GONE);
                }


                if (mList.get(position).getFilePath() != null && !TextUtils.isEmpty(mList.get(position).getFilePath())) {
                    Picasso.with(context).load(mList.get(position).getFilePath()).error(R.drawable.ic_camera_circle).fit().centerCrop().into(binding.ivChat);
                }

                if(mList.get(position).getFilePath()!=null&&(!TextUtils.isEmpty(mList.get(position).getFilePath()))&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                    try {
                        Picasso.with(context).load(mList.get(position).getMessage()).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                if(mList.get(position).getForwordStatus()!=null){
                    if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.llForwordMsg.setVisibility(VISIBLE);
                    }else {
                        binding.llForwordMsg.setVisibility(GONE);
                    }}




                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0") && mList.get(position).getFilePath()==null || mList.get(position).getType()==0) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, String.valueOf(mList.get(position).getType()), view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }
                        return false;
                    }
                });


                binding.ivChat.setOnClickListener(v -> {
                        Intent intent = new Intent(new Intent(context, UIChatProfileActivity.class));
                        // intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());
                        intent.putExtra("PATH",mList.get(position).getFilePath());
                        intent.putExtra("NAME",  mList.get(position).getUserName());
                        intent.putExtra("TIME",  mList.get(position).getFormattedTime());
                        intent.putExtra("READSATUS",   mList.get(position).getReadStatus());
                        context.startActivity(intent);
                        CommonUtils.savePreferencesString(context, wehyphens.com.satyaconnect.utils.AppConstants.RECEIVER_ID, mList.get(position).getUserName());

                        Log.e("groupreceiver_id", "groupCurrent_receiver_id" + mList.get(position).getReadStatus());
                        Log.e("groupCurrent_USErName", "groupCurrent_ReceiverName" + mList.get(position).getUserName());
                      //  Log.e("chatId", "chatId" + arrayList.get(position).chatId);

                });
            }

            @Override
            public void customFun3(ChatImageItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));

                String path = "https://firebasestorage.googleapis.com/v0/b/satyaconnectapp.appspot.com/o/SatyaImage%2FSatyaImage?alt=media&token=1045c19c-3172-4b08-9a12-11cb4908dc69";
                Log.e("imagesend", "imagesend::" + mList.get(position).getFilePath());
                Log.e("imagesend", "imagesend::" + mList.get(position).getFilePath());

                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                if(profilepic!=null&&(!TextUtils.isEmpty(profilepic)&&!profilepic.equalsIgnoreCase(""))) {
                    Picasso.with(context).load("file://"+profilepic).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);

                }

                if (mList.get(position).getFilePath() != null && !TextUtils.isEmpty(mList.get(position).getFilePath())) {
                    Picasso.with(context).load(mList.get(position).getFilePath()).error(R.drawable.ic_camera_circle).fit().centerCrop().into(binding.ivChat);

                }


                binding.flBackground.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "position -> "+position, Toast.LENGTH_SHORT).show();

                    }
                });

                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                if(mList.get(position).getForwordStatus()!=null){
                    if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.llForwordMsg.setVisibility(VISIBLE);
                    }else {
                        binding.llForwordMsg.setVisibility(GONE);
                    }}




                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (String.valueOf(mList.get(position).getType()).equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            // binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, mList.get(position).getImageType(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }
                        return false;
                    }
                });


                Log.e("profilePicSender",profilepic);
                binding.ivChat.setOnClickListener(v -> {
                    Intent intent = new Intent(new Intent(context, UIChatProfileActivity.class));
                    // intent.putExtra(AppConstants.GROUP_NODE,arrayList.get(position).getGroupDetail().getNode());
                    intent.putExtra("PATH",mList.get(position).getFilePath());
                    intent.putExtra("NAME",  mList.get(position).getUserName());
                    intent.putExtra("TIME",  mList.get(position).getFormattedTime());
                    intent.putExtra("READSATUS",   mList.get(position).getReadStatus());

                    Log.e("groupCurrent_USErName", "groupCurrent_USErName" + mList.get(position).getUserName());
                    Log.e("groupreceiver_id", "groupCurrent_receiver_id" + mList.get(position).getReadStatus());
                    Log.e("groupCurrent_USErName", "groupCurrent_USErName" + mList.get(position).getUserName());
                    context.startActivity(intent);
                });
            }

            @Override
            public void customFun4(ChatVideoItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));



                binding.play.setOnClickListener(v -> {
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath", mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });

                // binding. videoView.setVideoPath("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
                //  binding. videoView.setVideoPath(mList.get(position).getFilePath());
                //  binding. videoView.start();
                // Picasso.with(context).load(mList.get(position).getFilePath()).into(binding.VideoView);
            }

            @Override
            public void customFun5(ChatVideoItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));



                binding.play.setOnClickListener(v -> {
                    Intent intent = new Intent(context, VideoActivity.class);
                    intent.putExtra("VideoPath", mList.get(position).getFilePath());
                    //  intent.putExtra("VideoPath",mList.get(position).getFilePath());
                    context.startActivity(intent);

                });
            }

            @Override
            public void customFun6(ChatAudioItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                binding.totalTime.setText(mList.get(position).getFormattedTime());

                if(mList.get(position).getFilePath()!=null&&(!TextUtils.isEmpty(mList.get(position).getFilePath()))&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                    try {
                        Picasso.with(context).load(mList.get(position).getMessage()).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                String audioUrl = "http://www.all-birds.com/Sound/western%20bluebird.wav";
                Log.e("ChatView", "customFun7:"+mList.get(position).getFilePath());
                // String audioUrl = "http://www.wehyphens.com/test/MereNaamTu%20.mp3";

                MediaPlayerHandler playerHandler = new MediaPlayerHandler();
                statusAudiio = true;

                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (statusAudiio) {
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);
                            playerHandler.startPlaying();

                            statusAudiio = false;

                        } else {
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);
                            playerHandler.pausePlaying();
                            statusAudiio = true;

                        }
                    }
                });
                playerHandler.setSeekLisner(mList.get(position).getMessage(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                        //      Toast.makeText(context, "path::"+mList.get(position).getFilePath(), Toast.LENGTH_SHORT).show();
                        binding.progessSeekBar.setMax(setLength);
                        binding.remiainTime.setText("" + milliSecondsToTimer((long) setLength));

                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        long totalDuration = length; // to get total duration in milliseconds
                        binding.progessSeekBar.setProgress(length);

                        binding.remiainTime.setText("" + milliSecondsToTimer((long) length));
                       // binding.remiainTime.setText(length+"");


                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        if(isPlaying==1)
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);
                        else  if(isPlaying==2)
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);

                        else  if(isPlaying==3)
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);
                        //binding.progessSeekBar.setSta(isPlaying);


                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {
                      //  binding.remiainTime.setText("" + milliSecondsToTimer((long) duration));
                    }
                });
            }

            @Override
            public void customFun7(ChatAudioItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                if(profilepic!=null&&(!TextUtils.isEmpty(profilepic)&&!profilepic.equalsIgnoreCase(""))) {
                    Picasso.with(context).load("file://"+profilepic).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);
                }
                binding.totalTime.setText(mList.get(position).getFormattedTime());
               String audioUrl = "http://www.all-birds.com/Sound/western%20bluebird.wav";
                Log.e("ChatView", "customFun7:"+mList.get(position).getFilePath());
              // String audioUrl = "http://www.wehyphens.com/test/MereNaamTu%20.mp3";
                MediaPlayerHandler playerHandler = new MediaPlayerHandler();
                statusAudiio = true;
                int recordlengnth=mList.get(position).getMessage().getBytes().length;
                String audioTime=   milliSecondsToTimer((long) recordlengnth);
                binding.tvTotalTime.setText(mList.get(position).getFilesize());
                binding.remiainTime.setVisibility(GONE);
            //   Toast.makeText(context, "recorded:::"+mList.get(position).getFilesize(), Toast.LENGTH_SHORT).show();
                binding.closePlayer.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "close", Toast.LENGTH_SHORT).show();
                    }
                });
                binding.poetDetailPlayBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (statusAudiio) {
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);
                            playerHandler.startPlaying();

                            statusAudiio = false;

                        } else {
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);
                            playerHandler.pausePlaying();
                            statusAudiio = true;

                        }
                    }
                });
                playerHandler.setSeekLisner(mList.get(position).getMessage(), position, new MediaPlayerHandler.SeekListner() {
                    @Override
                    public void setSeekBarMaxLength(int setLength) {
                  //      Toast.makeText(context, "path::"+mList.get(position).getFilePath(), Toast.LENGTH_SHORT).show();
                        binding.progessSeekBar.setMax(setLength);
                       // binding.remiainTime.setText("" + milliSecondsToTimer((long) setLength));

                    }

                    @Override
                    public void setSeekBarCurrentProgress(int length) {
                        long totalDuration = length; //

                        binding.remiainTime.setVisibility(VISIBLE);
                        binding.tvTotalTime.setVisibility(GONE);
                        binding.progessSeekBar.setProgress(length);
                        binding.remiainTime.setText("" + milliSecondsToTimer((long) length));


                    }

                    @Override
                    public void playStatus(int isPlaying) {
                        if(isPlaying==1) {
                            binding.remiainTime.setVisibility(VISIBLE);
                            binding.tvTotalTime.setVisibility(GONE);

                            binding.poetDetailPlayBtn.setImageResource(R.drawable.pause);
                        }
                        else  if(isPlaying==2)
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);

                     else  if(isPlaying==3)
                            binding.poetDetailPlayBtn.setImageResource(R.drawable.ic_play_vector);
                        //binding.progessSeekBar.setSta(isPlaying);


                    }

                    @Override
                    public void invalidatePrevious() {

                    }

                    @Override
                    public void setTotalLength(int duration) {

                        binding.remiainTime.setVisibility(VISIBLE);
                        binding.tvTotalTime.setVisibility(GONE);

                    //    Toast.makeText(context, "lent", Toast.LENGTH_SHORT).show();
                      //  binding.remiainTime.setText("" + milliSecondsToTimer((long) duration));
                    }
                });
            }

            @Override
            public void customFun8(ChatDocItemRcvBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));


                if(mList.get(position).getFilePath()!=null&&(!TextUtils.isEmpty(mList.get(position).getFilePath()))&&!mList.get(position).getFilePath().equalsIgnoreCase("")) {
                    Picasso.with(context).load(mList.get(position).getFiletype()).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);

                }

                if (mList.get(position).getReadStatus() == 1) {
                    binding.tvReciverName.setVisibility(VISIBLE);
                    binding.tvReciverName.setText(CommonUtils.NameCaps(mList.get(position).getUserName()));


                } else {

                    binding.tvReciverName.setVisibility(GONE);
                }
                String forwordStatus=mList.get(position).getForwordStatus();
                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                if(mList.get(position).getForwordStatus()!=null){
                    if (mList.get(position).getForwordStatus().equalsIgnoreCase("1")){
                        binding.llForwordMsg.setVisibility(VISIBLE);
                    }else {
                        binding.llForwordMsg.setVisibility(GONE);
                    }}





                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position, mList.get(position).getFiletype(), view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);

                        }


                       /* statusDelete=true;
                        if(statusDelete){
                            binding.flBackground.setBackgroundColor(Color.parseColor("#3000CAAB"));
                            typingListener.userDeleteClick(position);

                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));

                                    // Do something after 5s = 5000ms

                                }
                            }, 4000);
                        }*/
                        return false;
                    }
                });

                binding.flBackground.setOnClickListener(v->{
                    binding.flBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                });






                String path = mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename = path.substring(path.lastIndexOf("/") + 1);
                binding.tvFileNmae.setText(mList.get(position).getMessage());
                binding.tvSize.setText(mList.get(position).getFilesize());


                binding.llDownload.setOnClickListener(v -> {


                });
                //  binding.tvType.setText(mList.get(position).getFiletype());
                Log.e("docpath", path);


                binding.llPdf.setOnClickListener(v -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mList.get(position).getFilePath()));
                    context.startActivity(browserIntent);

                });

            }

            @Override
            public void customFun9(ChatDocItemSentBinding binding, int position, List<ChatMessage> mList) {
                binding.setData(mList.get(position));
                String profilepic = CommonUtils.getPreferencesString(context, AppConstants.SENDER_PROFILE_PIC);
                if(profilepic!=null&&(!TextUtils.isEmpty(profilepic)&&!profilepic.equalsIgnoreCase(""))) {
                    Picasso.with(context).load("file://"+profilepic).error(R.drawable.placeholder_user).fit().noFade().centerCrop().into(binding.ivUserPic);
                }

                String forwordStatus=mList.get(position).getForwordStatus();

                if(forwordStatus==null){
                    forwordStatus="0";
                }
                if(forwordStatus.equalsIgnoreCase("1")){
                    binding.llForwordMsg.setVisibility(VISIBLE);
                }
                else {
                    binding.llForwordMsg.setVisibility(GONE);
                }

                //  TODO For long pres click listener
                binding.flBackground.setOnLongClickListener(new OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        statusDelete=true;
                        if (mList.get(position).getFilePath().equalsIgnoreCase("0")) {
                            statusDelete=false;
                        }else {
                            statusDelete=true;
                        }
                        if(statusDelete){
                            typingListener.userDeleteClick(position, mList.get(position).getFiletype(),view);
                            chatListView.clearOnScrollListeners();
                            chatListView.setSaveEnabled(true);
                        }
                        return false;
                    }
                });
                String path = mList.get(position).getFilePath();//it contain your path of image..im using a temp string..
                String filename = path.substring(path.lastIndexOf("/") + 1);
                binding.tvFileNmae.setText(mList.get(position).getMessage());
                binding.tvSize.setText(mList.get(position).getFilesize());

                binding.llDownload.setOnClickListener(v -> {

                });

                //   binding.tvType.setText(mList.get(position).getFiletype());

                Log.e("docpath", path);
                binding.llPdf.setOnClickListener(v -> {

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mList.get(position).getFilePath()));
                    context.startActivity(browserIntent);


                });


            }

            @Override
            public void customFun10(AdminAddDeleteBinding binding, int position, List<ChatMessage> mList) {
                binding.messageTextView.setText(mList.get(position).getMessage());
                Log.e("time",mList.get(position).getTimestamp()+"  "+mList.get(position).getFormattedDate());
                }
        };
//                bubbleBackgroundRcv, bubbleBackgroundSend, bubbleElevation);
        chatListView.setAdapter(chatViewListAdapter);


    }


    private void setViewAttributes() {
        setChatViewBackground();
        setInputFrameAttributes();
        setInputTextAttributes();
        setSendButtonAttributes();
        setUseEditorAction();
    }

    private void getChatViewBackgroundColor() {
        backgroundColor = attributes.getColor(R.styleable.ChatView_backgroundColor, -1);
    }

    private void getAttributesForBubbles() {

        float dip4 = context.getResources().getDisplayMetrics().density * 4.0f;
        int elevation = attributes.getInt(R.styleable.ChatView_bubbleElevation, ELEVATED);
        bubbleElevation = elevation == ELEVATED ? dip4 : 0;

        bubbleBackgroundRcv = attributes.getColor(R.styleable.ChatView_bubbleBackgroundRcv, ContextCompat.getColor(context, R.color.white));
        bubbleBackgroundSend = attributes.getColor(R.styleable.ChatView_bubbleBackgroundSend, ContextCompat.getColor(context, R.color.default_bubble_color_send));
    }


    private void getAttributesForInputFrame() {
        inputFrameBackgroundColor = attributes.getColor(R.styleable.ChatView_inputBackgroundColor, -1);
    }

    private void setInputFrameAttributes() {
        inputFrame.setCardBackgroundColor(inputFrameBackgroundColor);
    }

    private void setChatViewBackground() {
        this.setBackgroundColor(backgroundColor);
    }

    private void getAttributesForInputText() {
        setInputTextDefaults();
        if (hasStyleResourceSet()) {
            setTextAppearanceAttributes();
            setInputTextSize();
            setInputTextColor();
            setInputHintColor();
            textAppearanceAttributes.recycle();
        }
        overrideTextStylesIfSetIndividually();
    }

    private void setTextAppearanceAttributes() {
        final int textAppearanceId = attributes.getResourceId(R.styleable.ChatView_inputTextAppearance, 0);
        textAppearanceAttributes = getContext().obtainStyledAttributes(textAppearanceId, R.styleable.ChatViewInputTextAppearance);
    }

    private void setInputTextAttributes() {
        inputEditText.setTextColor(inputTextColor);
        inputEditText.setHintTextColor(inputHintColor);
        inputEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, inputTextSize);
    }

    private void getAttributesForSendButton() {
        sendButtonBackgroundTint = attributes.getColor(R.styleable.ChatView_sendBtnBackgroundTint, -1);
        sendButtonIconTint = attributes.getColor(R.styleable.ChatView_sendBtnIconTint, Color.WHITE);
        sendButtonIcon = attributes.getDrawable(R.styleable.ChatView_sendBtnIcon);
    }

    private void setSendButtonAttributes() {
       /* actionsMenu.getSendButton().setColorNormal(sendButtonBackgroundTint);
        actionsMenu.setIconDrawable(sendButtonIcon);

        buttonDrawable = actionsMenu.getIconDrawable();
        actionsMenu.setButtonIconTint(sendButtonIconTint);*/

    }

    private void getUseEditorAction() {
        useEditorAction = attributes.getBoolean(R.styleable.ChatView_inputUseEditorAction, false);
    }

    private void setUseEditorAction() {
        if (useEditorAction) {
            setupEditorAction();
        } else {
            inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        }
    }

    private boolean hasStyleResourceSet() {
        return attributes.hasValue(R.styleable.ChatView_inputTextAppearance);
    }

    private void setInputTextDefaults() {
        inputTextSize = context.getResources().getDimensionPixelSize(R.dimen.default_input_text_size);
        inputTextColor = ContextCompat.getColor(context, R.color.black);
        inputHintColor = ContextCompat.getColor(context, R.color.main_color_gray);
    }

    private void setInputTextSize() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextSize)) {
            inputTextSize = attributes.getDimensionPixelSize(R.styleable.ChatView_inputTextSize, inputTextSize);
        }
    }

    private void setInputTextColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextColor)) {
            inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        }
    }

    private void setInputHintColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputHintColor)) {
            inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
        }
    }

    private void overrideTextStylesIfSetIndividually() {
        inputTextSize = (int) attributes.getDimension(R.styleable.ChatView_inputTextSize, inputTextSize);
        inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
    }

    private void setupEditorAction() {
        inputEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputEditText.setImeOptions(EditorInfo.IME_ACTION_SEND);
        inputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    long stamp = System.currentTimeMillis();
                    String message = inputEditText.getText().toString();

                    if (!TextUtils.isEmpty(message)) {
                        sendMessage(message, stamp, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void setButtonClickListeners() {





        actionsMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                long stamp = System.currentTimeMillis();

                String message = inputEditText.getText().toString();
                if (!TextUtils.isEmpty(message)) {

                    // Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();

                    sendMessage(message, stamp, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                }


            }
        });

       /* actionsMenu.getSendButton().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (actionsMenu.isExpanded()) {
                    actionsMenu.collapse();
                    return;
                }

                long stamp = System.currentTimeMillis();

                String message = inputEditText.getText().toString();
                if (!TextUtils.isEmpty(message)) {

                    // Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();

                    sendMessage(message, stamp, ChatMessage.Type.TYPE_VIEW_SEN_MSG_TEXT_1);
                }


            }
        });*/

     /*   actionsMenu.getSendButton().setOnLongClickListener(new OnLongClickListener() {


            @Override
            public boolean onLongClick(View v) {
                actionsMenu.expand();
                return true;
            }
        });*/

        ivAttachment.setOnClickListener(view -> attachmentListener.onCamClick(false));
    }

    private void setUserTypingListener() {
        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {

                    if (!isTyping) {
                        isTyping = true;
                        if (typingListener != null) typingListener.userStartedTyping();
                    }

                    removeCallbacks(typingTimerRunnable);
                    postDelayed(typingTimerRunnable, 1500);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setUserStoppedTypingListener() {

        inputEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (previousFocusState && !hasFocus && typingListener != null) {
                    typingListener.userStoppedTyping();
                }
                previousFocusState = hasFocus;
            }
        });
    }

    private void setUserClickListener() {

        ivRecordVoice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                typingListener.userClick();

            }
        });
    }


    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
        return super.addViewInLayout(child, index, params);
    }

    public String getTypedMessage() {
        return inputEditText.getText().toString();
    }

    public void setTypingListener(TypingListener typingListener) {
        this.typingListener = typingListener;
    }





    public void setOnSentMessageListener(OnSentMessageListener onSentMessageListener) {
        this.onSentMessageListener = onSentMessageListener;
    }

    public void setEmojiClickListener(OnEmojiClickListener emojiClickListener) {
        this.emojiClickListener = emojiClickListener;
    }

    public void setAttachmentListener(OnAttachmentClickListener attachmentListener) {
        this.attachmentListener = attachmentListener;
    }

    public void sendMessage(String message, long stamp, int msgType) {

        ChatMessage chatMessage = new ChatMessage(message, stamp, msgType);
        if (onSentMessageListener != null && onSentMessageListener.sendMessage(chatMessage)) {
            chatViewListAdapter.addMessage(chatMessage);
            chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
            inputEditText.setText("");
        }
    }

    public void addMessage(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void addMessageImg(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void addMessages(ArrayList<ChatMessage> messages) {
        chatViewListAdapter.addMessages(messages);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }

    public void removeMessage(int position) {
        chatViewListAdapter.removeMessage(position);
        chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);
    }


    public void clearMessages() {
        chatViewListAdapter.clearMessages();
    }

    public EditText getInputEditText() {
        return inputEditText;
    }

   /* public FloatingActionsMenu getActionsMenu() {
        return actionsMenu;
    }*/

    public void hideInputLay() {
        inputLayout.setVisibility(View.GONE);
    }

    public void showInputLay() {
        inputLayout.setVisibility(View.VISIBLE);
    }

    public void addMessage(String path, long timestamp, int typeViewSenMsgImg3) {
    }


    public interface TypingListener {

        void userStartedTyping();

        void userStoppedTyping();

        void userClick();

        void userDeleteClick(int indexList, String b,View view);

    }

    public interface OnSentMessageListener {
        boolean sendMessage(ChatMessage chatMessage);
    }

    public interface OnAttachmentClickListener {
        void onCamClick(boolean isCam);
    }


    public interface OnEmojiClickListener {
        void onEmojiClick(boolean emoji);
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId) {
        iconToBeChanged.setImageResource(drawableResourceId);
    }


    public static String formateMilliSeccond(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);

        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        //      return  String.format("%02d Min, %02d Sec",
        //                TimeUnit.MILLISECONDS.toMinutes(milliseconds),
        //                TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
        //                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));

        // return timer string
        return finalTimerString;
    }

    public void setAdapter(int deletePosition, ChatMessage chatMessage) {
        chatViewListAdapter.getList().set(deletePosition,chatMessage);
        chatViewListAdapter.notifyDataSetChanged();

        // chatListView.scrollToPosition(chatViewListAdapter.getList().size() - 1);

    }


    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

}