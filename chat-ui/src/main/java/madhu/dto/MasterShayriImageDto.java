package madhu.dto;

import java.util.List;

/**
 * Created by abul on 11/12/17.
 */

public class MasterShayriImageDto {

    private String date;
    private List<ShayariDetailDto> shyri;

    public MasterShayriImageDto() {
    }

    public MasterShayriImageDto(String date, List<ShayariDetailDto> shyri) {
        this.date = date;
        this.shyri = shyri;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<ShayariDetailDto> getShyri() {
        return shyri;
    }

    public void setShyri(List<ShayariDetailDto> shyri) {
        this.shyri = shyri;
    }
}
